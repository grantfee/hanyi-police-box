package com.hanyi.runner;

import com.hanyi.web.controller.task.ScheduleTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;


@Component
public class HanYiApplicationRunner implements ApplicationRunner, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(HanYiApplicationRunner.class);

    @Autowired
    private ScheduleTask scheduleTask;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("HanYiApplicationRunner started ...");
        initCacheConfig();
        scheduleTask.start();
    }

    @Override
    public void destroy() throws Exception {
        scheduleTask.stop();
    }

    private void  initCacheConfig(){
        String json = null;

    }
}
