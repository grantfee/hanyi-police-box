package com.hanyi.web.controller.system;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaIgnore;
import com.hanyi.system.domain.vo.SysPersonAttrVo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.hanyi.common.annotation.RepeatSubmit;
import com.hanyi.common.annotation.Log;
import com.hanyi.common.core.controller.BaseController;
import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.common.core.domain.R;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import com.hanyi.common.core.validate.QueryGroup;
import com.hanyi.common.enums.BusinessType;
import com.hanyi.common.utils.poi.ExcelUtil;
import com.hanyi.system.domain.vo.SysFaceAttrVo;
import com.hanyi.system.domain.bo.SysFaceAttrBo;
import com.hanyi.system.service.ISysFaceAttrService;
import com.hanyi.common.core.page.TableDataInfo;

/**
 * 人脸属性
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/faceAttr")
public class SysFaceAttrController extends BaseController {

    private final ISysFaceAttrService iSysFaceAttrService;

    /**
     * 查询人脸属性列表
     */
    @SaCheckPermission("system:faceAttr:list")
    @GetMapping("/list")
    public TableDataInfo<SysFaceAttrVo> list(SysFaceAttrBo bo, PageQuery pageQuery) {
        return iSysFaceAttrService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出人脸属性列表
     */
    @SaCheckPermission("system:faceAttr:export")
    @Log(title = "人脸属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysFaceAttrBo bo, HttpServletResponse response) {
        List<SysFaceAttrVo> list = iSysFaceAttrService.queryList(bo);
        ExcelUtil.exportExcel(list, "人脸属性", SysFaceAttrVo.class, response);
    }

    /**
     * 获取人脸属性详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:faceAttr:query")
    @GetMapping("/{id}")
    public R<SysFaceAttrVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iSysFaceAttrService.queryById(id));
    }

    /**
     * 获取车辆属性详细信息
     *
     * @param id 主键
     */
    @SaIgnore
    @GetMapping("/detail/{id}")
    public R<SysFaceAttrVo> getInfoByEventId(@NotNull(message = "主键不能为空")
                                               @PathVariable Long id) {
        return R.ok(iSysFaceAttrService.queryByEventId(id));
    }
    /**
     * 新增人脸属性
     */
    @SaCheckPermission("system:faceAttr:add")
    @Log(title = "人脸属性", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysFaceAttrBo bo) {
        return toAjax(iSysFaceAttrService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改人脸属性
     */
    @SaCheckPermission("system:faceAttr:edit")
    @Log(title = "人脸属性", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysFaceAttrBo bo) {
        return toAjax(iSysFaceAttrService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除人脸属性
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:faceAttr:remove")
    @Log(title = "人脸属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iSysFaceAttrService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
