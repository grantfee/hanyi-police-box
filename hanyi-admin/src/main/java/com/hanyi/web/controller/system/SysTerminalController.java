package com.hanyi.web.controller.system;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.hanyi.common.annotation.RepeatSubmit;
import com.hanyi.common.annotation.Log;
import com.hanyi.common.core.controller.BaseController;
import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.common.core.domain.R;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import com.hanyi.common.core.validate.QueryGroup;
import com.hanyi.common.enums.BusinessType;
import com.hanyi.common.utils.poi.ExcelUtil;
import com.hanyi.system.domain.vo.SysTerminalVo;
import com.hanyi.system.domain.bo.SysTerminalBo;
import com.hanyi.system.service.ISysTerminalService;
import com.hanyi.common.core.page.TableDataInfo;

/**
 * 终端管理
 *
 * @author grantfee
 * @date 2023-11-21
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/terminal")
public class SysTerminalController extends BaseController {

    private final ISysTerminalService iSysTerminalService;

    /**
     * 查询终端管理列表
     */
    @SaCheckPermission("system:terminal:list")
    @GetMapping("/list")
    public TableDataInfo<SysTerminalVo> list(SysTerminalBo bo, PageQuery pageQuery) {
        return iSysTerminalService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出终端管理列表
     */
    @SaCheckPermission("system:terminal:export")
    @Log(title = "终端管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysTerminalBo bo, HttpServletResponse response) {
        List<SysTerminalVo> list = iSysTerminalService.queryList(bo);
        ExcelUtil.exportExcel(list, "终端管理", SysTerminalVo.class, response);
    }

    /**
     * 获取终端管理详细信息
     *
     * @param terminalId 主键
     */
    @SaCheckPermission("system:terminal:query")
    @GetMapping("/{terminalId}")
    public R<SysTerminalVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long terminalId) {
        return R.ok(iSysTerminalService.queryById(terminalId));
    }

    /**
     * 新增终端管理
     */
    @SaCheckPermission("system:terminal:add")
    @Log(title = "终端管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysTerminalBo bo) {
        return toAjax(iSysTerminalService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改终端管理
     */
    @SaCheckPermission("system:terminal:edit")
    @Log(title = "终端管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysTerminalBo bo) {
        return toAjax(iSysTerminalService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除终端管理
     *
     * @param terminalIds 主键串
     */
    @SaCheckPermission("system:terminal:remove")
    @Log(title = "终端管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{terminalIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] terminalIds) {
        return toAjax(iSysTerminalService.deleteWithValidByIds(Arrays.asList(terminalIds), true) ? 1 : 0);
    }
}
