package com.hanyi.web.controller.system;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaIgnore;
import com.hanyi.system.domain.vo.SysVehicleAttrVo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.hanyi.common.annotation.RepeatSubmit;
import com.hanyi.common.annotation.Log;
import com.hanyi.common.core.controller.BaseController;
import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.common.core.domain.R;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import com.hanyi.common.core.validate.QueryGroup;
import com.hanyi.common.enums.BusinessType;
import com.hanyi.common.utils.poi.ExcelUtil;
import com.hanyi.system.domain.vo.SysPersonAttrVo;
import com.hanyi.system.domain.bo.SysPersonAttrBo;
import com.hanyi.system.service.ISysPersonAttrService;
import com.hanyi.common.core.page.TableDataInfo;

/**
 * 行人属性
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/personAttr")
public class SysPersonAttrController extends BaseController {

    private final ISysPersonAttrService iSysPersonAttrService;

    /**
     * 查询行人属性列表
     */
    @SaCheckPermission("system:personAttr:list")
    @GetMapping("/list")
    public TableDataInfo<SysPersonAttrVo> list(SysPersonAttrBo bo, PageQuery pageQuery) {
        return iSysPersonAttrService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出行人属性列表
     */
    @SaCheckPermission("system:personAttr:export")
    @Log(title = "行人属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysPersonAttrBo bo, HttpServletResponse response) {
        List<SysPersonAttrVo> list = iSysPersonAttrService.queryList(bo);
        ExcelUtil.exportExcel(list, "行人属性", SysPersonAttrVo.class, response);
    }

    /**
     * 获取行人属性详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:personAttr:query")
    @GetMapping("/{id}")
    public R<SysPersonAttrVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iSysPersonAttrService.queryById(id));
    }

    /**
     * 获取车辆属性详细信息
     *
     * @param id 主键
     */
    @SaIgnore
    @GetMapping("/detail/{id}")
    public R<SysPersonAttrVo> getInfoByEventId(@NotNull(message = "主键不能为空")
                                                @PathVariable Long id) {
        return R.ok(iSysPersonAttrService.queryByEventId(id));
    }

    /**
     * 新增行人属性
     */
    @SaCheckPermission("system:personAttr:add")
    @Log(title = "行人属性", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysPersonAttrBo bo) {
        return toAjax(iSysPersonAttrService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改行人属性
     */
    @SaCheckPermission("system:personAttr:edit")
    @Log(title = "行人属性", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysPersonAttrBo bo) {
        return toAjax(iSysPersonAttrService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除行人属性
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:personAttr:remove")
    @Log(title = "行人属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iSysPersonAttrService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
