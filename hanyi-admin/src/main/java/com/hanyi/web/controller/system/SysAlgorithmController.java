package com.hanyi.web.controller.system;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.net.NetUtil;
import com.hanyi.common.annotation.Log;
import com.hanyi.common.annotation.RepeatSubmit;
import com.hanyi.common.core.controller.BaseController;
import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.common.core.domain.R;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import com.hanyi.common.enums.BusinessType;
import com.hanyi.common.utils.ShellUtils;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.utils.poi.ExcelUtil;
import com.hanyi.system.domain.SysLocalTask;
import com.hanyi.system.domain.bo.SysLocalTaskBo;
import com.hanyi.system.domain.vo.SysLocalTaskVo;
import com.hanyi.system.service.ISysLocalTaskService;
import com.hanyi.thirdparty.AlgorithmLicenseResponse;
import com.hanyi.thirdparty.BaseResponse;
import com.hanyi.thirdparty.ServiceVersionResponse;
import com.hanyi.thirdparty.SetGlobalUploadConfigRequest;
import com.hanyi.thirdparty.service.IAlgorithmService;
import com.hanyi.web.controller.task.ScheduleTask;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * 本地任务
 *
 * @author grantfee
 * @date 2023-11-06
 */

@Slf4j
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/algorithm")
public class SysAlgorithmController extends BaseController {

    private final IAlgorithmService iAlgorithmService;

    /**
     * 获取版本详细信息
     *
     */
    @SaCheckPermission("system:localTask:query")
    @GetMapping("/version")
    public R<ServiceVersionResponse> getVersion( ) {
        ServiceVersionResponse resp = iAlgorithmService.getServiceVersion();
        return R.ok(resp);
    }

    /**
     * 调用脚本重新启动算法模块
     *
     */
    @SaCheckPermission("system:localTask:query")
    @GetMapping("/restartAlgorithm")
    public R<Void> restartAlgorithm( ) {
        List<String> result =  ShellUtils.exceCommond("/home/linaro/hanyi-admin/restart_ais.sh");
        if(!result.isEmpty()){
            for(String s:result){
                log.info("exec shell result:"+s);
            }
        }else{
            R.fail("Failed to exec restart cmd");
        }
        return R.ok();
    }

    /**
     * 获取本地任务详细信息
     *
     */
    @SaCheckPermission("system:localTask:query")
    @GetMapping("/info")
    public R<AlgorithmLicenseResponse> getAlgorithmInfo( ) {
        AlgorithmLicenseResponse resp = iAlgorithmService.getAlgorithmLicense();
        return R.ok(resp);
    }

    @SaCheckPermission("system:localTask:add")
    @Log(title = "全局配置", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PostMapping("/updateGlobalConfig")
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SetGlobalUploadConfigRequest globalUploadConfigRequest){
        String localIP =  NetUtil.getLocalhostStr();
        String uploadUrl = localIP+":8080"+globalUploadConfigRequest.getUpload_address();
        globalUploadConfigRequest.setUpload_address(uploadUrl);

        BaseResponse resp =  iAlgorithmService.setGlobalUploadConfig(globalUploadConfigRequest);
        if(resp.getCode()==0){
            return R.ok();
        }
        return R.fail(resp.getMessage());
    }

}
