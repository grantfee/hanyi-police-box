package com.hanyi.web.controller.system;

import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.annotation.SaIgnore;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.hanyi.common.annotation.RepeatSubmit;
import com.hanyi.common.annotation.Log;
import com.hanyi.common.core.controller.BaseController;
import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.common.core.domain.R;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import com.hanyi.common.core.validate.QueryGroup;
import com.hanyi.common.enums.BusinessType;
import com.hanyi.common.utils.poi.ExcelUtil;
import com.hanyi.system.domain.vo.SysVehicleAttrVo;
import com.hanyi.system.domain.bo.SysVehicleAttrBo;
import com.hanyi.system.service.ISysVehicleAttrService;
import com.hanyi.common.core.page.TableDataInfo;

/**
 * 车辆属性
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/vehicleAttr")
public class SysVehicleAttrController extends BaseController {

    private final ISysVehicleAttrService iSysVehicleAttrService;

    /**
     * 查询车辆属性列表
     */
    @SaCheckPermission("system:vehicleAttr:list")
    @GetMapping("/list")
    public TableDataInfo<SysVehicleAttrVo> list(SysVehicleAttrBo bo, PageQuery pageQuery) {
        return iSysVehicleAttrService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出车辆属性列表
     */
    @SaCheckPermission("system:vehicleAttr:export")
    @Log(title = "车辆属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysVehicleAttrBo bo, HttpServletResponse response) {
        List<SysVehicleAttrVo> list = iSysVehicleAttrService.queryList(bo);
        ExcelUtil.exportExcel(list, "车辆属性", SysVehicleAttrVo.class, response);
    }

    /**
     * 获取车辆属性详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:vehicleAttr:query")
    @GetMapping("/{id}")
    public R<SysVehicleAttrVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iSysVehicleAttrService.queryById(id));
    }

    /**
     * 获取车辆属性详细信息
     *
     * @param id 主键
     */
    @SaIgnore
    @GetMapping("/detail/{id}")
    public R<SysVehicleAttrVo> getInfoByEventId(@NotNull(message = "主键不能为空")
                                       @PathVariable Long id) {
        return R.ok(iSysVehicleAttrService.queryByEventId(id));
    }

    /**
     * 新增车辆属性
     */
    @SaCheckPermission("system:vehicleAttr:add")
    @Log(title = "车辆属性", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysVehicleAttrBo bo) {
        return toAjax(iSysVehicleAttrService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改车辆属性
     */
    @SaCheckPermission("system:vehicleAttr:edit")
    @Log(title = "车辆属性", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysVehicleAttrBo bo) {
        return toAjax(iSysVehicleAttrService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除车辆属性
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:vehicleAttr:remove")
    @Log(title = "车辆属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iSysVehicleAttrService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
