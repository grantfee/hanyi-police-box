package com.hanyi.web.controller.system;

import java.util.List;
import java.util.Arrays;

import com.hanyi.system.domain.SysRealtimeTask;
import com.hanyi.thirdparty.service.IAlgorithmService;
import com.hanyi.web.controller.task.ScheduleTask;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.hanyi.common.annotation.RepeatSubmit;
import com.hanyi.common.annotation.Log;
import com.hanyi.common.core.controller.BaseController;
import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.common.core.domain.R;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import com.hanyi.common.core.validate.QueryGroup;
import com.hanyi.common.enums.BusinessType;
import com.hanyi.common.utils.poi.ExcelUtil;
import com.hanyi.system.domain.vo.SysRealtimeTaskVo;
import com.hanyi.system.domain.bo.SysRealtimeTaskBo;
import com.hanyi.system.service.ISysRealtimeTaskService;
import com.hanyi.common.core.page.TableDataInfo;

/**
 * 实时任务
 *
 * @author grantfee
 * @date 2023-11-05
 */
@Slf4j
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/realtimeTask")
public class SysRealtimeTaskController extends BaseController {

    private final ISysRealtimeTaskService iSysRealtimeTaskService;


    private final ScheduleTask scheduleTask;

    /**
     * 查询实时任务列表
     */
    @SaCheckPermission("system:realtimeTask:list")
    @GetMapping("/list")
    public TableDataInfo<SysRealtimeTaskVo> list(SysRealtimeTaskBo bo, PageQuery pageQuery) {
        return iSysRealtimeTaskService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出实时任务列表
     */
    @SaCheckPermission("system:realtimeTask:export")
    @Log(title = "实时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysRealtimeTaskBo bo, HttpServletResponse response) {
        List<SysRealtimeTaskVo> list = iSysRealtimeTaskService.queryList(bo);
        ExcelUtil.exportExcel(list, "实时任务", SysRealtimeTaskVo.class, response);
    }

    /**
     * 获取实时任务详细信息
     *
     * @param taskId 主键
     */
    @SaCheckPermission("system:realtimeTask:query")
    @GetMapping("/{taskId}")
    public R<SysRealtimeTaskVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long taskId) {
        return R.ok(iSysRealtimeTaskService.queryById(taskId));
    }

    /**
     * 新增实时任务
     */
    @SaCheckPermission("system:realtimeTask:add")
    @Log(title = "实时任务", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysRealtimeTaskBo bo) {
        return toAjax(iSysRealtimeTaskService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改实时任务
     */
    @SaCheckPermission("system:realtimeTask:edit")
    @Log(title = "实时任务", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysRealtimeTaskBo bo) {
        return toAjax(iSysRealtimeTaskService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除实时任务
     *
     * @param taskIds 主键串
     */
    @SaCheckPermission("system:realtimeTask:remove")
    @Log(title = "实时任务", businessType = BusinessType.DELETE)
    @DeleteMapping("/{taskIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] taskIds) {
        return toAjax(iSysRealtimeTaskService.deleteWithValidByIds(Arrays.asList(taskIds), true) ? 1 : 0);
    }

    /**
     * 启动实时任务
     *
     * @param taskId 主键
     */
    @SaCheckPermission("system:realtimeTask:query")
    @GetMapping("/start/{taskId}")
    public R<Void> startTask(@NotNull(message = "主键不能为空")
                                        @PathVariable Long taskId) {
        log.info("prepare to start realtime task:"+taskId);
        //发送消息到边缘盒子启动任务
        SysRealtimeTask task = iSysRealtimeTaskService.queryByTaskId(taskId);
        boolean ok = scheduleTask.startRealTimeTask(task.getTaskId(),task.getMainStreamUrl());
        if(ok){
            task.setStatus("1");
            iSysRealtimeTaskService.updateByEntity(task);
            return R.ok();
        }
        return R.fail("启动任务失败!请稍后重试");
    }

    @Async
    protected void startTaskAsync(SysRealtimeTask task){
        scheduleTask.startRealTimeTask(task.getTaskId(),task.getMainStreamUrl());
    }

    /**
     * 停止实时任务
     *
     * @param taskId 主键
     */
    @SaCheckPermission("system:realtimeTask:query")
    @GetMapping("/stop/{taskId}")
    public R<Void> stopTask(@NotNull(message = "主键不能为空")
                                        @PathVariable Long taskId) {
        log.info("prepare to stop realtime task:"+taskId);
        //发送消息到边缘盒子停止任务
        SysRealtimeTask task = iSysRealtimeTaskService.queryByTaskId(taskId);
        task.setStatus("0");
        iSysRealtimeTaskService.updateByEntity(task);

        stopTaskAsync(taskId);
        return R.ok();
    }

    @Async
    protected void stopTaskAsync(long taskId){
        scheduleTask.stopTask(taskId);
    }
}
