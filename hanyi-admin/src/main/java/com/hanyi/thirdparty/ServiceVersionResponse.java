package com.hanyi.thirdparty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-12 10:49
 * @Version 1.0
 */

@Data
@NoArgsConstructor
public class ServiceVersionResponse extends BaseResponse{
    private VersionInfo data;

    public static class VersionInfo{
        private String server_version;
        private String algorithm_version;

        public String getServer_version() {
            return server_version;
        }

        public void setServer_version(String server_version) {
            this.server_version = server_version;
        }

        public String getAlgorithm_version() {
            return algorithm_version;
        }

        public void setAlgorithm_version(String algorithm_version) {
            this.algorithm_version = algorithm_version;
        }
    }
}
