package com.hanyi.thirdparty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 10:59
 * @Version 1.0
 *
 * 检查指定通道状态请求
 */

@Data
@NoArgsConstructor
public class CheckChannelInfoRequest {
    private int channel_id;
    private String task_id;
}
