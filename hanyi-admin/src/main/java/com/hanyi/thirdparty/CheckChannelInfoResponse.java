package com.hanyi.thirdparty;

import com.hanyi.thirdparty.domain.ChannelInfo;

/**
 *
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 11:16
 * @Version 1.0
 *
 * 通道状态响应
 */
public class CheckChannelInfoResponse extends BaseResponse{
    private ChannelInfo data;

    public ChannelInfo getData() {
        return data;
    }

    public void setData(ChannelInfo data) {
        this.data = data;
    }
}
