package com.hanyi.thirdparty;

import com.hanyi.thirdparty.domain.StreamConfig;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 10:14
 * @Version 1.0
 * 添加处理通道
 * {
 * "task_id": "task1",
 * "alg_flag": 8,
 * "upload_address": "192.168.1.104:18185/api/platform/upload",
 * "keep_frame": false,
 * "stream_config": {
 * "url": "/mnt/102_data/zhangyixiong/pic/video/zhuheqiao.h264",
 * "speed_rate": 1,
 * "pump_frame_rate": 5,
 * "transfer_type": "tcp",
 * "loop_play": true,
 * "type": 1,
 * "start_time": 0,
 * "end_time": 0
 *  },
 * "regions_config": []
 * }
 */

@Data
@NoArgsConstructor
public class AddChannelRequest {

    private String task_id;
    private int alg_flag;
    private String upload_address;
    private boolean keep_frame;
    private StreamConfig stream_config;

}
