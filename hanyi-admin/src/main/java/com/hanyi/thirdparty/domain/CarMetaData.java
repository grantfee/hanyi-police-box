package com.hanyi.thirdparty.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-20 14:26
 * @Version 1.0
 */
@Data
@NoArgsConstructor
public class CarMetaData {
    private long ID;
    private String DeviceID;
    private int Workflow;
    private String WorkflowName;
    private int Channel;
    private String ChannelName;
    private long Timestamp;
    private int MetaType;
    private String Attribute;
    private String Feature;
    private String Image;
    private String FullImage;
    private String ExtraImage;
    private DetectRect Rect;
    private String Region;
}
