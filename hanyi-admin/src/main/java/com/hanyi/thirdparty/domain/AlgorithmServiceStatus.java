package com.hanyi.thirdparty.domain;

import java.util.List;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 9:42
 * @Version 1.0
 */
public class AlgorithmServiceStatus {
    private int server_status;
    private List<DeviceAlgorithmInfo> device_info_list;

    public int getServer_status() {
        return server_status;
    }

    public void setServer_status(int server_status) {
        this.server_status = server_status;
    }

    public List<DeviceAlgorithmInfo> getDevice_info_list() {
        return device_info_list;
    }

    public void setDevice_info_list(List<DeviceAlgorithmInfo> device_info_list) {
        this.device_info_list = device_info_list;
    }
}
