package com.hanyi.thirdparty.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-04 10:31
 * @Version 1.0
 */

@Data
@NoArgsConstructor
public class AnalysisAttribute {
    private String attribute_type;

    private AttributeData attribute;


    public static class AttributeData{
        private String attr_name;
        private int label;
        private String text;
        private float score;

        private String licence;
        private float licence_score;

        public String getAttr_name() {
            return attr_name;
        }

        public void setAttr_name(String attr_name) {
            this.attr_name = attr_name;
        }

        public int getLabel() {
            return label;
        }

        public void setLabel(int label) {
            this.label = label;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public float getScore() {
            return score;
        }

        public void setScore(float score) {
            this.score = score;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public float getLicence_score() {
            return licence_score;
        }

        public void setLicence_score(float licence_score) {
            this.licence_score = licence_score;
        }
    }
}
