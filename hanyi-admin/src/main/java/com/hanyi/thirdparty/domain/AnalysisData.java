package com.hanyi.thirdparty.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-04 10:07
 * @Version 1.0
 */

@NoArgsConstructor
@Data
public class AnalysisData {
    private int type;
    private int device_id;
    private String task_id;
    private int channel_id;
    private String crop_image_data;
    private String full_image_data;

    private Result result;

    public  static class Result{
        private long frame_id;

        private List<AlgResult> alg_results;

        public long getFrame_id() {
            return frame_id;
        }

        public void setFrame_id(long frame_id) {
            this.frame_id = frame_id;
        }

        public List<AlgResult> getAlg_results() {
            return alg_results;
        }

        public void setAlg_results(List<AlgResult> alg_results) {
            this.alg_results = alg_results;
        }

        public static class AlgResult{
            private int code;
            private int algType;

            private List<AlgResultObjects> objects;

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public int getAlgType() {
                return algType;
            }

            public void setAlgType(int algType) {
                this.algType = algType;
            }

            public List<AlgResultObjects> getObjects() {
                return objects;
            }

            public void setObjects(List<AlgResultObjects> objects) {
                this.objects = objects;
            }

            public static class AlgResultObjects{
                private int type;
                private boolean is_mistake;
                private List<AnalysisAttribute> attributes;

                public int getType() {
                    return type;
                }

                public void setType(int type) {
                    this.type = type;
                }

                public boolean isIs_mistake() {
                    return is_mistake;
                }

                public void setIs_mistake(boolean is_mistake) {
                    this.is_mistake = is_mistake;
                }

                public List<AnalysisAttribute> getAttributes() {
                    return attributes;
                }

                public void setAttributes(List<AnalysisAttribute> attributes) {
                    this.attributes = attributes;
                }
            }
        }
    }
}
