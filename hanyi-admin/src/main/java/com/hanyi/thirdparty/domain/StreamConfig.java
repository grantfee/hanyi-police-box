package com.hanyi.thirdparty.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 10:12
 * @Version 1.0
 *
 * "stream_config": {
 * "url": "/mnt/102_data/zhangyixiong/pic/video/zhuheqiao.h264",
 * "speed_rate": 1,
 * "pump_frame_rate": 5,
 * "transfer_type": "tcp",
 * "loop_play": true,
 * "type": 1,
 * "start_time": 0,
 * "end_time": 0
 *  }
 */

@Data
@NoArgsConstructor
public class StreamConfig {
    private String url;
    private int speed_rate;
    private int pump_frame_rate;
    private String transfer_type;
    private boolean loop_play;
    private int type;

}
