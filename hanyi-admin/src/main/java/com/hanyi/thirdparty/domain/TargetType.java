package com.hanyi.thirdparty.domain;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-04 11:48
 * @Version 1.0
 */
public class TargetType {
    public final static int PERSON_CLS=1;
    public final static int VEHICLE_CAR_CLS=5;
    public final static int VEHICLE_BUS_CLS=6;
    public final static int VEHICLE_MINANBAO_CLS=7;
    public final static int VEHICLE_TRUCK_CLS=8;

    public final static int FACE_CLS = 9;
}
