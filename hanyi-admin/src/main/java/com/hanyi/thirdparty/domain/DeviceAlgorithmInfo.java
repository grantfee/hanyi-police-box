package com.hanyi.thirdparty.domain;

import java.util.List;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 9:52
 * @Version 1.0
 */
public class DeviceAlgorithmInfo {
    private int device_id;

    private List<AlgorithmInfo> alg_info_list;

    public int getDevice_id() {
        return device_id;
    }

    public void setDevice_id(int device_id) {
        this.device_id = device_id;
    }

    public List<AlgorithmInfo> getAlg_info_list() {
        return alg_info_list;
    }

    public void setAlg_info_list(List<AlgorithmInfo> alg_info_list) {
        this.alg_info_list = alg_info_list;
    }

    public class AlgorithmInfo{
        private int alg_type;
        private int capability;
        private int remain;

        public int getAlg_type() {
            return alg_type;
        }

        public void setAlg_type(int alg_type) {
            this.alg_type = alg_type;
        }

        public int getCapability() {
            return capability;
        }

        public void setCapability(int capability) {
            this.capability = capability;
        }

        public int getRemain() {
            return remain;
        }

        public void setRemain(int remain) {
            this.remain = remain;
        }
    }
}
