package com.hanyi.thirdparty.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-20 14:22
 * @Version 1.0
 */

@Data
@NoArgsConstructor
public class DetectRect {
    private int Left;
    private int Top;
    private int Right;
    private int Bottom;
}
