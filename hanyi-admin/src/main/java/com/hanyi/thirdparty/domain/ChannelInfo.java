package com.hanyi.thirdparty.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 11:14
 * @Version 1.0
 *
 *
"device_id": 0,
"channel_id": 1,
"task_id": "task1",
"status": 2
//通道的状态信息，0:通道不存在，1:通道正在创建，2:通道正在运
//⾏，3:通道正在修改，4:通道正在关闭，5:通道被释放，6:通道打
//开失败，7:通道seek失败
}
 */

@Data
@NoArgsConstructor
public class ChannelInfo {
    private int device_id;
    private int channel_id;
    private String task_id;
    private int status;
}
