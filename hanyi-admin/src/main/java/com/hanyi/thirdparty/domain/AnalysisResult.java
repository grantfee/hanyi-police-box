package com.hanyi.thirdparty.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-04 10:17
 * @Version 1.0
 */
@NoArgsConstructor
@Data
public class AnalysisResult {
    private int code;
    private String message;
    private List<AnalysisData> data;

}
