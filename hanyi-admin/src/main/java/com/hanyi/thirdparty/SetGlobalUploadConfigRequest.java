package com.hanyi.thirdparty;

/**
 *
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 11:22
 * @Version 1.0
 *
 * 设置上传配置
 * {
 * "upload_address": "192.168.1.104:18185",
 * "upload_image_type": 0,
 * "upload_frame_results": false,
 * "upload_type": 0,
 * "storage_dir": "./data",
 * "upload_datas": {
 * "crop_image": true,
 * "full_image": false,
 * "result": true
 *  }
 * }
 */
public class SetGlobalUploadConfigRequest {
    private String upload_address;
    private int upload_image_type;
    private boolean upload_frame_results;
    private int upload_type;
    private String storage_dir;

    private UploadType upload_datas;

    public String getUpload_address() {
        return upload_address;
    }

    public void setUpload_address(String upload_address) {
        this.upload_address = upload_address;
    }

    public int getUpload_image_type() {
        return upload_image_type;
    }

    public void setUpload_image_type(int upload_image_type) {
        this.upload_image_type = upload_image_type;
    }

    public boolean isUpload_frame_results() {
        return upload_frame_results;
    }

    public void setUpload_frame_results(boolean upload_frame_results) {
        this.upload_frame_results = upload_frame_results;
    }

    public int getUpload_type() {
        return upload_type;
    }

    public void setUpload_type(int upload_type) {
        this.upload_type = upload_type;
    }

    public String getStorage_dir() {
        return storage_dir;
    }

    public void setStorage_dir(String storage_dir) {
        this.storage_dir = storage_dir;
    }

    public UploadType getUpload_datas() {
        return upload_datas;
    }

    public void setUpload_datas(UploadType upload_datas) {
        this.upload_datas = upload_datas;
    }

    public static  class UploadType{
        private boolean crop_image;
        private boolean full_image;

        private boolean result;

        public boolean isCrop_image() {
            return crop_image;
        }

        public void setCrop_image(boolean crop_image) {
            this.crop_image = crop_image;
        }

        public boolean isFull_image() {
            return full_image;
        }

        public void setFull_image(boolean full_image) {
            this.full_image = full_image;
        }

        public boolean isResult() {
            return result;
        }

        public void setResult(boolean result) {
            this.result = result;
        }
    }
}
