package com.hanyi.thirdparty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-12 10:49
 * @Version 1.0
 */

@Data
@NoArgsConstructor
public class AlgorithmLicenseResponse extends BaseResponse{
    private LicenseInfo data;

    public static class LicenseInfo{
        private int server_status;

        public int getServer_status() {
            return server_status;
        }

        public void setServer_status(int server_status) {
            this.server_status = server_status;
        }
    }
}
