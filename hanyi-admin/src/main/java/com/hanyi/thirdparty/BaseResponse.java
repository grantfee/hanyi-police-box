package com.hanyi.thirdparty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-20 11:12
 * @Version 1.0
 *
 * {
 *     "Code": 200,
 *     "Message": "msg: success",
 *     "Translate": "操作成功",
 *     "Detail": "",
 *     "Data": null
 *   }
 */

@Data
@NoArgsConstructor
public class BaseResponse {
    private int code;
    private String message;
}
