package com.hanyi.thirdparty.service;

import cn.hutool.core.net.NetUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.google.gson.Gson;
import com.hanyi.common.utils.spring.SpringUtils;
import com.hanyi.system.okhttp.OkHttpApi;
import com.hanyi.thirdparty.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 11:52
 * @Version 1.0
 */

@Slf4j
@Service
public class AlgorithmServiceImpl implements IAlgorithmService{
    private String localIP = "127.0.0.1";
    private final static int PORT=8182;
    private final static String GET_SERVICE_VERSION = "/api/aibox/version";
    private final static String GET_ALGORITHM_STATUS = "/api/aibox/info";
    private final static String SET_GLOBAL_CONFIG_URL = "/api/aibox/upload";
    private final static String CREATE_ALG_CHANNEL = "/api/aibox/channel/add";
    private final static String MODIFY_ALG_CHANNEL = "/api/aibox/channel/modify";
    private final static String CLOSE_ALG_CHANNEL = "/api/aibox/channel/close";
    private final static String CHECK_ALL_CHANNEL = "/api/aibox/channel/infos";
    @Autowired
    private OkHttpApi okHttpApi;


    @PostConstruct
    public void init(){
        String profile = SpringUtil.getActiveProfile();
        log.info("current active profile:"+profile);
        if(profile.equals("dev")){
            localIP = "192.168.2.100";
        }
    }
    @Override
    public ServiceVersionResponse getServiceVersion() {
        Gson gson = new Gson();
        String url = "http://"+localIP+":"+PORT+GET_SERVICE_VERSION;
        String result = okHttpApi.doPost(url,null);
        log.info("getServiceVersion resp:"+result);
        if(JSONUtil.isTypeJSON(result)){
            return gson.fromJson(result,ServiceVersionResponse.class);
        }

        return null;
    }

    @Override
    public AlgorithmLicenseResponse getAlgorithmLicense() {
        Gson gson = new Gson();
        String url = "http://"+localIP+":"+PORT+GET_ALGORITHM_STATUS;
        String result = okHttpApi.doPost(url,null);
        log.info("getAlgorithmLicense resp:"+result);
        if(JSONUtil.isTypeJSON(result)){
            return gson.fromJson(result,AlgorithmLicenseResponse.class);
        }

        return null;
    }

    /**
     * {
     * "upload_address": "192.168.31.242:8080/system/analysisResult/event/result",
     * "upload_image_type": 1,
     * "upload_frame_results": false,
     * "upload_type": 0,
     * "storage_dir": "/data/images/",
     * "upload_datas": {
     *     "crop_image": true,
     *     "full_image": true,
     *     "result": true
     *  }
     * }
     * */
    @Override
    public BaseResponse setGlobalUploadConfig(SetGlobalUploadConfigRequest request) {
        Gson gson = new Gson();
        String req = gson.toJson(request);
        //String localIP =  NetUtil.getLocalhostStr();
        String url = "http://"+localIP+":"+PORT+SET_GLOBAL_CONFIG_URL;
        String result = okHttpApi.doPostJson(url,req,null);
        log.info("setGlobalUploadConfig request:"+req);
        log.info("setGlobalUploadConfig resp:"+result);
        if(JSONUtil.isTypeJSON(result)){
            return gson.fromJson(result,BaseResponse.class);
        }

        return null;
    }

    @Override
    public AddChannelResponse addChannel(AddChannelRequest request) {
        Gson gson = new Gson();
        String req = gson.toJson(request);
        //String localIP =  NetUtil.getLocalhostStr();
        String url = "http://"+localIP+":"+PORT+CREATE_ALG_CHANNEL;
        String result = okHttpApi.doPostJson(url,req,null);
        log.info("addChannel request:"+req);
        log.info("addChannel resp:"+result);
        if(JSONUtil.isTypeJSON(result)){
            return gson.fromJson(result,AddChannelResponse.class);
        }
        return null;
    }

    @Override
    public BaseResponse modifyChannel(ModifyChannelRequest request) {
        Gson gson = new Gson();
        String req = gson.toJson(request);
        //String localIP =  NetUtil.getLocalhostStr();
        String url = "http://"+localIP+":"+PORT+MODIFY_ALG_CHANNEL;
        String result = okHttpApi.doPostJson(url,req,null);
        log.info("modifyChannel request:"+req);
        log.info("modifyChannel resp:"+result);
        if(JSONUtil.isTypeJSON(result)){
            return gson.fromJson(result,BaseResponse.class);
        }
        return null;

    }

    @Override
    public BaseResponse closeChannel(CloseChannelRequest request) {
        Gson gson = new Gson();
        String req = gson.toJson(request);
        //String localIP =  NetUtil.getLocalhostStr();

        String url = "http://"+localIP+":"+PORT+CLOSE_ALG_CHANNEL;
        String result = okHttpApi.doPostJson(url,req,null);
        log.info("closeChannel request:"+req);
        log.info("closeChannel resp:"+result);
        if(JSONUtil.isTypeJSON(result)){
            return gson.fromJson(result,BaseResponse.class);
        }
        return null;

    }

    @Override
    public CheckAllChannelInfoResponse checkChannelInfo() {
        Gson gson = new Gson();
        //String localIP =  NetUtil.getLocalhostStr();
        String url = "http://"+localIP+":"+PORT+CHECK_ALL_CHANNEL;
        String result = okHttpApi.doPost(url,null);
        log.info("checkChannelInfo request");
        log.info("checkChannelInfo resp:"+result);
        if(JSONUtil.isTypeJSON(result)){
            return gson.fromJson(result,CheckAllChannelInfoResponse.class);
        }
        return null;
    }
}
