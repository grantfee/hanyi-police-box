package com.hanyi.thirdparty.service;

import com.hanyi.thirdparty.*;
import com.hanyi.thirdparty.domain.StreamConfig;

public interface IAlgorithmService {
      ServiceVersionResponse getServiceVersion();
      AlgorithmLicenseResponse getAlgorithmLicense();
      BaseResponse setGlobalUploadConfig(SetGlobalUploadConfigRequest request);
      AddChannelResponse addChannel(AddChannelRequest request);
      BaseResponse modifyChannel(ModifyChannelRequest request);
      BaseResponse closeChannel(CloseChannelRequest request);
      CheckAllChannelInfoResponse checkChannelInfo();
}
