package com.hanyi.thirdparty;

import com.fasterxml.jackson.databind.ser.Serializers;

/**
 *
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 10:29
 * @Version 1.0
 *
 * 添加处理通道响应
 */
public class AddChannelResponse extends BaseResponse {

    private Channel data;

    public Channel getData() {
        return data;
    }

    public void setData(Channel data) {
        this.data = data;
    }

    public class Channel{
        private int channel;

        public int getChannel() {
            return channel;
        }

        public void setChannel(int channel) {
            this.channel = channel;
        }
    }
}
