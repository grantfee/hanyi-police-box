package com.hanyi.thirdparty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 10:44
 * @Version 1.0
 *关闭通道请求
 * {
 * "task_id": "task1",
 * "channel_id": 0
 * }
 */

@Data
@NoArgsConstructor
public class CloseChannelRequest {
    private String task_id;
    private int channel_id;
}
