package com.hanyi.thirdparty;

import com.hanyi.thirdparty.domain.ChannelInfo;

import java.util.List;

/**
 *
 * @Author grantfee
 * @Description TODO
 * @Date 2023-12-26 11:16
 * @Version 1.0
 *
 * 检查所有通道状态响应
 */
public class CheckAllChannelInfoResponse extends BaseResponse{

    private AllChannelInfo data;

    public AllChannelInfo getData() {
        return data;
    }

    public void setData(AllChannelInfo data) {
        this.data = data;
    }

    public class AllChannelInfo{
        private int running_channel_count;
        private List<ChannelInfo> channel_infos;

        public int getRunning_channel_count() {
            return running_channel_count;
        }

        public void setRunning_channel_count(int running_channel_count) {
            this.running_channel_count = running_channel_count;
        }

        public List<ChannelInfo> getChannel_infos() {
            return channel_infos;
        }

        public void setChannel_infos(List<ChannelInfo> channel_infos) {
            this.channel_infos = channel_infos;
        }
    }



}
