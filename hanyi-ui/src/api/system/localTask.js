import request from '@/utils/request'

// 查询本地任务列表
export function listLocalTask(query) {
  return request({
    url: '/system/localTask/list',
    method: 'get',
    params: query
  })
}

// 查询本地任务详细
export function getLocalTask(taskId) {
  return request({
    url: '/system/localTask/' + taskId,
    method: 'get'
  })
}

// 新增本地任务
export function addLocalTask(data) {
  return request({
    url: '/system/localTask',
    method: 'post',
    data: data
  })
}

// 修改本地任务
export function updateLocalTask(data) {
  return request({
    url: '/system/localTask',
    method: 'put',
    data: data
  })
}

// 删除本地任务
export function delLocalTask(taskId) {
  return request({
    url: '/system/localTask/' + taskId,
    method: 'delete'
  })
}

// 启动任务
export function startLocalTask(taskId) {
  return request({
    url: '/system/localTask/start/' + taskId,
    method: 'get'
  })
}

// 停止任务
export function stopLocalTask(taskId) {
  return request({
    url: '/system/localTask/stop/' + taskId,
    method: 'get'
  })
}

// 启动任务
export function playLocalVideo(taskId) {
  return request({
    url: '/system/localTask/play/' + taskId,
    method: 'get'
  })
}
