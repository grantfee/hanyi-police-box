import request from '@/utils/request'

// 查询行人属性列表
export function listPersonAttr(query) {
  return request({
    url: '/system/personAttr/list',
    method: 'get',
    params: query
  })
}

// 查询行人属性详细
export function getPersonAttr(id) {
  return request({
    url: '/system/personAttr/' + id,
    method: 'get'
  })
}

// 查询车辆属性详细
export function getPersonAttrByEventId(id) {
  return request({
    url: '/system/personAttr/detail/' + id,
    method: 'get'
  })
}

// 新增行人属性
export function addPersonAttr(data) {
  return request({
    url: '/system/personAttr',
    method: 'post',
    data: data
  })
}

// 修改行人属性
export function updatePersonAttr(data) {
  return request({
    url: '/system/personAttr',
    method: 'put',
    data: data
  })
}

// 删除行人属性
export function delPersonAttr(id) {
  return request({
    url: '/system/personAttr/' + id,
    method: 'delete'
  })
}
