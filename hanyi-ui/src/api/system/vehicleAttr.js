import request from '@/utils/request'

// 查询车辆属性列表
export function listVehicleAttr(query) {
  return request({
    url: '/system/vehicleAttr/list',
    method: 'get',
    params: query
  })
}

// 查询车辆属性详细
export function getVehicleAttr(id) {
  return request({
    url: '/system/vehicleAttr/' + id,
    method: 'get'
  })
}

// 查询车辆属性详细
export function getVehicleAttrByEventId(id) {
  return request({
    url: '/system/vehicleAttr/detail/' + id,
    method: 'get'
  })
}

// 新增车辆属性
export function addVehicleAttr(data) {
  return request({
    url: '/system/vehicleAttr',
    method: 'post',
    data: data
  })
}

// 修改车辆属性
export function updateVehicleAttr(data) {
  return request({
    url: '/system/vehicleAttr',
    method: 'put',
    data: data
  })
}

// 删除车辆属性
export function delVehicleAttr(id) {
  return request({
    url: '/system/vehicleAttr/' + id,
    method: 'delete'
  })
}
