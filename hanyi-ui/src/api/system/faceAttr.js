import request from '@/utils/request'

// 查询人脸属性列表
export function listFaceAttr(query) {
  return request({
    url: '/system/faceAttr/list',
    method: 'get',
    params: query
  })
}

// 查询人脸属性详细
export function getFaceAttr(id) {
  return request({
    url: '/system/faceAttr/' + id,
    method: 'get'
  })
}

// 查询人脸属性详细
export function getFaceAttrByEventId(id) {
  return request({
    url: '/system/faceAttr/detail/' + id,
    method: 'get'
  })
}


// 新增人脸属性
export function addFaceAttr(data) {
  return request({
    url: '/system/faceAttr',
    method: 'post',
    data: data
  })
}

// 修改人脸属性
export function updateFaceAttr(data) {
  return request({
    url: '/system/faceAttr',
    method: 'put',
    data: data
  })
}

// 删除人脸属性
export function delFaceAttr(id) {
  return request({
    url: '/system/faceAttr/' + id,
    method: 'delete'
  })
}
