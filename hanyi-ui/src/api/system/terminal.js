import request from '@/utils/request'

// 查询终端管理列表
export function listTerminal(query) {
  return request({
    url: '/system/terminal/list',
    method: 'get',
    params: query
  })
}

// 查询终端管理详细
export function getTerminal(terminalId) {
  return request({
    url: '/system/terminal/' + terminalId,
    method: 'get'
  })
}

// 新增终端管理
export function addTerminal(data) {
  return request({
    url: '/system/terminal',
    method: 'post',
    data: data
  })
}

// 修改终端管理
export function updateTerminal(data) {
  return request({
    url: '/system/terminal',
    method: 'put',
    data: data
  })
}

// 删除终端管理
export function delTerminal(terminalId) {
  return request({
    url: '/system/terminal/' + terminalId,
    method: 'delete'
  })
}
