import request from '@/utils/request'

// 查询任务结果列表
export function listAnalysisResult(query) {
  return request({
    url: '/system/analysisResult/list',
    method: 'get',
    params: query
  })
}

// 查询任务结果详细
export function getAnalysisResult(id) {
  return request({
    url: '/system/analysisResult/' + id,
    method: 'get'
  })
}

// 新增任务结果
export function addAnalysisResult(data) {
  return request({
    url: '/system/analysisResult',
    method: 'post',
    data: data
  })
}

// 修改任务结果
export function updateAnalysisResult(data) {
  return request({
    url: '/system/analysisResult',
    method: 'put',
    data: data
  })
}

// 删除任务结果
export function delAnalysisResult(id) {
  return request({
    url: '/system/analysisResult/' + id,
    method: 'delete'
  })
}

 
