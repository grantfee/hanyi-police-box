import request from '@/utils/request'

// 查询离线任务列表
export function listOfflineTask(query) {
  return request({
    url: '/system/offlineTask/list',
    method: 'get',
    params: query
  })
}

// 查询离线任务详细
export function getOfflineTask(taskId) {
  return request({
    url: '/system/offlineTask/' + taskId,
    method: 'get'
  })
}

// 新增离线任务
export function addOfflineTask(data) {
  return request({
    url: '/system/offlineTask',
    method: 'post',
    data: data
  })
}

// 修改离线任务
export function updateOfflineTask(data) {
  return request({
    url: '/system/offlineTask',
    method: 'put',
    data: data
  })
}

// 删除离线任务
export function delOfflineTask(taskId) {
  return request({
    url: '/system/offlineTask/' + taskId,
    method: 'delete'
  })
}

// 启动任务
export function startOfflineTask(taskId) {
  return request({
    url: '/system/offlineTask/start/' + taskId,
    method: 'get'
  })
}

// 停止任务
export function stopOfflineTask(taskId) {
  return request({
    url: '/system/offlineTask/stop/' + taskId,
    method: 'get'
  })
}