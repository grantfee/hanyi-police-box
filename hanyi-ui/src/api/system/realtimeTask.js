import request from '@/utils/request'

// 查询实时任务列表
export function listRealtimeTask(query) {
  return request({
    url: '/system/realtimeTask/list',
    method: 'get',
    params: query
  })
}

// 查询实时任务详细
export function getRealtimeTask(taskId) {
  return request({
    url: '/system/realtimeTask/' + taskId,
    method: 'get'
  })
}

// 新增实时任务
export function addRealtimeTask(data) {
  return request({
    url: '/system/realtimeTask',
    method: 'post',
    data: data
  })
}

// 修改实时任务
export function updateRealtimeTask(data) {
  return request({
    url: '/system/realtimeTask',
    method: 'put',
    data: data
  })
}

// 删除实时任务
export function delRealtimeTask(taskId) {
  return request({
    url: '/system/realtimeTask/' + taskId,
    method: 'delete'
  })
}


// 启动任务
export function startRealtimeTask(taskId) {
  return request({
    url: '/system/realtimeTask/start/' + taskId,
    method: 'get'
  })
}

// 停止任务
export function stopRealtimeTask(taskId) {
  return request({
    url: '/system/realtimeTask/stop/' + taskId,
    method: 'get'
  })
}