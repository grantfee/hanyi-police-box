package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysVehicleAttr;
import com.hanyi.system.domain.vo.SysVehicleAttrVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 车辆属性Mapper接口
 *
 * @author grantfee
 * @date 2024-01-08
 */
public interface SysVehicleAttrMapper extends BaseMapperPlus<SysVehicleAttrMapper, SysVehicleAttr, SysVehicleAttrVo> {

}
