package com.hanyi.system.mapper;

import com.hanyi.common.core.mapper.BaseMapperPlus;
import com.hanyi.system.domain.SysOssConfig;
import com.hanyi.system.domain.vo.SysOssConfigVo;

/**
 * 对象存储配置Mapper接口
 *
 * @author Grantfee Li
 * @author 孤舟烟雨
 * @date 2021-08-13
 */
public interface SysOssConfigMapper extends BaseMapperPlus<SysOssConfigMapper, SysOssConfig, SysOssConfigVo> {

}
