package com.hanyi.system.mapper;

import com.hanyi.common.core.mapper.BaseMapperPlus;
import com.hanyi.system.domain.SysConfig;

/**
 * 参数配置 数据层
 *
 * @author Grantfee Li
 */
public interface SysConfigMapper extends BaseMapperPlus<SysConfigMapper, SysConfig, SysConfig> {

}
