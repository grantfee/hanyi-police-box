package com.hanyi.system.mapper;

import com.hanyi.common.core.domain.entity.SysDictType;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author Grantfee Li
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictTypeMapper, SysDictType, SysDictType> {

}
