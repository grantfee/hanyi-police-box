package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysOfflineTask;
import com.hanyi.system.domain.vo.SysOfflineTaskVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 离线任务Mapper接口
 *
 * @author grantfee
 * @date 2023-11-05
 */
public interface SysOfflineTaskMapper extends BaseMapperPlus<SysOfflineTaskMapper, SysOfflineTask, SysOfflineTaskVo> {

}
