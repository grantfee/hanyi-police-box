package com.hanyi.system.mapper;

import com.hanyi.common.core.mapper.BaseMapperPlus;
import com.hanyi.system.domain.SysOss;
import com.hanyi.system.domain.vo.SysOssVo;

/**
 * 文件上传 数据层
 *
 * @author Grantfee Li
 */
public interface SysOssMapper extends BaseMapperPlus<SysOssMapper, SysOss, SysOssVo> {
}
