package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysTerminal;
import com.hanyi.system.domain.vo.SysTerminalVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 终端管理Mapper接口
 *
 * @author grantfee
 * @date 2023-11-21
 */
public interface SysTerminalMapper extends BaseMapperPlus<SysTerminalMapper, SysTerminal, SysTerminalVo> {

}
