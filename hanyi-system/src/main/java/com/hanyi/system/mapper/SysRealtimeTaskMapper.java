package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysRealtimeTask;
import com.hanyi.system.domain.vo.SysRealtimeTaskVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 实时任务Mapper接口
 *
 * @author grantfee
 * @date 2023-11-05
 */
public interface SysRealtimeTaskMapper extends BaseMapperPlus<SysRealtimeTaskMapper, SysRealtimeTask, SysRealtimeTaskVo> {

}
