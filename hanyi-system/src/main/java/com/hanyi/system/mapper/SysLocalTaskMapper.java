package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysLocalTask;
import com.hanyi.system.domain.vo.SysLocalTaskVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 本地任务Mapper接口
 *
 * @author grantfee
 * @date 2023-11-06
 */
public interface SysLocalTaskMapper extends BaseMapperPlus<SysLocalTaskMapper, SysLocalTask, SysLocalTaskVo> {

}
