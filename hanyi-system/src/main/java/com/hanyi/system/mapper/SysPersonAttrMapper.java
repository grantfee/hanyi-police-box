package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysPersonAttr;
import com.hanyi.system.domain.vo.SysPersonAttrVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 行人属性Mapper接口
 *
 * @author grantfee
 * @date 2024-01-08
 */
public interface SysPersonAttrMapper extends BaseMapperPlus<SysPersonAttrMapper, SysPersonAttr, SysPersonAttrVo> {

}
