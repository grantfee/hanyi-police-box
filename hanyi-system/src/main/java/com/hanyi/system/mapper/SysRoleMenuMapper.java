package com.hanyi.system.mapper;

import com.hanyi.common.core.mapper.BaseMapperPlus;
import com.hanyi.system.domain.SysRoleMenu;

/**
 * 角色与菜单关联表 数据层
 *
 * @author Grantfee Li
 */
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenuMapper, SysRoleMenu, SysRoleMenu> {

}
