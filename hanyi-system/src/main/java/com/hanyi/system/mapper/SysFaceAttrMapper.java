package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysFaceAttr;
import com.hanyi.system.domain.vo.SysFaceAttrVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;

/**
 * 人脸属性Mapper接口
 *
 * @author grantfee
 * @date 2024-01-08
 */
public interface SysFaceAttrMapper extends BaseMapperPlus<SysFaceAttrMapper, SysFaceAttr, SysFaceAttrVo> {

}
