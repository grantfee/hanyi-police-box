package com.hanyi.system.mapper;

import com.hanyi.system.domain.SysAnalysisResult;
import com.hanyi.system.domain.SysEventDay;
import com.hanyi.system.domain.SysEventHour;
import com.hanyi.system.domain.vo.SysAnalysisResultVo;
import com.hanyi.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 任务结果Mapper接口
 *
 * @author grantfee
 * @date 2023-11-06
 */
public interface SysAnalysisResultMapper extends BaseMapperPlus<SysAnalysisResultMapper, SysAnalysisResult, SysAnalysisResultVo> {
    @Select("select DATE_FORMAT(create_time, '%H:00') AS time, count(id) AS count " +
        "from sys_analysis_result " +
        "WHERE dept_id=#{deptId} and event_type=#{eventType} and create_time BETWEEN  #{startTime} AND #{endTime} " +
        "GROUP BY time " +
        "ORDER BY time")
    public List<SysEventHour> selectCountByHours(@Param("deptId") long deptId,
                                                 @Param("eventType") String eventType,
                                                 @Param("startTime") String startTime,
                                                 @Param("endTime") String endTime);


    @Select("SELECT DATE_FORMAT( create_time, '%d' ) AS day," +
        " count(id) AS count " +
        " FROM sys_analysis_result" +
        " WHERE dept_id=#{deptId} and event_type=#{eventType} and create_time BETWEEN #{startTime} AND #{endTime} " +
        " GROUP BY day")
    public List<SysEventDay> selectCountByDays(@Param("deptId") long deptId,
                                                @Param("eventType") String eventType,
                                               @Param("startTime") String startTime,
                                               @Param("endTime") String endTime);
}
