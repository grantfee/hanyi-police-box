package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hanyi.system.domain.SysOfflineTask;
import com.hanyi.system.domain.SysRealtimeTask;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysLocalTaskBo;
import com.hanyi.system.domain.vo.SysLocalTaskVo;
import com.hanyi.system.domain.SysLocalTask;
import com.hanyi.system.mapper.SysLocalTaskMapper;
import com.hanyi.system.service.ISysLocalTaskService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 本地任务Service业务层处理
 *
 * @author grantfee
 * @date 2023-11-06
 */
@RequiredArgsConstructor
@Service
public class SysLocalTaskServiceImpl implements ISysLocalTaskService {

    private final SysLocalTaskMapper baseMapper;

    @Override
    public SysLocalTask queryByTaskId(Long taskId) {
        return  baseMapper.selectById(taskId);
    }

    @Override
    public Boolean updateByEntity(SysLocalTask update) {
        return baseMapper.updateById(update) > 0;
    }


    /**
     * 查询本地任务
     */
    @Override
    public SysLocalTaskVo queryById(Long taskId){
        return baseMapper.selectVoById(taskId);
    }

    /**
     * 查询本地任务列表
     */
    @Override
    public TableDataInfo<SysLocalTaskVo> queryPageList(SysLocalTaskBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysLocalTask> lqw = buildQueryWrapper(bo);
        Page<SysLocalTaskVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询本地任务列表
     */
    @Override
    public List<SysLocalTaskVo> queryList(SysLocalTaskBo bo) {
        LambdaQueryWrapper<SysLocalTask> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    @Override
    public List<SysLocalTask> queryList() {
        return baseMapper.selectList();
    }

    private LambdaQueryWrapper<SysLocalTask> buildQueryWrapper(SysLocalTaskBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysLocalTask> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysLocalTask::getName, bo.getName());
        lqw.like(StringUtils.isNotBlank(bo.getVideoName()), SysLocalTask::getVideoName, bo.getVideoName());
        lqw.eq(bo.getStartTime() != null, SysLocalTask::getStartTime, bo.getStartTime());
        lqw.eq(bo.getEndTime() != null, SysLocalTask::getEndTime, bo.getEndTime());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysLocalTask::getStatus, bo.getStatus());
        lqw.eq(bo.getDeptId() != null, SysLocalTask::getDeptId, bo.getDeptId());
        return lqw;
    }

    /**
     * 新增本地任务
     */
    @Override
    public Boolean insertByBo(SysLocalTaskBo bo) {
        SysLocalTask add = BeanUtil.toBean(bo, SysLocalTask.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTaskId(add.getTaskId());
        }
        return flag;
    }

    /**
     * 修改本地任务
     */
    @Override
    public Boolean updateByBo(SysLocalTaskBo bo) {
        SysLocalTask update = BeanUtil.toBean(bo, SysLocalTask.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysLocalTask entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除本地任务
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
