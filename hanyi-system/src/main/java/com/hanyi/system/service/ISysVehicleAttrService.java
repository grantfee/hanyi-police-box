package com.hanyi.system.service;

import com.hanyi.system.domain.SysVehicleAttr;
import com.hanyi.system.domain.vo.SysVehicleAttrVo;
import com.hanyi.system.domain.bo.SysVehicleAttrBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 车辆属性Service接口
 *
 * @author grantfee
 * @date 2024-01-08
 */
public interface ISysVehicleAttrService {

    /**
     * 查询车辆属性
     */
    SysVehicleAttrVo queryById(Long id);

    SysVehicleAttrVo queryByEventId(Long id);
    /**
     * 查询车辆属性列表
     */
    TableDataInfo<SysVehicleAttrVo> queryPageList(SysVehicleAttrBo bo, PageQuery pageQuery);

    /**
     * 查询车辆属性列表
     */
    List<SysVehicleAttrVo> queryList(SysVehicleAttrBo bo);

    /**
     * 新增车辆属性
     */
    Boolean insertByBo(SysVehicleAttrBo bo);

    /**
     * 修改车辆属性
     */
    Boolean updateByBo(SysVehicleAttrBo bo);

    /**
     * 校验并批量删除车辆属性信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
