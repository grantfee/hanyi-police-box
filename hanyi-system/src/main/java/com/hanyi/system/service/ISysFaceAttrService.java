package com.hanyi.system.service;

import com.hanyi.system.domain.SysFaceAttr;
import com.hanyi.system.domain.vo.SysFaceAttrVo;
import com.hanyi.system.domain.bo.SysFaceAttrBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 人脸属性Service接口
 *
 * @author grantfee
 * @date 2024-01-08
 */
public interface ISysFaceAttrService {

    /**
     * 查询人脸属性
     */
    SysFaceAttrVo queryById(Long id);

    SysFaceAttrVo queryByEventId(Long eventId);
    /**
     * 查询人脸属性列表
     */
    TableDataInfo<SysFaceAttrVo> queryPageList(SysFaceAttrBo bo, PageQuery pageQuery);

    /**
     * 查询人脸属性列表
     */
    List<SysFaceAttrVo> queryList(SysFaceAttrBo bo);

    /**
     * 新增人脸属性
     */
    Boolean insertByBo(SysFaceAttrBo bo);

    /**
     * 修改人脸属性
     */
    Boolean updateByBo(SysFaceAttrBo bo);

    /**
     * 校验并批量删除人脸属性信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
