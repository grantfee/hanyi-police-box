package com.hanyi.system.service;

import com.hanyi.system.domain.SysAnalysisResult;
import com.hanyi.system.domain.SysEventDay;
import com.hanyi.system.domain.SysEventHour;
import com.hanyi.system.domain.vo.SysAnalysisResultVo;
import com.hanyi.system.domain.bo.SysAnalysisResultBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 任务结果Service接口
 *
 * @author grantfee
 * @date 2023-11-06
 */
public interface ISysAnalysisResultService {

    /**
     * 查询任务结果
     */
    SysAnalysisResultVo queryById(Long id);

    /**
     * 查询任务结果列表
     */
    TableDataInfo<SysAnalysisResultVo> queryPageList(SysAnalysisResultBo bo, PageQuery pageQuery);

    /**
     * 查询任务结果列表
     */
    List<SysAnalysisResultVo> queryList(SysAnalysisResultBo bo);

    /**
     * 新增任务结果
     */
    Boolean insertByBo(SysAnalysisResultBo bo);

    /**
     * 修改任务结果
     */
    Boolean updateByBo(SysAnalysisResultBo bo);

    /**
     * 校验并批量删除任务结果信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Long selectCountByVehicle(long deptId);
    Long selectCountByPerson(long deptId);
    Long selectCountByFace(long deptId);

    List<SysEventHour> selectCountByHours(long deptId,String eventType,String startTime,String endTime);

    List<SysEventDay> selectCountByDays(long deptId,String eventType,String startTime,String endTime);
}
