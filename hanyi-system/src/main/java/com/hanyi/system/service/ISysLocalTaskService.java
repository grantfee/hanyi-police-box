package com.hanyi.system.service;

import com.hanyi.system.domain.SysLocalTask;
import com.hanyi.system.domain.SysOfflineTask;
import com.hanyi.system.domain.SysRealtimeTask;
import com.hanyi.system.domain.vo.SysLocalTaskVo;
import com.hanyi.system.domain.bo.SysLocalTaskBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 本地任务Service接口
 *
 * @author grantfee
 * @date 2023-11-06
 */
public interface ISysLocalTaskService {

    SysLocalTask queryByTaskId(Long taskId);

    Boolean updateByEntity(SysLocalTask task);
    /**
     * 查询本地任务
     */
    SysLocalTaskVo queryById(Long taskId);

    /**
     * 查询本地任务列表
     */
    TableDataInfo<SysLocalTaskVo> queryPageList(SysLocalTaskBo bo, PageQuery pageQuery);

    /**
     * 查询本地任务列表
     */
    List<SysLocalTaskVo> queryList(SysLocalTaskBo bo);

    List<SysLocalTask> queryList();
    /**
     * 新增本地任务
     */
    Boolean insertByBo(SysLocalTaskBo bo);

    /**
     * 修改本地任务
     */
    Boolean updateByBo(SysLocalTaskBo bo);

    /**
     * 校验并批量删除本地任务信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
