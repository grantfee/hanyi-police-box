package com.hanyi.system.service;

import com.hanyi.system.domain.SysPersonAttr;
import com.hanyi.system.domain.vo.SysPersonAttrVo;
import com.hanyi.system.domain.bo.SysPersonAttrBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.system.domain.vo.SysVehicleAttrVo;

import java.util.Collection;
import java.util.List;

/**
 * 行人属性Service接口
 *
 * @author grantfee
 * @date 2024-01-08
 */
public interface ISysPersonAttrService {

    /**
     * 查询行人属性
     */
    SysPersonAttrVo queryById(Long id);

    SysPersonAttrVo queryByEventId(Long id);

    /**
     * 查询行人属性列表
     */
    TableDataInfo<SysPersonAttrVo> queryPageList(SysPersonAttrBo bo, PageQuery pageQuery);

    /**
     * 查询行人属性列表
     */
    List<SysPersonAttrVo> queryList(SysPersonAttrBo bo);

    /**
     * 新增行人属性
     */
    Boolean insertByBo(SysPersonAttrBo bo);

    /**
     * 修改行人属性
     */
    Boolean updateByBo(SysPersonAttrBo bo);

    /**
     * 校验并批量删除行人属性信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
