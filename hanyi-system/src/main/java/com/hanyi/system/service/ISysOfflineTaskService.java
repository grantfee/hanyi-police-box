package com.hanyi.system.service;

import com.hanyi.system.domain.SysOfflineTask;
import com.hanyi.system.domain.SysRealtimeTask;
import com.hanyi.system.domain.vo.SysOfflineTaskVo;
import com.hanyi.system.domain.bo.SysOfflineTaskBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 离线任务Service接口
 *
 * @author grantfee
 * @date 2023-11-05
 */
public interface ISysOfflineTaskService {

    SysOfflineTask queryByTaskId(Long taskId);

    Boolean updateByEntity(SysOfflineTask task);

    /**
     * 查询离线任务
     */
    SysOfflineTaskVo queryById(Long taskId);

    /**
     * 查询离线任务列表
     */
    TableDataInfo<SysOfflineTaskVo> queryPageList(SysOfflineTaskBo bo, PageQuery pageQuery);

    /**
     * 查询离线任务列表
     */
    List<SysOfflineTaskVo> queryList(SysOfflineTaskBo bo);

    List<SysOfflineTask> queryList();
    /**
     * 新增离线任务
     */
    Boolean insertByBo(SysOfflineTaskBo bo);

    /**
     * 修改离线任务
     */
    Boolean updateByBo(SysOfflineTaskBo bo);

    /**
     * 校验并批量删除离线任务信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
