package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hanyi.system.domain.SysOfflineTask;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysRealtimeTaskBo;
import com.hanyi.system.domain.vo.SysRealtimeTaskVo;
import com.hanyi.system.domain.SysRealtimeTask;
import com.hanyi.system.mapper.SysRealtimeTaskMapper;
import com.hanyi.system.service.ISysRealtimeTaskService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 实时任务Service业务层处理
 *
 * @author grantfee
 * @date 2023-11-05
 */
@RequiredArgsConstructor
@Service
public class SysRealtimeTaskServiceImpl implements ISysRealtimeTaskService {

    private final SysRealtimeTaskMapper baseMapper;

    @Override
    public SysRealtimeTask queryByTaskId(Long taskId) {
        return  baseMapper.selectById(taskId);
    }

    @Override
    public Boolean updateByEntity(SysRealtimeTask update) {
        return baseMapper.updateById(update) > 0;
    }

    @Override
    public Boolean updateTaskStatusById(Long taskId, String status) {
        SysRealtimeTask task = baseMapper.selectById(taskId);
        task.setStatus(status);
        return baseMapper.updateById(task)>0;
    }

    /**
     * 查询实时任务
     */
    @Override
    public SysRealtimeTaskVo queryById(Long taskId){
        return baseMapper.selectVoById(taskId);
    }

    /**
     * 查询实时任务列表
     */
    @Override
    public TableDataInfo<SysRealtimeTaskVo> queryPageList(SysRealtimeTaskBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysRealtimeTask> lqw = buildQueryWrapper(bo);
        Page<SysRealtimeTaskVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询实时任务列表
     */
    @Override
    public List<SysRealtimeTaskVo> queryList(SysRealtimeTaskBo bo) {
        LambdaQueryWrapper<SysRealtimeTask> lqw = buildQueryWrapper(bo);

        return baseMapper.selectVoList(lqw);
    }

    @Override
    public List<SysRealtimeTask> queryList() {
        return baseMapper.selectList();
    }

    private LambdaQueryWrapper<SysRealtimeTask> buildQueryWrapper(SysRealtimeTaskBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysRealtimeTask> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysRealtimeTask::getName, bo.getName());
        lqw.like(StringUtils.isNotBlank(bo.getIpcName()), SysRealtimeTask::getIpcName, bo.getIpcName());
        lqw.eq(StringUtils.isNotBlank(bo.getIpcPosition()), SysRealtimeTask::getIpcPosition, bo.getIpcPosition());
        lqw.eq(bo.getStartTime() != null, SysRealtimeTask::getStartTime, bo.getStartTime());
        lqw.eq(bo.getEndTime() != null, SysRealtimeTask::getEndTime, bo.getEndTime());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysRealtimeTask::getStatus, bo.getStatus());
        lqw.eq(bo.getDeptId() != null, SysRealtimeTask::getDeptId, bo.getDeptId());
        return lqw;
    }

    /**
     * 新增实时任务
     */
    @Override
    public Boolean insertByBo(SysRealtimeTaskBo bo) {
        SysRealtimeTask add = BeanUtil.toBean(bo, SysRealtimeTask.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTaskId(add.getTaskId());
        }
        return flag;
    }

    /**
     * 修改实时任务
     */
    @Override
    public Boolean updateByBo(SysRealtimeTaskBo bo) {
        SysRealtimeTask update = BeanUtil.toBean(bo, SysRealtimeTask.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysRealtimeTask entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除实时任务
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }


}
