package com.hanyi.system.service;

import com.hanyi.common.core.domain.PageQuery;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.system.domain.SysOss;
import com.hanyi.system.domain.bo.SysOssBo;
import com.hanyi.system.domain.vo.SysOssVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;

/**
 * 文件上传 服务层
 *
 * @author Grantfee Li
 */
public interface ISysOssService {

    TableDataInfo<SysOssVo> queryPageList(SysOssBo sysOss, PageQuery pageQuery);

    List<SysOssVo> listByIds(Collection<Long> ossIds);

    SysOssVo getById(Long ossId);

    SysOss upload(MultipartFile file);

    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

}
