package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysVehicleAttrBo;
import com.hanyi.system.domain.vo.SysVehicleAttrVo;
import com.hanyi.system.domain.SysVehicleAttr;
import com.hanyi.system.mapper.SysVehicleAttrMapper;
import com.hanyi.system.service.ISysVehicleAttrService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 车辆属性Service业务层处理
 *
 * @author grantfee
 * @date 2024-01-08
 */
@RequiredArgsConstructor
@Service
public class SysVehicleAttrServiceImpl implements ISysVehicleAttrService {

    private final SysVehicleAttrMapper baseMapper;

    /**
     * 查询车辆属性
     */
    @Override
    public SysVehicleAttrVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }


    @Override
    public SysVehicleAttrVo queryByEventId(Long eventId) {
        LambdaQueryWrapper<SysVehicleAttr> lqw = Wrappers.lambdaQuery();
        lqw.eq(eventId != null, SysVehicleAttr::getResultId, eventId);
        return baseMapper.selectVoOne(lqw);
    }

    /**
     * 查询车辆属性列表
     */
    @Override
    public TableDataInfo<SysVehicleAttrVo> queryPageList(SysVehicleAttrBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysVehicleAttr> lqw = buildQueryWrapper(bo);
        Page<SysVehicleAttrVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询车辆属性列表
     */
    @Override
    public List<SysVehicleAttrVo> queryList(SysVehicleAttrBo bo) {
        LambdaQueryWrapper<SysVehicleAttr> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysVehicleAttr> buildQueryWrapper(SysVehicleAttrBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysVehicleAttr> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getResultId() != null, SysVehicleAttr::getResultId, bo.getResultId());
        lqw.eq(StringUtils.isNotBlank(bo.getVehicleType()), SysVehicleAttr::getVehicleType, bo.getVehicleType());
        lqw.eq(bo.getVehicleTypeScore() != null, SysVehicleAttr::getVehicleTypeScore, bo.getVehicleTypeScore());
        lqw.eq(StringUtils.isNotBlank(bo.getColor()), SysVehicleAttr::getColor, bo.getColor());
        lqw.eq(bo.getColorScore() != null, SysVehicleAttr::getColorScore, bo.getColorScore());
        lqw.eq(StringUtils.isNotBlank(bo.getLicense()), SysVehicleAttr::getLicense, bo.getLicense());
        lqw.eq(bo.getLicenseScore() != null, SysVehicleAttr::getLicenseScore, bo.getLicenseScore());
        lqw.eq(StringUtils.isNotBlank(bo.getBrand()), SysVehicleAttr::getBrand, bo.getBrand());
        lqw.eq(bo.getBrandScore() != null, SysVehicleAttr::getBrandScore, bo.getBrandScore());
        lqw.eq(bo.getDeptId() != null, SysVehicleAttr::getDeptId, bo.getDeptId());
        return lqw;
    }

    /**
     * 新增车辆属性
     */
    @Override
    public Boolean insertByBo(SysVehicleAttrBo bo) {
        SysVehicleAttr add = BeanUtil.toBean(bo, SysVehicleAttr.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改车辆属性
     */
    @Override
    public Boolean updateByBo(SysVehicleAttrBo bo) {
        SysVehicleAttr update = BeanUtil.toBean(bo, SysVehicleAttr.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysVehicleAttr entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除车辆属性
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
