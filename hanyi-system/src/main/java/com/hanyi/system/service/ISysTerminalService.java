package com.hanyi.system.service;

import com.hanyi.system.domain.SysTerminal;
import com.hanyi.system.domain.vo.SysTerminalVo;
import com.hanyi.system.domain.bo.SysTerminalBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 终端管理Service接口
 *
 * @author grantfee
 * @date 2023-11-21
 */
public interface ISysTerminalService {

    /**
     * 查询终端管理
     */
    SysTerminalVo queryById(Long terminalId);

    /**
     * 查询终端管理列表
     */
    TableDataInfo<SysTerminalVo> queryPageList(SysTerminalBo bo, PageQuery pageQuery);

    /**
     * 查询终端管理列表
     */
    List<SysTerminalVo> queryList(SysTerminalBo bo);

    /**
     * 新增终端管理
     */
    Boolean insertByBo(SysTerminalBo bo);

    /**
     * 修改终端管理
     */
    Boolean updateByBo(SysTerminalBo bo);

    /**
     * 校验并批量删除终端管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
