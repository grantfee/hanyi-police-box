package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hanyi.system.domain.SysVehicleAttr;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysPersonAttrBo;
import com.hanyi.system.domain.vo.SysPersonAttrVo;
import com.hanyi.system.domain.SysPersonAttr;
import com.hanyi.system.mapper.SysPersonAttrMapper;
import com.hanyi.system.service.ISysPersonAttrService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 行人属性Service业务层处理
 *
 * @author grantfee
 * @date 2024-01-08
 */
@RequiredArgsConstructor
@Service
public class SysPersonAttrServiceImpl implements ISysPersonAttrService {

    private final SysPersonAttrMapper baseMapper;

    /**
     * 查询行人属性
     */
    @Override
    public SysPersonAttrVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }


    @Override
    public SysPersonAttrVo queryByEventId(Long eventId) {
        LambdaQueryWrapper<SysPersonAttr> lqw = Wrappers.lambdaQuery();
        lqw.eq(eventId != null, SysPersonAttr::getResultId, eventId);
        return baseMapper.selectVoOne(lqw);
    }

    /**
     * 查询行人属性列表
     */
    @Override
    public TableDataInfo<SysPersonAttrVo> queryPageList(SysPersonAttrBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysPersonAttr> lqw = buildQueryWrapper(bo);
        Page<SysPersonAttrVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询行人属性列表
     */
    @Override
    public List<SysPersonAttrVo> queryList(SysPersonAttrBo bo) {
        LambdaQueryWrapper<SysPersonAttr> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysPersonAttr> buildQueryWrapper(SysPersonAttrBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysPersonAttr> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getResultId() != null, SysPersonAttr::getResultId, bo.getResultId());
        lqw.eq(StringUtils.isNotBlank(bo.getGender()), SysPersonAttr::getGender, bo.getGender());
        lqw.eq(bo.getGenderScore() != null, SysPersonAttr::getGenderScore, bo.getGenderScore());
        lqw.eq(StringUtils.isNotBlank(bo.getAge()), SysPersonAttr::getAge, bo.getAge());
        lqw.eq(bo.getAgeScore() != null, SysPersonAttr::getAgeScore, bo.getAgeScore());
        lqw.eq(StringUtils.isNotBlank(bo.getUpperColor()), SysPersonAttr::getUpperColor, bo.getUpperColor());
        lqw.eq(bo.getUpperColorScore() != null, SysPersonAttr::getUpperColorScore, bo.getUpperColorScore());
        lqw.eq(StringUtils.isNotBlank(bo.getBottomColor()), SysPersonAttr::getBottomColor, bo.getBottomColor());
        lqw.eq(bo.getBottomColorScore() != null, SysPersonAttr::getBottomColorScore, bo.getBottomColorScore());
        lqw.eq(StringUtils.isNotBlank(bo.getUpperType()), SysPersonAttr::getUpperType, bo.getUpperType());
        lqw.eq(bo.getUpperTypeScore() != null, SysPersonAttr::getUpperTypeScore, bo.getUpperTypeScore());
        lqw.eq(StringUtils.isNotBlank(bo.getBottomType()), SysPersonAttr::getBottomType, bo.getBottomType());
        lqw.eq(bo.getBottomTypeScore() != null, SysPersonAttr::getBottomTypeScore, bo.getBottomTypeScore());
        lqw.eq(StringUtils.isNotBlank(bo.getHair()), SysPersonAttr::getHair, bo.getHair());
        lqw.eq(bo.getHairScore() != null, SysPersonAttr::getHairScore, bo.getHairScore());
        lqw.eq(StringUtils.isNotBlank(bo.getHat()), SysPersonAttr::getHat, bo.getHat());
        lqw.eq(bo.getHatScore() != null, SysPersonAttr::getHatScore, bo.getHatScore());
        lqw.eq(StringUtils.isNotBlank(bo.getMask()), SysPersonAttr::getMask, bo.getMask());
        lqw.eq(bo.getMaskScore() != null, SysPersonAttr::getMaskScore, bo.getMaskScore());
        lqw.eq(StringUtils.isNotBlank(bo.getGlass()), SysPersonAttr::getGlass, bo.getGlass());
        lqw.eq(bo.getGlassScore() != null, SysPersonAttr::getGlassScore, bo.getGlassScore());
        lqw.eq(bo.getDeptId() != null, SysPersonAttr::getDeptId, bo.getDeptId());
        return lqw;
    }

    /**
     * 新增行人属性
     */
    @Override
    public Boolean insertByBo(SysPersonAttrBo bo) {
        SysPersonAttr add = BeanUtil.toBean(bo, SysPersonAttr.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改行人属性
     */
    @Override
    public Boolean updateByBo(SysPersonAttrBo bo) {
        SysPersonAttr update = BeanUtil.toBean(bo, SysPersonAttr.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysPersonAttr entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除行人属性
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
