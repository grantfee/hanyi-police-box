package com.hanyi.system.service;

import com.hanyi.system.domain.SysRealtimeTask;
import com.hanyi.system.domain.vo.SysRealtimeTaskVo;
import com.hanyi.system.domain.bo.SysRealtimeTaskBo;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 实时任务Service接口
 *
 * @author grantfee
 * @date 2023-11-05
 */
public interface ISysRealtimeTaskService {

    SysRealtimeTask queryByTaskId(Long taskId);

    Boolean updateTaskStatusById(Long taskId,String status);

    Boolean updateByEntity(SysRealtimeTask task);
    /**
     * 查询实时任务
     */
    SysRealtimeTaskVo queryById(Long taskId);

    /**
     * 查询实时任务列表
     */
    TableDataInfo<SysRealtimeTaskVo> queryPageList(SysRealtimeTaskBo bo, PageQuery pageQuery);

    /**
     * 查询实时任务列表
     */
    List<SysRealtimeTaskVo> queryList(SysRealtimeTaskBo bo);

    List<SysRealtimeTask> queryList();

    /**
     * 新增实时任务
     */
    Boolean insertByBo(SysRealtimeTaskBo bo);

    /**
     * 修改实时任务
     */
    Boolean updateByBo(SysRealtimeTaskBo bo);

    /**
     * 校验并批量删除实时任务信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
