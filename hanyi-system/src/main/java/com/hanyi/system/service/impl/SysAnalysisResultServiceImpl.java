package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hanyi.system.domain.SysEventDay;
import com.hanyi.system.domain.SysEventHour;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysAnalysisResultBo;
import com.hanyi.system.domain.vo.SysAnalysisResultVo;
import com.hanyi.system.domain.SysAnalysisResult;
import com.hanyi.system.mapper.SysAnalysisResultMapper;
import com.hanyi.system.service.ISysAnalysisResultService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 任务结果Service业务层处理
 *
 * @author grantfee
 * @date 2023-11-06
 */
@RequiredArgsConstructor
@Service
public class SysAnalysisResultServiceImpl implements ISysAnalysisResultService {

    private final SysAnalysisResultMapper baseMapper;

    /**
     * 查询任务结果
     */
    @Override
    public SysAnalysisResultVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询任务结果列表
     */
    @Override
    public TableDataInfo<SysAnalysisResultVo> queryPageList(SysAnalysisResultBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysAnalysisResult> lqw = buildQueryWrapper(bo);
        Page<SysAnalysisResultVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询任务结果列表
     */
    @Override
    public List<SysAnalysisResultVo> queryList(SysAnalysisResultBo bo) {
        LambdaQueryWrapper<SysAnalysisResult> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysAnalysisResult> buildQueryWrapper(SysAnalysisResultBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysAnalysisResult> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getTaskName()), SysAnalysisResult::getTaskName, bo.getTaskName());
        lqw.eq(StringUtils.isNotBlank(bo.getTaskType()), SysAnalysisResult::getTaskType, bo.getTaskType());
        lqw.eq(StringUtils.isNotBlank(bo.getEventType()), SysAnalysisResult::getEventType, bo.getEventType());
        lqw.eq(StringUtils.isNotBlank(bo.getEventTag()), SysAnalysisResult::getEventTag, bo.getEventTag());
        lqw.ge(bo.getEventStartTime() != null,SysAnalysisResult::getEventStartTime,bo.getEventStartTime());
        lqw.le(bo.getEventEndTime() != null,SysAnalysisResult::getEventStartTime,bo.getEventEndTime());
        lqw.eq(bo.getDeptId() != null, SysAnalysisResult::getDeptId, bo.getDeptId());
        lqw.orderByDesc(SysAnalysisResult::getCreateTime);

        return lqw;
    }

    /**
     * 新增任务结果
     */
    @Override
    public Boolean insertByBo(SysAnalysisResultBo bo) {
        SysAnalysisResult add = BeanUtil.toBean(bo, SysAnalysisResult.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改任务结果
     */
    @Override
    public Boolean updateByBo(SysAnalysisResultBo bo) {
        SysAnalysisResult update = BeanUtil.toBean(bo, SysAnalysisResult.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysAnalysisResult entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除任务结果
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Long selectCountByVehicle(long deptId) {
        LambdaQueryWrapper<SysAnalysisResult> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysAnalysisResult::getEventType, "1");
        lqw.eq(SysAnalysisResult::getDeptId,deptId);
        return baseMapper.selectCount(lqw);
    }

    @Override
    public Long selectCountByPerson(long deptId) {
        LambdaQueryWrapper<SysAnalysisResult> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysAnalysisResult::getEventType, "0");
        lqw.eq(SysAnalysisResult::getDeptId,deptId);
        return baseMapper.selectCount(lqw);
    }

    @Override
    public Long selectCountByFace(long deptId) {
        LambdaQueryWrapper<SysAnalysisResult> lqw = Wrappers.lambdaQuery();
        lqw.eq(SysAnalysisResult::getEventType, "2");
        return baseMapper.selectCount(lqw);
    }

    @Override
    public List<SysEventHour> selectCountByHours(long deptId,String eventType,String startTime,String endTime) {
        return baseMapper.selectCountByHours(deptId,eventType,startTime,endTime);
    }

    @Override
    public List<SysEventDay> selectCountByDays(long deptId,String eventType, String startTime, String endTime) {
        return baseMapper.selectCountByDays(deptId,eventType,startTime,endTime);
    }
}
