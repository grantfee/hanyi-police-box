package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hanyi.system.domain.SysLocalTask;
import com.hanyi.system.domain.SysRealtimeTask;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysOfflineTaskBo;
import com.hanyi.system.domain.vo.SysOfflineTaskVo;
import com.hanyi.system.domain.SysOfflineTask;
import com.hanyi.system.mapper.SysOfflineTaskMapper;
import com.hanyi.system.service.ISysOfflineTaskService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 离线任务Service业务层处理
 *
 * @author grantfee
 * @date 2023-11-05
 */
@RequiredArgsConstructor
@Service
public class SysOfflineTaskServiceImpl implements ISysOfflineTaskService {

    private final SysOfflineTaskMapper baseMapper;

    @Override
    public SysOfflineTask queryByTaskId(Long taskId) {
        return  baseMapper.selectById(taskId);
    }

    @Override
    public Boolean updateByEntity(SysOfflineTask update) {
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 查询离线任务
     */
    @Override
    public SysOfflineTaskVo queryById(Long taskId){
        return baseMapper.selectVoById(taskId);
    }

    /**
     * 查询离线任务列表
     */
    @Override
    public TableDataInfo<SysOfflineTaskVo> queryPageList(SysOfflineTaskBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysOfflineTask> lqw = buildQueryWrapper(bo);
        Page<SysOfflineTaskVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询离线任务列表
     */
    @Override
    public List<SysOfflineTaskVo> queryList(SysOfflineTaskBo bo) {
        LambdaQueryWrapper<SysOfflineTask> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    @Override
    public List<SysOfflineTask> queryList() {
        return baseMapper.selectList();
    }

    private LambdaQueryWrapper<SysOfflineTask> buildQueryWrapper(SysOfflineTaskBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysOfflineTask> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), SysOfflineTask::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getStreamStatus()), SysOfflineTask::getStreamStatus, bo.getStreamStatus());
        lqw.eq(bo.getStartTime() != null, SysOfflineTask::getStartTime, bo.getStartTime());
        lqw.eq(bo.getEndTime() != null, SysOfflineTask::getEndTime, bo.getEndTime());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysOfflineTask::getStatus, bo.getStatus());
        lqw.eq(bo.getDeptId() != null, SysOfflineTask::getDeptId, bo.getDeptId());
        return lqw;
    }

    /**
     * 新增离线任务
     */
    @Override
    public Boolean insertByBo(SysOfflineTaskBo bo) {
        SysOfflineTask add = BeanUtil.toBean(bo, SysOfflineTask.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTaskId(add.getTaskId());
        }
        return flag;
    }

    /**
     * 修改离线任务
     */
    @Override
    public Boolean updateByBo(SysOfflineTaskBo bo) {
        SysOfflineTask update = BeanUtil.toBean(bo, SysOfflineTask.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysOfflineTask entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除离线任务
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
