package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hanyi.system.domain.SysRealtimeTask;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysTerminalBo;
import com.hanyi.system.domain.vo.SysTerminalVo;
import com.hanyi.system.domain.SysTerminal;
import com.hanyi.system.mapper.SysTerminalMapper;
import com.hanyi.system.service.ISysTerminalService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 终端管理Service业务层处理
 *
 * @author grantfee
 * @date 2023-11-21
 */
@RequiredArgsConstructor
@Service
public class SysTerminalServiceImpl implements ISysTerminalService {

    private final SysTerminalMapper baseMapper;

    /**
     * 查询终端管理
     */
    @Override
    public SysTerminalVo queryById(Long terminalId){
        return baseMapper.selectVoById(terminalId);
    }

    /**
     * 查询终端管理列表
     */
    @Override
    public TableDataInfo<SysTerminalVo> queryPageList(SysTerminalBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysTerminal> lqw = buildQueryWrapper(bo);
        Page<SysTerminalVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询终端管理列表
     */
    @Override
    public List<SysTerminalVo> queryList(SysTerminalBo bo) {
        LambdaQueryWrapper<SysTerminal> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysTerminal> buildQueryWrapper(SysTerminalBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysTerminal> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getTerminalName()), SysTerminal::getTerminalName, bo.getTerminalName());
        lqw.eq(bo.getDeptId() != null, SysTerminal::getDeptId, bo.getDeptId());
        return lqw;
    }

    /**
     * 新增终端管理
     */
    @Override
    public Boolean insertByBo(SysTerminalBo bo) {
        SysTerminal add = BeanUtil.toBean(bo, SysTerminal.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTerminalId(add.getTerminalId());
        }
        return flag;
    }

    /**
     * 修改终端管理
     */
    @Override
    public Boolean updateByBo(SysTerminalBo bo) {
        SysTerminal update = BeanUtil.toBean(bo, SysTerminal.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysTerminal entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除终端管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
