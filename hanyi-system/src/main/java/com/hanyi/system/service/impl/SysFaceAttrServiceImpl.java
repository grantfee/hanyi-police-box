package com.hanyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.hanyi.common.utils.StringUtils;
import com.hanyi.common.core.page.TableDataInfo;
import com.hanyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hanyi.system.domain.SysPersonAttr;
import com.hanyi.system.domain.vo.SysPersonAttrVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.hanyi.system.domain.bo.SysFaceAttrBo;
import com.hanyi.system.domain.vo.SysFaceAttrVo;
import com.hanyi.system.domain.SysFaceAttr;
import com.hanyi.system.mapper.SysFaceAttrMapper;
import com.hanyi.system.service.ISysFaceAttrService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 人脸属性Service业务层处理
 *
 * @author grantfee
 * @date 2024-01-08
 */
@RequiredArgsConstructor
@Service
public class SysFaceAttrServiceImpl implements ISysFaceAttrService {

    private final SysFaceAttrMapper baseMapper;

    /**
     * 查询人脸属性
     */
    @Override
    public SysFaceAttrVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    @Override
    public SysFaceAttrVo queryByEventId(Long eventId) {
        LambdaQueryWrapper<SysFaceAttr> lqw = Wrappers.lambdaQuery();
        lqw.eq(eventId != null, SysFaceAttr::getResultId, eventId);
        return baseMapper.selectVoOne(lqw);
    }
    /**
     * 查询人脸属性列表
     */
    @Override
    public TableDataInfo<SysFaceAttrVo> queryPageList(SysFaceAttrBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysFaceAttr> lqw = buildQueryWrapper(bo);
        Page<SysFaceAttrVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询人脸属性列表
     */
    @Override
    public List<SysFaceAttrVo> queryList(SysFaceAttrBo bo) {
        LambdaQueryWrapper<SysFaceAttr> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysFaceAttr> buildQueryWrapper(SysFaceAttrBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysFaceAttr> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getResultId() != null, SysFaceAttr::getResultId, bo.getResultId());
        lqw.eq(StringUtils.isNotBlank(bo.getGender()), SysFaceAttr::getGender, bo.getGender());
        lqw.eq(bo.getGenderScore() != null, SysFaceAttr::getGenderScore, bo.getGenderScore());
        lqw.eq(StringUtils.isNotBlank(bo.getAge()), SysFaceAttr::getAge, bo.getAge());
        lqw.eq(bo.getAgeScore() != null, SysFaceAttr::getAgeScore, bo.getAgeScore());
        lqw.eq(StringUtils.isNotBlank(bo.getHair()), SysFaceAttr::getHair, bo.getHair());
        lqw.eq(bo.getHairScore() != null, SysFaceAttr::getHairScore, bo.getHairScore());
        lqw.eq(StringUtils.isNotBlank(bo.getHat()), SysFaceAttr::getHat, bo.getHat());
        lqw.eq(bo.getHatScore() != null, SysFaceAttr::getHatScore, bo.getHatScore());
        lqw.eq(StringUtils.isNotBlank(bo.getMask()), SysFaceAttr::getMask, bo.getMask());
        lqw.eq(bo.getMaskScore() != null, SysFaceAttr::getMaskScore, bo.getMaskScore());
        lqw.eq(StringUtils.isNotBlank(bo.getGlass()), SysFaceAttr::getGlass, bo.getGlass());
        lqw.eq(bo.getGlassScore() != null, SysFaceAttr::getGlassScore, bo.getGlassScore());
        return lqw;
    }

    /**
     * 新增人脸属性
     */
    @Override
    public Boolean insertByBo(SysFaceAttrBo bo) {
        SysFaceAttr add = BeanUtil.toBean(bo, SysFaceAttr.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改人脸属性
     */
    @Override
    public Boolean updateByBo(SysFaceAttrBo bo) {
        SysFaceAttr update = BeanUtil.toBean(bo, SysFaceAttr.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysFaceAttr entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除人脸属性
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
