package com.hanyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.hanyi.common.annotation.ExcelDictFormat;
import com.hanyi.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 实时任务视图对象 sys_realtime_task
 *
 * @author grantfee
 * @date 2023-11-05
 */
@Data
@ExcelIgnoreUnannotated
public class SysRealtimeTaskVo {

    private static final long serialVersionUID = 1L;

    /**
     * 任务ID
     */
    @ExcelProperty(value = "任务ID")
    private Long taskId;

    /**
     * 任务名称
     */
    @ExcelProperty(value = "任务名称")
    private String name;

    /**
     * 摄像头名称
     */
    @ExcelProperty(value = "摄像头名称")
    private String ipcName;

    /**
     * 摄像头位置
     */
    @ExcelProperty(value = "摄像头位置")
    private String ipcPosition;

    /**
     * 主码流
     */
    @ExcelProperty(value = "主码流")
    private String mainStreamUrl;

    /**
     * 子码流
     */
    @ExcelProperty(value = "子码流")
    private String subStreamUrl;

    /**
     * 视频流状态
     */
    @ExcelProperty(value = "视频流状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_stream_status")
    private String mainStreamStatus;

    @ExcelProperty(value = "是否定时任务", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_yes_no")
    private String isTimePlan;
    /**
     * 开始时间
     */
    @ExcelProperty(value = "开始时间")
    private Date startTime;

    /**
     * 结束时间
     */
    @ExcelProperty(value = "结束时间")
    private Date endTime;

    @ExcelProperty(value = "进度")
    private int progress;

    /**
     * 通道的状态信息，0:通道不存在，1:通道正在创建，2:通道正在运
     * ⾏，3:通道正在修改，4:通道正在关闭，5:通道被释放，6:通道打
     * 开失败，7:通道seek失败
     * **/
    private Integer channelStatus;

    /**
     * 任务状态（0未开始，1正常， 2停止，3异常）
     */
    @ExcelProperty(value = "任务状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_task_status")
    private String status;

    /**
     * 终端ID
     */
    @ExcelProperty(value = "终端ID")
    private Long terminalId;

    /**
     * 终端名称
     */
    @ExcelProperty(value = "终端名称")
    private String terminalName;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


    private long deptId;
}
