package com.hanyi.system.domain.bo;

import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.hanyi.common.core.domain.BaseEntity;

/**
 * 车辆属性业务对象 sys_vehicle_attr
 *
 * @author grantfee
 * @date 2024-01-08
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SysVehicleAttrBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 事件ID
     */
    @NotNull(message = "事件ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long resultId;

    /**
     * 车型
     */
    @NotBlank(message = "车型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String vehicleType;

    /**
     * 车型分值
     */
    @NotNull(message = "车型分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float vehicleTypeScore;

    /**
     * 颜色
     */
    @NotBlank(message = "颜色不能为空", groups = { AddGroup.class, EditGroup.class })
    private String color;

    /**
     * 颜色分值
     */
    @NotNull(message = "颜色分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float colorScore;

    /**
     * 车牌
     */
    @NotBlank(message = "车牌不能为空", groups = { AddGroup.class, EditGroup.class })
    private String license;

    /**
     * 车牌分值
     */
    @NotNull(message = "车牌分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float licenseScore;

    /**
     * 品牌
     */
    @NotBlank(message = "品牌不能为空", groups = { AddGroup.class, EditGroup.class })
    private String brand;

    /**
     * 品牌分值
     */
    @NotNull(message = "品牌分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float brandScore;

    /**
     * 部门ID
     */
    private Long deptId;


}
