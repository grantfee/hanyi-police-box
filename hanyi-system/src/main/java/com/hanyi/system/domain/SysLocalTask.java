package com.hanyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hanyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotNull;

/**
 * 本地任务对象 sys_local_task
 *
 * @author grantfee
 * @date 2023-11-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_local_task")
public class SysLocalTask extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 任务ID
     */
    @TableId(value = "task_id")
    private Long taskId;
    /**
     * 任务名称
     */
    private String name;

    private String isFile;

    private String isLocalFile;
    /**
     * 视频名称
     */
    private String videoName;
    /**
     * 视频路径
     */
    private String videoPath;
    /**
     * 视频状态（0正常, 1断开）
     */
    private String videoStatus;

    private String rtspUrl;

    /**
    * 是否定时任务(N否, Y是)
    * */
    private String isTimePlan;

    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 通道的状态信息，0:通道不存在，1:通道正在创建，2:通道正在运
     * ⾏，3:通道正在修改，4:通道正在关闭，5:通道被释放，6:通道打
     * 开失败，7:通道seek失败
     * **/
    private Integer channelStatus;

    /**
     * 任务状态（0未开始，1正常， 2停止，3异常）
     */
    private String status;

    /**
     * 终端ID
     */
    private Long terminalId;

    /**
     * 终端名称
     */
    private String terminalName;

    /**
     * 备注
     */
    private String remark;

    private Long deptId;
}
