package com.hanyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.hanyi.common.core.domain.BaseEntity;

/**
 * 行人属性对象 sys_person_attr
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_person_attr")
public class SysPersonAttr extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 事件ID
     */
    private Long resultId;
    /**
     * 性别
     */
    private String gender;
    /**
     * 性别分值
     */
    private Float genderScore;
    /**
     * 年龄
     */
    private String age;
    /**
     * 年龄分值
     */
    private Float ageScore;
    /**
     * 上衣颜色
     */
    private String upperColor;
    /**
     * 上衣颜色分值
     */
    private Float upperColorScore;
    /**
     * 下衣颜色
     */
    private String bottomColor;
    /**
     * 下衣颜色分值
     */
    private Float bottomColorScore;
    /**
     * 上衣类型
     */
    private String upperType;
    /**
     * 上衣类型分值
     */
    private Float upperTypeScore;
    /**
     * 下衣类型
     */
    private String bottomType;
    /**
     * 下衣类型分值
     */
    private Float bottomTypeScore;
    /**
     * 头发
     */
    private String hair;
    /**
     * 头发分值
     */
    private Float hairScore;
    /**
     * 帽子
     */
    private String hat;
    /**
     * 帽子分值
     */
    private Float hatScore;
    /**
     * 口罩
     */
    private String mask;
    /**
     * 口罩分值
     */
    private Float maskScore;
    /**
     * 眼镜
     */
    private String glass;
    /**
     * 眼镜分值
     */
    private Float glassScore;
    /**
     * 部门ID
     */
    private Long deptId;

}
