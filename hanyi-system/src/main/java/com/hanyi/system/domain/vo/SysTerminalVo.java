package com.hanyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.hanyi.common.annotation.ExcelDictFormat;
import com.hanyi.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 终端管理视图对象 sys_terminal
 *
 * @author grantfee
 * @date 2023-11-21
 */
@Data
@ExcelIgnoreUnannotated
public class SysTerminalVo {

    private static final long serialVersionUID = 1L;

    /**
     * 终端ID
     */
    @ExcelProperty(value = "终端ID")
    private Long terminalId;

    /**
     * 终端名称
     */
    @ExcelProperty(value = "终端名称")
    private String terminalName;

    /**
     * IP地址
     */
    @ExcelProperty(value = "IP地址")
    private String serviceIp;

    /**
     * 端口
     */
    @ExcelProperty(value = "端口")
    private Long servicePort;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
