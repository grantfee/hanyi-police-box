package com.hanyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.hanyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotNull;

/**
 * 终端管理对象 sys_terminal
 *
 * @author grantfee
 * @date 2023-11-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_terminal")
public class SysTerminal extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 终端ID
     */
    @TableId(value = "terminal_id")
    private Long terminalId;
    /**
     * 终端名称
     */
    private String terminalName;
    /**
     * IP地址
     */
    private String serviceIp;
    /**
     * 端口
     */
    private Long servicePort;
    /**
     * 备注
     */
    private String remark;

    private Long deptId;
}
