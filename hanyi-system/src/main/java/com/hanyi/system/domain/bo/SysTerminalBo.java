package com.hanyi.system.domain.bo;

import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.hanyi.common.core.domain.BaseEntity;

/**
 * 终端管理业务对象 sys_terminal
 *
 * @author grantfee
 * @date 2023-11-21
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SysTerminalBo extends BaseEntity {

    /**
     * 终端ID
     */
    @NotNull(message = "终端ID不能为空", groups = { EditGroup.class })
    private Long terminalId;

    /**
     * 终端名称
     */
    @NotBlank(message = "终端名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String terminalName;

    /**
     * IP地址
     */
    @NotBlank(message = "IP地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String serviceIp;

    /**
     * 端口
     */
    @NotNull(message = "端口不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long servicePort;

    /**
     * 备注
     */
    private String remark;


    @NotNull(message = "部门ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long deptId;
}
