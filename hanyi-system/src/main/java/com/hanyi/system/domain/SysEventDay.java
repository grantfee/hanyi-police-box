package com.hanyi.system.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 每小时统计对象
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-18 14:17
 * @Version 1.0
 */

@Data
@NoArgsConstructor
public class SysEventDay {
    private String day;
    private Long count;
}
