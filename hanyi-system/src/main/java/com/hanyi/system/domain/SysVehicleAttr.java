package com.hanyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.hanyi.common.core.domain.BaseEntity;

/**
 * 车辆属性对象 sys_vehicle_attr
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_vehicle_attr")
public class SysVehicleAttr extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 事件ID
     */
    private Long resultId;
    /**
     * 车型
     */
    private String vehicleType;
    /**
     * 车型分值
     */
    private Float vehicleTypeScore;
    /**
     * 颜色
     */
    private String color;
    /**
     * 颜色分值
     */
    private Float colorScore;
    /**
     * 车牌
     */
    private String license;
    /**
     * 车牌分值
     */
    private Float licenseScore;
    /**
     * 品牌
     */
    private String brand;
    /**
     * 品牌分值
     */
    private Float brandScore;
    /**
     * 部门ID
     */
    private Long deptId;

}
