package com.hanyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hanyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotNull;

/**
 * 任务结果对象 sys_analysis_result
 *
 * @author grantfee
 * @date 2023-11-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_analysis_result")
public class SysAnalysisResult extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 任务ID
     */
    private Long taskId;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 任务类型
     */
    private String taskType;
    /**
     * 人数
     */
    private Long personCount;
    /**
     * 事件类型(0-行人，1-车辆)
     */
    private String eventType;
    /**
     * 事件开始
     */
    private Date eventStartTime;
    /**
     * 事件结束
     */
    private Date eventEndTime;


    private String eventTag;
    /**
     * 截图
     */
    private String captureImageUrl;

    private String fullImageUrl;
    private Long deptId;
}
