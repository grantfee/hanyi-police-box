package com.hanyi.system.domain.bo;

import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hanyi.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 任务结果业务对象 sys_analysis_result
 *
 * @author grantfee
 * @date 2023-11-06
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SysAnalysisResultBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 任务ID
     */
    @NotNull(message = "任务ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long taskId;

    /**
     * 任务名称
     */
    @NotBlank(message = "任务名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String taskName;

    /**
     * 任务类型
     */
    @NotBlank(message = "任务类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String taskType;

    /**
     * 人数
     */
    @NotNull(message = "人数不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long personCount;

    /**
     * 事件类型(0-行人，1-车辆)
     */
    @NotBlank(message = "事件类型(0-行人，1-车辆)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String eventType;

    private String eventTag;
    /**
     * 事件开始
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date eventStartTime;

    /**
     * 事件结束
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date eventEndTime;

    /**
     * 截图
     */
    private String captureImageUrl;

    /**
     * 截全图
     */
    private String fullImageUrl;

    @NotNull(message = "部门ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long deptId;

}
