package com.hanyi.system.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.hanyi.common.annotation.ExcelDictFormat;
import com.hanyi.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 任务结果视图对象 sys_analysis_result
 *
 * @author grantfee
 * @date 2023-11-06
 */
@Data
@ExcelIgnoreUnannotated
public class SysAnalysisResultVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 任务ID
     */
    @ExcelProperty(value = "任务ID")
    private Long taskId;

    /**
     * 任务名称
     */
    @ExcelProperty(value = "任务名称")
    private String taskName;

    /**
     * 任务类型
     */
    @ExcelProperty(value = "任务类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_task_type")
    private String taskType;

    /**
     * 人数
     */
    @ExcelProperty(value = "人数")
    private Long personCount;

    /**
     * 事件类型(0-行人，1-车辆)
     */
    @ExcelProperty(value = "事件类型(0-行人，1-车辆)", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_event_type")
    private String eventType;

    /**
     * 事件开始
     */
    @ExcelProperty(value = "事件开始")
    private Date eventStartTime;

    /**
     * 事件结束
     */
    @ExcelProperty(value = "事件结束")
    private Date eventEndTime;

    @ExcelProperty(value = "事件标记")
    private String eventTag;

    /**
     * 截图
     */
    @ExcelProperty(value = "截图")
    private String captureImageUrl;

    /**
     * 截全图
     */
    @ExcelProperty(value = "全景图")
    private String fullImageUrl;

    @ExcelProperty(value = "部门ID")
    private Long deptId;

}
