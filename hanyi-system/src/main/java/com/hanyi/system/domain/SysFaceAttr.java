package com.hanyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.hanyi.common.core.domain.BaseEntity;

/**
 * 人脸属性对象 sys_face_attr
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_face_attr")
public class SysFaceAttr extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 事件ID
     */
    private Long resultId;
    /**
     * 性别
     */
    private String gender;
    /**
     * 性别分值
     */
    private Float genderScore;
    /**
     * 年龄
     */
    private String age;
    /**
     * 年龄分值
     */
    private Float ageScore;
    /**
     * 头发
     */
    private String hair;
    /**
     * 头发分值
     */
    private Long hairScore;
    /**
     * 帽子
     */
    private String hat;
    /**
     * 帽子分值
     */
    private Float hatScore;
    /**
     * 口罩
     */
    private String mask;
    /**
     * 口罩分值
     */
    private Float maskScore;
    /**
     * 眼镜
     */
    private String glass;
    /**
     * 眼镜分值
     */
    private Float glassScore;
    /**
     * 部门ID
     */
    private Long deptId;

}
