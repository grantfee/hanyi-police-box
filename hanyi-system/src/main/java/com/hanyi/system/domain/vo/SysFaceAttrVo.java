package com.hanyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.hanyi.common.annotation.ExcelDictFormat;
import com.hanyi.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 人脸属性视图对象 sys_face_attr
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Data
@ExcelIgnoreUnannotated
public class SysFaceAttrVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 事件ID
     */
    @ExcelProperty(value = "事件ID")
    private Long resultId;

    /**
     * 性别
     */
    @ExcelProperty(value = "性别")
    private String gender;

    /**
     * 性别分值
     */
    @ExcelProperty(value = "性别分值")
    private Long genderScore;

    /**
     * 年龄
     */
    @ExcelProperty(value = "年龄")
    private String age;

    /**
     * 年龄分值
     */
    @ExcelProperty(value = "年龄分值")
    private Long ageScore;

    /**
     * 头发
     */
    @ExcelProperty(value = "头发")
    private String hair;

    /**
     * 头发分值
     */
    @ExcelProperty(value = "头发分值")
    private Long hairScore;

    /**
     * 帽子
     */
    @ExcelProperty(value = "帽子")
    private String hat;

    /**
     * 帽子分值
     */
    @ExcelProperty(value = "帽子分值")
    private Long hatScore;

    /**
     * 口罩
     */
    @ExcelProperty(value = "口罩")
    private String mask;

    /**
     * 口罩分值
     */
    @ExcelProperty(value = "口罩分值")
    private Long maskScore;

    /**
     * 眼镜
     */
    @ExcelProperty(value = "眼镜")
    private String glass;

    /**
     * 眼镜分值
     */
    @ExcelProperty(value = "眼镜分值")
    private Long glassScore;

    /**
     * 部门ID
     */
    @ExcelProperty(value = "部门ID")
    private Long deptId;


}
