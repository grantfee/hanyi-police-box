package com.hanyi.system.domain.bo;

import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.hanyi.common.core.domain.BaseEntity;

/**
 * 行人属性业务对象 sys_person_attr
 *
 * @author grantfee
 * @date 2024-01-08
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SysPersonAttrBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 事件ID
     */
    @NotNull(message = "事件ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long resultId;

    /**
     * 性别
     */
    @NotBlank(message = "性别不能为空", groups = { AddGroup.class, EditGroup.class })
    private String gender;

    /**
     * 性别分值
     */
    @NotNull(message = "性别分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float genderScore;

    /**
     * 年龄
     */
    @NotBlank(message = "年龄不能为空", groups = { AddGroup.class, EditGroup.class })
    private String age;

    /**
     * 年龄分值
     */
    @NotNull(message = "年龄分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float ageScore;

    /**
     * 上衣颜色
     */
    @NotBlank(message = "上衣颜色不能为空", groups = { AddGroup.class, EditGroup.class })
    private String upperColor;

    /**
     * 上衣颜色分值
     */
    @NotNull(message = "上衣颜色分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float upperColorScore;

    /**
     * 下衣颜色
     */
    @NotBlank(message = "下衣颜色不能为空", groups = { AddGroup.class, EditGroup.class })
    private String bottomColor;

    /**
     * 下衣颜色分值
     */
    @NotNull(message = "下衣颜色分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float bottomColorScore;

    /**
     * 上衣类型
     */
    @NotBlank(message = "上衣类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String upperType;

    /**
     * 上衣类型分值
     */
    @NotNull(message = "上衣类型分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float upperTypeScore;

    /**
     * 下衣类型
     */
    @NotBlank(message = "下衣类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String bottomType;

    /**
     * 下衣类型分值
     */
    @NotNull(message = "下衣类型分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float bottomTypeScore;

    /**
     * 头发
     */
    @NotBlank(message = "头发不能为空", groups = { AddGroup.class, EditGroup.class })
    private String hair;

    /**
     * 头发分值
     */
    @NotNull(message = "头发分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float hairScore;

    /**
     * 帽子
     */
    @NotBlank(message = "帽子不能为空", groups = { AddGroup.class, EditGroup.class })
    private String hat;

    /**
     * 帽子分值
     */
    @NotNull(message = "帽子分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float hatScore;

    /**
     * 口罩
     */
    @NotBlank(message = "口罩不能为空", groups = { AddGroup.class, EditGroup.class })
    private String mask;

    /**
     * 口罩分值
     */
    @NotNull(message = "口罩分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float maskScore;

    /**
     * 眼镜
     */
    @NotBlank(message = "眼镜不能为空", groups = { AddGroup.class, EditGroup.class })
    private String glass;

    /**
     * 眼镜分值
     */
    @NotNull(message = "眼镜分值不能为空", groups = { AddGroup.class, EditGroup.class })
    private Float glassScore;

    /**
     * 部门ID
     */
    private Long deptId;


}
