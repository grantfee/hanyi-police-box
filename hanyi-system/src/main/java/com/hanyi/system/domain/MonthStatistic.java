package com.hanyi.system.domain;

import java.util.List;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-18 15:06
 * @Version 1.0
 */
public  class MonthStatistic {
    private List<String> days;
    private List<Long> counts;

    public List<String> getDays() {
        return days;
    }

    public void setDays(List<String> days) {
        this.days = days;
    }

    public List<Long> getCounts() {
        return counts;
    }

    public void setCounts(List<Long> counts) {
        this.counts = counts;
    }
}
