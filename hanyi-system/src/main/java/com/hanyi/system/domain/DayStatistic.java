package com.hanyi.system.domain;

import java.util.List;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-18 15:06
 * @Version 1.0
 */
public  class DayStatistic{
    private List<String> hours;
    private List<Long> counts;

    public List<String> getHours() {
        return hours;
    }

    public void setHours(List<String> hours) {
        this.hours = hours;
    }

    public List<Long> getCounts() {
        return counts;
    }

    public void setCounts(List<Long> counts) {
        this.counts = counts;
    }
}
