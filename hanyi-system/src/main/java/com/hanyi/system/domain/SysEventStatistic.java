package com.hanyi.system.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author grantfee
 * @Description TODO
 * @Date 2024-01-18 10:30
 * @Version 1.0
 */
@Data
@NoArgsConstructor
public class SysEventStatistic {

    private TotalCountStatistic totalCountStatistic;

    private DayStatistic personDayStatistic;
    private DayStatistic vehicleDayStatistic;
    private DayStatistic faceDayStatistic;

    private MonthStatistic personMonthStatistic;
    private MonthStatistic vehicleMonthStatistic;
    private MonthStatistic faceMonthStatistic;

    public static class TotalCountStatistic{
        private long personCount;
        private long veihcleCount;
        private long faceCount;
        private long totalCount;

        public long getPersonCount() {
            return personCount;
        }

        public void setPersonCount(long personCount) {
            this.personCount = personCount;
        }

        public long getVeihcleCount() {
            return veihcleCount;
        }

        public void setVeihcleCount(long veihcleCount) {
            this.veihcleCount = veihcleCount;
        }

        public long getFaceCount() {
            return faceCount;
        }

        public void setFaceCount(long faceCount) {
            this.faceCount = faceCount;
        }

        public long getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(long totalCount) {
            this.totalCount = totalCount;
        }
    }


}
