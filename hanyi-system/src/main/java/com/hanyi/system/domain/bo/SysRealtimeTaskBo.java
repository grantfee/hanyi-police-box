package com.hanyi.system.domain.bo;

import com.hanyi.common.core.validate.AddGroup;
import com.hanyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hanyi.common.core.domain.BaseEntity;

/**
 * 实时任务业务对象 sys_realtime_task
 *
 * @author grantfee
 * @date 2023-11-05
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SysRealtimeTaskBo extends BaseEntity {

    /**
     * 任务ID
     */
    @NotNull(message = "任务ID不能为空", groups = { EditGroup.class })
    private Long taskId;

    /**
     * 任务名称
     */
    @NotBlank(message = "任务名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 摄像头名称
     */
    @NotBlank(message = "摄像头名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ipcName;

    /**
     * 摄像头位置
     */
    @NotBlank(message = "摄像头位置不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ipcPosition;

    /**
     * 主码流
     */
    @NotBlank(message = "主码流不能为空", groups = { AddGroup.class, EditGroup.class })
    private String mainStreamUrl;

    /**
     * 子码流
     */
    @NotBlank(message = "子码流不能为空", groups = { AddGroup.class, EditGroup.class })
    private String subStreamUrl;

    /**
     * 视频流状态
     */
    @NotBlank(message = "视频流状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private String mainStreamStatus;

    @NotBlank(message = "是否定时任务（N否, Y是）不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isTimePlan;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    private Integer progress;

    /**
     * 通道的状态信息，0:通道不存在，1:通道正在创建，2:通道正在运
     * ⾏，3:通道正在修改，4:通道正在关闭，5:通道被释放，6:通道打
     * 开失败，7:通道seek失败
     * **/
    private Integer channelStatus;

    /**
     * 任务状态（0未开始，1正常， 2停止，3异常）
     */
    @NotBlank(message = "任务状态（0未开始，1正常， 2停止，3异常）不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 终端ID
     */
    private Long terminalId;

    /**
     * 终端名称
     */
    private String terminalName;

    /**
     * 备注
     */
    private String remark;

    @NotNull(message = "部门ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long deptId;
}
