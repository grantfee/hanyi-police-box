package com.hanyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.hanyi.common.annotation.ExcelDictFormat;
import com.hanyi.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 车辆属性视图对象 sys_vehicle_attr
 *
 * @author grantfee
 * @date 2024-01-08
 */
@Data
@ExcelIgnoreUnannotated
public class SysVehicleAttrVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 事件ID
     */
    @ExcelProperty(value = "事件ID")
    private Long resultId;

    /**
     * 车型
     */
    @ExcelProperty(value = "车型")
    private String vehicleType;

    /**
     * 车型分值
     */
    @ExcelProperty(value = "车型分值")
    private Long vehicleTypeScore;

    /**
     * 颜色
     */
    @ExcelProperty(value = "颜色")
    private String color;

    /**
     * 颜色分值
     */
    @ExcelProperty(value = "颜色分值")
    private Long colorScore;

    /**
     * 车牌
     */
    @ExcelProperty(value = "车牌")
    private String license;

    /**
     * 车牌分值
     */
    @ExcelProperty(value = "车牌分值")
    private Long licenseScore;

    /**
     * 品牌
     */
    @ExcelProperty(value = "品牌")
    private String brand;

    /**
     * 品牌分值
     */
    @ExcelProperty(value = "品牌分值")
    private Long brandScore;

    /**
     * 部门ID
     */
    @ExcelProperty(value = "部门ID")
    private Long deptId;


}
