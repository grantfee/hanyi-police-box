package com.hanyi.common.exception.user;

/**
 * 用户不存在
 *
 * @author grantfee
 */
public class AppUserPasswordNotMatchException extends UserException {
    private static final long serialVersionUID = 1L;

    public AppUserPasswordNotMatchException() {
        super("user.password.not.match");
    }
}
