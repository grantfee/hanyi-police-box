package com.hanyi.common.exception.user;

/**
 * 用户不存在
 *
 * @author grantfee
 */
public class AppUserNotExistException extends UserException {
    private static final long serialVersionUID = 1L;

    public AppUserNotExistException(String msg) {
        super(msg);
    }
}
