package com.hanyi.common.exception.user;

/**
 * 验证码失效异常类
 *
 * @author ruoyi
 */
public class SmsCodeExpireException extends UserException {
    private static final long serialVersionUID = 1L;

    public SmsCodeExpireException() {
        super("user.jcaptcha.expire");
    }
}
