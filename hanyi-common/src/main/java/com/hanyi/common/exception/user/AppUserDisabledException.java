package com.hanyi.common.exception.user;

/**
 * 用户不存在
 *
 * @author grantfee
 */
public class AppUserDisabledException extends UserException {
    private static final long serialVersionUID = 1L;

    public AppUserDisabledException(String msg) {
        super(msg);
    }
}
