package com.hanyi.common.core.domain.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class AppLoginUser extends LoginUser implements Serializable {
    private static final long serialVersionUID = 1L;

    private long age;
    private int sex;
    private String clientName;
    private String email;

}
