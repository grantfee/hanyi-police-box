#!/bin/sh

### BEGIN INIT INFO
# Provides: [AIS，Admin web 后台]
# Required-Start: $network $remote_fs $local_fs
# Required-Stop: $network $remote_fs $local_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description:启动算法模块，启动管理端 
# Description: 
### END INIT INFO

# [需要执行的命令]
sleep 5
cd /home/linaro/3.0.2_7c5607_20231010_release/ais 
sudo python start.py start

# 启动mysql redis webrtc docker容器
sudo docker start mysql 
sudo docker start redis
sudo docker start webrtc-streamer

# 启动后台服务
sleep 3
cd /home/linaro/hanyi-admin
sudo ./admin.sh start
exit 0

