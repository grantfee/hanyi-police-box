-- MySQL dump 10.13  Distrib 8.0.19, for Linux (aarch64)
--
-- Host: 127.0.0.1    Database: hanyi
-- ------------------------------------------------------
-- Server version	8.2.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gen_table`
--

DROP TABLE IF EXISTS `gen_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gen_table` (
  `table_id` bigint NOT NULL COMMENT '编号',
  `table_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) COLLATE utf8mb4_general_ci DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) COLLATE utf8mb4_general_ci DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='代码生成业务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_table`
--

LOCK TABLES `gen_table` WRITE;
/*!40000 ALTER TABLE `gen_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `gen_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_table_column`
--

DROP TABLE IF EXISTS `gen_table_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gen_table_column` (
  `column_id` bigint NOT NULL COMMENT '编号',
  `table_id` bigint DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典类型',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='代码生成业务表字段';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_table_column`
--

LOCK TABLES `gen_table_column` WRITE;
/*!40000 ALTER TABLE `gen_table_column` DISABLE KEYS */;
/*!40000 ALTER TABLE `gen_table_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_analysis_result`
--

DROP TABLE IF EXISTS `sys_analysis_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_analysis_result` (
  `id` bigint NOT NULL COMMENT 'ID',
  `task_id` bigint NOT NULL COMMENT '任务ID',
  `task_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `task_type` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '任务类型(0-实时任务, 1-离线任务，2-本地任务)',
  `person_count` int NOT NULL COMMENT '人数',
  `event_type` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '事件类型(0-行人，1-车辆)',
  `event_start_time` datetime DEFAULT NULL COMMENT '事件开始',
  `event_end_time` datetime DEFAULT NULL COMMENT '事件结束',
  `video_playback_url` varchar(128) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '回放地址',
  `capture_image_url` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '截图',
  `full_image_url` varchar(225) COLLATE utf8mb4_general_ci DEFAULT '',
  `dept_id` bigint NOT NULL,
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='任务结果表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_analysis_result`
--

LOCK TABLES `sys_analysis_result` WRITE;
/*!40000 ALTER TABLE `sys_analysis_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_analysis_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_config` (
  `config_id` bigint NOT NULL COMMENT '参数主键',
  `config_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) COLLATE utf8mb4_general_ci DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='参数配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES (1,'主框架页-默认皮肤样式名称','sys.index.skinName','skin-blue','Y','admin','2022-12-02 05:07:00','',NULL,'蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow'),(2,'用户管理-账号初始密码','sys.user.initPassword','123456','Y','admin','2022-12-02 05:07:00','',NULL,'初始化密码 123456'),(3,'主框架页-侧边栏主题','sys.index.sideTheme','theme-dark','Y','admin','2022-12-02 05:07:00','',NULL,'深色主题theme-dark，浅色主题theme-light'),(4,'账号自助-验证码开关','sys.account.captchaEnabled','false','N','admin','2022-12-02 05:07:00','admin','2024-01-17 11:23:16','是否开启验证码功能（true开启，false关闭）'),(5,'账号自助-是否开启用户注册功能','sys.account.registerUser','false','Y','admin','2022-12-02 05:07:00','',NULL,'是否开启注册用户功能（true开启，false关闭）'),(11,'OSS预览列表资源开关','sys.oss.previewListResource','true','Y','admin','2022-12-02 05:07:00','',NULL,'true:开启, false:关闭');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept`
--

DROP TABLE IF EXISTS `sys_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dept` (
  `dept_id` bigint NOT NULL COMMENT '部门id',
  `parent_id` bigint DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(500) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '部门名称',
  `order_num` int DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='部门表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept`
--

LOCK TABLES `sys_dept` WRITE;
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` VALUES (100,0,'0','智波科技',0,'智波','15888888888','xxx@qq.com','0','0','admin','2022-12-02 05:06:52','admin','2022-12-03 20:30:31'),(101,100,'0,100','深圳总公司',1,'若依','15888888888','ry@qq.com','0','0','admin','2022-12-02 05:06:52','',NULL),(102,100,'0,100','长沙分公司',2,'若依','15888888888','ry@qq.com','0','2','admin','2022-12-02 05:06:52','',NULL),(103,101,'0,100,101','研发部门',1,'若依','15888888888','ry@qq.com','0','0','admin','2022-12-02 05:06:52','',NULL),(104,101,'0,100,101','市场部门',2,'若依','15888888888','ry@qq.com','0','0','admin','2022-12-02 05:06:52','',NULL),(105,101,'0,100,101','测试部门',3,'若依','15888888888','ry@qq.com','0','0','admin','2022-12-02 05:06:52','',NULL),(106,101,'0,100,101','财务部门',4,'若依','15888888888','ry@qq.com','0','0','admin','2022-12-02 05:06:52','',NULL),(107,101,'0,100,101','运维部门',5,'若依','15888888888','ry@qq.com','0','0','admin','2022-12-02 05:06:52','',NULL),(108,102,'0,100,102','市场部门',1,'若依','15888888888','ry@qq.com','0','2','admin','2022-12-02 05:06:52','',NULL),(109,102,'0,100,102','财务部门',2,'若依','15888888888','ry@qq.com','0','2','admin','2022-12-02 05:06:52','',NULL),(1727580449928417281,100,'0,100','深圳分公司',2,'tang','','','0','0','grantfee','2023-11-23 14:50:50','grantfee','2023-11-23 14:50:50'),(1727583281322692610,1727580449928417281,'0,100,1727580449928417281','西乡分部',1,'','','','0','0','admin','2023-11-23 15:02:05','admin','2023-11-23 15:02:05'),(1727665135430176770,1727580449928417281,'0,100,1727580449928417281','固戍分部',2,'','','','0','0','admin','2023-11-23 20:27:21','admin','2023-11-23 20:27:21'),(1727666297910890498,1727583281322692610,'0,100,1727580449928417281,1727583281322692610','西乡街道',1,'','','','0','0','xixiang','2023-11-23 20:31:58','xixiang','2023-11-23 20:31:58');
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_data`
--

DROP TABLE IF EXISTS `sys_dict_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint NOT NULL COMMENT '字典编码',
  `dict_sort` int DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) COLLATE utf8mb4_general_ci DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典数据表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_data`
--

LOCK TABLES `sys_dict_data` WRITE;
/*!40000 ALTER TABLE `sys_dict_data` DISABLE KEYS */;
INSERT INTO `sys_dict_data` VALUES (1,1,'男','0','sys_user_sex','','','Y','0','admin','2022-12-02 05:06:59','',NULL,'性别男'),(2,2,'女','1','sys_user_sex','','','N','0','admin','2022-12-02 05:06:59','',NULL,'性别女'),(3,3,'未知','2','sys_user_sex','','','N','0','admin','2022-12-02 05:06:59','',NULL,'性别未知'),(4,1,'显示','0','sys_show_hide','','primary','Y','0','admin','2022-12-02 05:07:00','',NULL,'显示菜单'),(5,2,'隐藏','1','sys_show_hide','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'隐藏菜单'),(6,1,'正常','0','sys_normal_disable','','primary','Y','0','admin','2022-12-02 05:07:00','',NULL,'正常状态'),(7,2,'停用','1','sys_normal_disable','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'停用状态'),(12,1,'是','Y','sys_yes_no','','primary','Y','0','admin','2022-12-02 05:07:00','',NULL,'系统默认是'),(13,2,'否','N','sys_yes_no','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'系统默认否'),(14,1,'通知','1','sys_notice_type','','warning','Y','0','admin','2022-12-02 05:07:00','',NULL,'通知'),(15,2,'公告','2','sys_notice_type','','success','N','0','admin','2022-12-02 05:07:00','',NULL,'公告'),(16,1,'正常','0','sys_notice_status','','primary','Y','0','admin','2022-12-02 05:07:00','',NULL,'正常状态'),(17,2,'关闭','1','sys_notice_status','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'关闭状态'),(18,1,'新增','1','sys_oper_type','','info','N','0','admin','2022-12-02 05:07:00','',NULL,'新增操作'),(19,2,'修改','2','sys_oper_type','','info','N','0','admin','2022-12-02 05:07:00','',NULL,'修改操作'),(20,3,'删除','3','sys_oper_type','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'删除操作'),(21,4,'授权','4','sys_oper_type','','primary','N','0','admin','2022-12-02 05:07:00','',NULL,'授权操作'),(22,5,'导出','5','sys_oper_type','','warning','N','0','admin','2022-12-02 05:07:00','',NULL,'导出操作'),(23,6,'导入','6','sys_oper_type','','warning','N','0','admin','2022-12-02 05:07:00','',NULL,'导入操作'),(24,7,'强退','7','sys_oper_type','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'强退操作'),(25,8,'生成代码','8','sys_oper_type','','warning','N','0','admin','2022-12-02 05:07:00','',NULL,'生成操作'),(26,9,'清空数据','9','sys_oper_type','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'清空操作'),(27,1,'成功','0','sys_common_status','','primary','N','0','admin','2022-12-02 05:07:00','',NULL,'正常状态'),(28,2,'失败','1','sys_common_status','','danger','N','0','admin','2022-12-02 05:07:00','',NULL,'停用状态'),(29,99,'其他','0','sys_oper_type','','info','N','0','admin','2022-12-02 05:07:00','',NULL,'其他操作'),(1720996575672872962,0,'未开始','0','sys_task_status','','default','N','0','admin','2023-11-05 10:48:52','admin','2023-11-05 10:48:52',''),(1720996733215125505,1,'运行中','1','sys_task_status','','success','N','0','admin','2023-11-05 10:49:30','admin','2023-11-05 10:49:30',''),(1720996829415682050,2,'结束','2','sys_task_status','','info','N','0','admin','2023-11-05 10:49:53','admin','2023-11-05 10:49:53',''),(1720996930418716674,3,'异常','3','sys_task_status','','danger','N','0','admin','2023-11-05 10:50:17','admin','2023-11-05 10:50:17',''),(1720997920555479041,0,'未知','0','sys_stream_status','','default','N','0','admin','2023-11-05 10:54:13','admin','2023-11-05 10:54:33',''),(1720998062142599169,1,'正常','1','sys_stream_status','','success','N','0','admin','2023-11-05 10:54:47','admin','2023-11-05 10:54:47',''),(1720998172595400706,2,'断开','2','sys_stream_status','','danger','N','0','admin','2023-11-05 10:55:13','admin','2023-11-05 10:55:13',''),(1721363978793594882,0,'实时任务','0','sys_task_type','','primary','N','0','admin','2023-11-06 11:08:48','admin','2023-11-06 11:08:48',''),(1721364038650507266,1,'离线任务','1','sys_task_type','','primary','N','0','admin','2023-11-06 11:09:02','admin','2023-11-06 11:09:02',''),(1721364127267762177,2,'本地任务','2','sys_task_type','','primary','N','0','admin','2023-11-06 11:09:23','admin','2023-11-06 11:09:23',''),(1721364987334963201,0,'行人','0','sys_event_type','','primary','N','0','admin','2023-11-06 11:12:49','admin','2023-11-06 11:12:49',''),(1721365041533759489,1,'车辆','1','sys_event_type','','primary','N','0','admin','2023-11-06 11:13:01','admin','2023-11-06 11:13:01',''),(1742369453328596993,0,'通道不存在','0','sys_channel_status','','default','N','0','admin','2024-01-03 10:17:03','admin','2024-01-03 10:17:03',''),(1742369588628455426,1,'正在创建','1','sys_channel_status','','primary','N','0','admin','2024-01-03 10:17:36','admin','2024-01-03 10:17:36',''),(1742369675517657089,2,'正在运行','2','sys_channel_status','','success','N','0','admin','2024-01-03 10:17:56','admin','2024-01-03 10:17:56',''),(1742369781390278657,3,'正在修改','3','sys_channel_status','','info','N','0','admin','2024-01-03 10:18:22','admin','2024-01-03 10:18:22',''),(1742369890337325058,4,'正在关闭','4','sys_channel_status','','info','N','0','admin','2024-01-03 10:18:48','admin','2024-01-03 10:18:48',''),(1742370095275212801,5,'通道已释放','5','sys_channel_status','','info','N','0','admin','2024-01-03 10:19:37','admin','2024-01-03 10:19:37',''),(1742370211776200706,6,'打开失败','6','sys_channel_status','','danger','N','0','admin','2024-01-03 10:20:04','admin','2024-01-03 10:20:04',''),(1742371029820669953,7,'视频寻帧失败','7','sys_channel_status','','danger','N','0','admin','2024-01-03 10:23:19','admin','2024-01-03 10:23:19',''),(1743086116407640066,2,'人脸','2','sys_event_type','','primary','N','0','admin','2024-01-05 09:44:49','admin','2024-01-05 09:44:49','');
/*!40000 ALTER TABLE `sys_dict_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_type`
--

DROP TABLE IF EXISTS `sys_dict_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint NOT NULL COMMENT '字典主键',
  `dict_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典类型',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_type`
--

LOCK TABLES `sys_dict_type` WRITE;
/*!40000 ALTER TABLE `sys_dict_type` DISABLE KEYS */;
INSERT INTO `sys_dict_type` VALUES (1,'用户性别','sys_user_sex','0','admin','2022-12-02 05:06:59','',NULL,'用户性别列表'),(2,'菜单状态','sys_show_hide','0','admin','2022-12-02 05:06:59','',NULL,'菜单状态列表'),(3,'系统开关','sys_normal_disable','0','admin','2022-12-02 05:06:59','',NULL,'系统开关列表'),(6,'系统是否','sys_yes_no','0','admin','2022-12-02 05:06:59','',NULL,'系统是否列表'),(7,'通知类型','sys_notice_type','0','admin','2022-12-02 05:06:59','',NULL,'通知类型列表'),(8,'通知状态','sys_notice_status','0','admin','2022-12-02 05:06:59','',NULL,'通知状态列表'),(9,'操作类型','sys_oper_type','0','admin','2022-12-02 05:06:59','',NULL,'操作类型列表'),(10,'系统状态','sys_common_status','0','admin','2022-12-02 05:06:59','',NULL,'登录状态列表'),(1720996310827741186,'任务状态','sys_task_status','0','admin','2023-11-05 10:47:49','admin','2023-11-05 10:48:10','分析任务状态列表'),(1720997811218362369,'视频流状态','sys_stream_status','0','admin','2023-11-05 10:53:47','admin','2023-11-05 10:53:47','视频流出流状态'),(1721363818776702978,'任务类型','sys_task_type','0','admin','2023-11-06 11:08:10','admin','2023-11-06 11:08:10',''),(1721364305219497985,'事件类型','sys_event_type','0','admin','2023-11-06 11:10:06','admin','2023-11-06 11:10:06',''),(1742369273388761089,'通道状态','sys_channel_status','0','admin','2024-01-03 10:16:21','admin','2024-01-03 10:23:47','算法通道状态');
/*!40000 ALTER TABLE `sys_dict_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_face_attr`
--

DROP TABLE IF EXISTS `sys_face_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_face_attr` (
  `id` bigint NOT NULL COMMENT 'ID',
  `result_id` bigint NOT NULL COMMENT '事件ID',
  `gender` varchar(16) COLLATE utf8mb4_general_ci NOT NULL COMMENT '性别',
  `gender_score` float NOT NULL COMMENT '性别分值',
  `age` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '年龄',
  `age_score` float NOT NULL COMMENT '年龄分值',
  `hair` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '头发',
  `hair_score` float NOT NULL COMMENT '头发分值',
  `hat` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '帽子',
  `hat_score` float NOT NULL COMMENT '帽子分值',
  `mask` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '口罩',
  `mask_score` float NOT NULL COMMENT '口罩分值',
  `glass` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '眼镜',
  `glass_score` float NOT NULL COMMENT '眼镜分值',
  `dept_id` bigint DEFAULT NULL,
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='人脸属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_face_attr`
--

LOCK TABLES `sys_face_attr` WRITE;
/*!40000 ALTER TABLE `sys_face_attr` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_face_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_local_task`
--

DROP TABLE IF EXISTS `sys_local_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_local_task` (
  `task_id` bigint NOT NULL COMMENT '任务ID',
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `video_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '视频名称',
  `is_file` char(1) COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '文件或目录(1文件，2目录)',
  `is_local_file` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1',
  `video_path` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '视频路径',
  `video_status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '视频状态（0正常, 1断开）',
  `is_time_plan` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `progress` int NOT NULL DEFAULT '0' COMMENT '进度',
  `rtsp_url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '视频播放地址',
  `channel_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '通道的状态信息，0:通道不存在，1:通道正在创建，2:通道正在运\n⾏，3:通道正在修改，4:通道正在关闭，5:通道被释放，6:通道打\n开失败，7:通道seek失败',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '任务状态（0未开始，1正常， 2停止，3异常）',
  `terminal_id` bigint NOT NULL,
  `terminal_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='本地任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_local_task`
--

LOCK TABLES `sys_local_task` WRITE;
/*!40000 ALTER TABLE `sys_local_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_local_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_logininfor`
--

DROP TABLE IF EXISTS `sys_logininfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint NOT NULL COMMENT '访问ID',
  `user_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '操作系统',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统访问记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_logininfor`
--

LOCK TABLES `sys_logininfor` WRITE;
/*!40000 ALTER TABLE `sys_logininfor` DISABLE KEYS */;
INSERT INTO `sys_logininfor` VALUES (1598602761799372801,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2022-12-02 16:59:33'),(1598662428332347393,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2022-12-02 20:56:39'),(1598672510386393090,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2022-12-02 21:36:43'),(1598672574198534146,'test','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误1次','2022-12-02 21:36:58'),(1598672624614068226,'test','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','验证码错误','2022-12-02 21:37:10'),(1598672745200308225,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误1次','2022-12-02 21:37:39'),(1598672801013911553,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误2次','2022-12-02 21:37:52'),(1598673183656071169,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误3次','2022-12-02 21:39:23'),(1598673358877315074,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误4次','2022-12-02 21:40:05'),(1598673409565478913,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误5次，帐户锁定10分钟','2022-12-02 21:40:17'),(1599017140739067905,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','验证码已失效','2022-12-03 20:26:09'),(1599017174633238529,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误1次','2022-12-03 20:26:17'),(1599017232170700801,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误2次','2022-12-03 20:26:31'),(1599017419895164929,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','1','密码输入错误3次','2022-12-03 20:27:15'),(1599017455135707138,'admin','192.168.31.18','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2022-12-03 20:27:24'),(1747459303326982145,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','1','验证码错误','2024-01-17 11:22:19'),(1747459421379862529,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 11:22:47'),(1747459665056342017,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 11:23:45'),(1747460073795461121,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 11:25:22'),(1747460386849923074,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 11:26:37'),(1747460399478972418,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 11:26:40'),(1747460439505215490,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 11:26:50'),(1747460477329448961,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 11:26:59'),(1747467340366557186,'admin','127.0.0.1','内网IP','MSEdge','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 11:54:15'),(1747467586253434881,'admin','127.0.0.1','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 11:55:14'),(1747470271369027585,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 12:05:54'),(1747470335638347777,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 12:06:09'),(1747492273802813441,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 13:33:20'),(1747492283529404418,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 13:33:22'),(1747492806718496770,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 13:35:27'),(1747492822531022850,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 13:35:30'),(1747492875064680450,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 13:35:43'),(1747492882757033985,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 13:35:45'),(1747493051871371265,'admin','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','退出成功','2024-01-17 13:36:25'),(1747493063372152834,'manager','192.168.2.183','内网IP','Chrome','Windows 10 or Windows Server 2016','0','登录成功','2024-01-17 13:36:28');
/*!40000 ALTER TABLE `sys_logininfor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menu` (
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  `menu_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '组件路径',
  `query_param` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路由参数',
  `is_frame` int DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '显示状态（0显示 1隐藏）',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='菜单权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,'系统管理',0,1,'system',NULL,'',1,0,'M','0','0','','system','admin','2022-12-02 05:06:53','',NULL,'系统管理目录'),(2,'系统监控',0,2,'monitor',NULL,'',1,0,'M','0','0','','monitor','admin','2022-12-02 05:06:53','',NULL,'系统监控目录'),(3,'系统工具',0,6,'tool',NULL,'',1,0,'M','0','0','','tool','admin','2022-12-02 05:06:53','admin','2024-01-17 13:35:02','系统工具目录'),(4,'PLUS官网',0,6,'https://gitee.com/JavaLionLi/RuoYi-Vue-Plus',NULL,'',0,0,'M','1','1','','guide','admin','2022-12-02 05:06:53','admin','2024-01-17 13:34:35','RuoYi-Vue-Plus官网地址'),(5,'测试菜单',0,7,'demo',NULL,NULL,1,0,'M','1','1',NULL,'star','admin','2022-12-02 05:07:40','admin','2024-01-17 13:35:10',''),(100,'用户管理',1,1,'user','system/user/index','',1,0,'C','0','0','system:user:list','user','admin','2022-12-02 05:06:53','',NULL,'用户管理菜单'),(101,'角色管理',1,2,'role','system/role/index','',1,0,'C','0','0','system:role:list','peoples','admin','2022-12-02 05:06:53','',NULL,'角色管理菜单'),(102,'菜单管理',1,3,'menu','system/menu/index','',1,0,'C','0','0','system:menu:list','tree-table','admin','2022-12-02 05:06:53','',NULL,'菜单管理菜单'),(103,'部门管理',1,4,'dept','system/dept/index','',1,0,'C','0','0','system:dept:list','tree','admin','2022-12-02 05:06:53','',NULL,'部门管理菜单'),(104,'岗位管理',1,5,'post','system/post/index','',1,0,'C','0','0','system:post:list','post','admin','2022-12-02 05:06:53','',NULL,'岗位管理菜单'),(105,'字典管理',1,6,'dict','system/dict/index','',1,0,'C','0','0','system:dict:list','dict','admin','2022-12-02 05:06:53','',NULL,'字典管理菜单'),(106,'参数设置',1,7,'config','system/config/index','',1,0,'C','0','0','system:config:list','edit','admin','2022-12-02 05:06:53','',NULL,'参数设置菜单'),(107,'通知公告',1,8,'notice','system/notice/index','',1,0,'C','0','0','system:notice:list','message','admin','2022-12-02 05:06:53','',NULL,'通知公告菜单'),(108,'日志管理',1,9,'log','','',1,0,'M','0','0','','log','admin','2022-12-02 05:06:53','',NULL,'日志管理菜单'),(109,'在线用户',2,1,'online','monitor/online/index','',1,0,'C','0','0','monitor:online:list','online','admin','2022-12-02 05:06:53','',NULL,'在线用户菜单'),(111,'数据监控',2,3,'druid','monitor/druid/index','',1,0,'C','0','0','monitor:druid:list','druid','admin','2022-12-02 05:06:53','',NULL,'数据监控菜单'),(112,'缓存列表',2,6,'cacheList','monitor/cache/list','',1,0,'C','0','0','monitor:cache:list','redis-list','admin','2022-12-02 05:06:53','',NULL,'缓存列表菜单'),(113,'缓存监控',2,5,'cache','monitor/cache/index','',1,0,'C','0','0','monitor:cache:list','redis','admin','2022-12-02 05:06:53','',NULL,'缓存监控菜单'),(114,'表单构建',3,1,'build','tool/build/index','',1,0,'C','0','0','tool:build:list','build','admin','2022-12-02 05:06:53','',NULL,'表单构建菜单'),(115,'代码生成',3,2,'gen','tool/gen/index','',1,0,'C','0','0','tool:gen:list','code','admin','2022-12-02 05:06:53','',NULL,'代码生成菜单'),(117,'Admin监控',2,5,'Admin','monitor/admin/index','',1,0,'C','0','0','monitor:admin:list','dashboard','admin','2022-12-02 05:06:53','',NULL,'Admin监控菜单'),(118,'文件管理',1,10,'oss','system/oss/index','',1,0,'C','0','0','system:oss:list','upload','admin','2022-12-02 05:06:53','',NULL,'文件管理菜单'),(120,'任务调度中心',2,5,'XxlJob','monitor/xxljob/index','',1,0,'C','0','0','monitor:xxljob:list','job','admin','2022-12-02 05:06:53','',NULL,'Xxl-Job控制台菜单'),(500,'操作日志',108,1,'operlog','monitor/operlog/index','',1,0,'C','0','0','monitor:operlog:list','form','admin','2022-12-02 05:06:53','',NULL,'操作日志菜单'),(501,'登录日志',108,2,'logininfor','monitor/logininfor/index','',1,0,'C','0','0','monitor:logininfor:list','logininfor','admin','2022-12-02 05:06:53','',NULL,'登录日志菜单'),(1001,'用户查询',100,1,'','','',1,0,'F','0','0','system:user:query','#','admin','2022-12-02 05:06:53','',NULL,''),(1002,'用户新增',100,2,'','','',1,0,'F','0','0','system:user:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1003,'用户修改',100,3,'','','',1,0,'F','0','0','system:user:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1004,'用户删除',100,4,'','','',1,0,'F','0','0','system:user:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1005,'用户导出',100,5,'','','',1,0,'F','0','0','system:user:export','#','admin','2022-12-02 05:06:54','',NULL,''),(1006,'用户导入',100,6,'','','',1,0,'F','0','0','system:user:import','#','admin','2022-12-02 05:06:54','',NULL,''),(1007,'重置密码',100,7,'','','',1,0,'F','0','0','system:user:resetPwd','#','admin','2022-12-02 05:06:54','',NULL,''),(1008,'角色查询',101,1,'','','',1,0,'F','0','0','system:role:query','#','admin','2022-12-02 05:06:54','',NULL,''),(1009,'角色新增',101,2,'','','',1,0,'F','0','0','system:role:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1010,'角色修改',101,3,'','','',1,0,'F','0','0','system:role:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1011,'角色删除',101,4,'','','',1,0,'F','0','0','system:role:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1012,'角色导出',101,5,'','','',1,0,'F','0','0','system:role:export','#','admin','2022-12-02 05:06:54','',NULL,''),(1013,'菜单查询',102,1,'','','',1,0,'F','0','0','system:menu:query','#','admin','2022-12-02 05:06:54','',NULL,''),(1014,'菜单新增',102,2,'','','',1,0,'F','0','0','system:menu:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1015,'菜单修改',102,3,'','','',1,0,'F','0','0','system:menu:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1016,'菜单删除',102,4,'','','',1,0,'F','0','0','system:menu:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1017,'部门查询',103,1,'','','',1,0,'F','0','0','system:dept:query','#','admin','2022-12-02 05:06:54','',NULL,''),(1018,'部门新增',103,2,'','','',1,0,'F','0','0','system:dept:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1019,'部门修改',103,3,'','','',1,0,'F','0','0','system:dept:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1020,'部门删除',103,4,'','','',1,0,'F','0','0','system:dept:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1021,'岗位查询',104,1,'','','',1,0,'F','0','0','system:post:query','#','admin','2022-12-02 05:06:54','',NULL,''),(1022,'岗位新增',104,2,'','','',1,0,'F','0','0','system:post:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1023,'岗位修改',104,3,'','','',1,0,'F','0','0','system:post:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1024,'岗位删除',104,4,'','','',1,0,'F','0','0','system:post:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1025,'岗位导出',104,5,'','','',1,0,'F','0','0','system:post:export','#','admin','2022-12-02 05:06:54','',NULL,''),(1026,'字典查询',105,1,'#','','',1,0,'F','0','0','system:dict:query','#','admin','2022-12-02 05:06:54','',NULL,''),(1027,'字典新增',105,2,'#','','',1,0,'F','0','0','system:dict:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1028,'字典修改',105,3,'#','','',1,0,'F','0','0','system:dict:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1029,'字典删除',105,4,'#','','',1,0,'F','0','0','system:dict:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1030,'字典导出',105,5,'#','','',1,0,'F','0','0','system:dict:export','#','admin','2022-12-02 05:06:54','',NULL,''),(1031,'参数查询',106,1,'#','','',1,0,'F','0','0','system:config:query','#','admin','2022-12-02 05:06:54','',NULL,''),(1032,'参数新增',106,2,'#','','',1,0,'F','0','0','system:config:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1033,'参数修改',106,3,'#','','',1,0,'F','0','0','system:config:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1034,'参数删除',106,4,'#','','',1,0,'F','0','0','system:config:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1035,'参数导出',106,5,'#','','',1,0,'F','0','0','system:config:export','#','admin','2022-12-02 05:06:54','',NULL,''),(1036,'公告查询',107,1,'#','','',1,0,'F','0','0','system:notice:query','#','admin','2022-12-02 05:06:54','',NULL,''),(1037,'公告新增',107,2,'#','','',1,0,'F','0','0','system:notice:add','#','admin','2022-12-02 05:06:54','',NULL,''),(1038,'公告修改',107,3,'#','','',1,0,'F','0','0','system:notice:edit','#','admin','2022-12-02 05:06:54','',NULL,''),(1039,'公告删除',107,4,'#','','',1,0,'F','0','0','system:notice:remove','#','admin','2022-12-02 05:06:54','',NULL,''),(1040,'操作查询',500,1,'#','','',1,0,'F','0','0','monitor:operlog:query','#','admin','2022-12-02 05:06:55','',NULL,''),(1041,'操作删除',500,2,'#','','',1,0,'F','0','0','monitor:operlog:remove','#','admin','2022-12-02 05:06:55','',NULL,''),(1042,'日志导出',500,4,'#','','',1,0,'F','0','0','monitor:operlog:export','#','admin','2022-12-02 05:06:55','',NULL,''),(1043,'登录查询',501,1,'#','','',1,0,'F','0','0','monitor:logininfor:query','#','admin','2022-12-02 05:06:55','',NULL,''),(1044,'登录删除',501,2,'#','','',1,0,'F','0','0','monitor:logininfor:remove','#','admin','2022-12-02 05:06:55','',NULL,''),(1045,'日志导出',501,3,'#','','',1,0,'F','0','0','monitor:logininfor:export','#','admin','2022-12-02 05:06:55','',NULL,''),(1046,'在线查询',109,1,'#','','',1,0,'F','0','0','monitor:online:query','#','admin','2022-12-02 05:06:55','',NULL,''),(1047,'批量强退',109,2,'#','','',1,0,'F','0','0','monitor:online:batchLogout','#','admin','2022-12-02 05:06:55','',NULL,''),(1048,'单条强退',109,3,'#','','',1,0,'F','0','0','monitor:online:forceLogout','#','admin','2022-12-02 05:06:55','',NULL,''),(1050,'账户解锁',501,4,'#','','',1,0,'F','0','0','monitor:logininfor:unlock','#','admin','2022-12-02 05:06:55','',NULL,''),(1055,'生成查询',115,1,'#','','',1,0,'F','0','0','tool:gen:query','#','admin','2022-12-02 05:06:55','',NULL,''),(1056,'生成修改',115,2,'#','','',1,0,'F','0','0','tool:gen:edit','#','admin','2022-12-02 05:06:55','',NULL,''),(1057,'生成删除',115,3,'#','','',1,0,'F','0','0','tool:gen:remove','#','admin','2022-12-02 05:06:55','',NULL,''),(1058,'导入代码',115,2,'#','','',1,0,'F','0','0','tool:gen:import','#','admin','2022-12-02 05:06:55','',NULL,''),(1059,'预览代码',115,4,'#','','',1,0,'F','0','0','tool:gen:preview','#','admin','2022-12-02 05:06:55','',NULL,''),(1060,'生成代码',115,5,'#','','',1,0,'F','0','0','tool:gen:code','#','admin','2022-12-02 05:06:55','',NULL,''),(1500,'测试单表',5,1,'demo','demo/demo/index',NULL,1,0,'C','0','0','demo:demo:list','#','admin','2022-12-02 05:07:40','',NULL,'测试单表菜单'),(1501,'测试单表查询',1500,1,'#','',NULL,1,0,'F','0','0','demo:demo:query','#','admin','2022-12-02 05:07:40','',NULL,''),(1502,'测试单表新增',1500,2,'#','',NULL,1,0,'F','0','0','demo:demo:add','#','admin','2022-12-02 05:07:40','',NULL,''),(1503,'测试单表修改',1500,3,'#','',NULL,1,0,'F','0','0','demo:demo:edit','#','admin','2022-12-02 05:07:40','',NULL,''),(1504,'测试单表删除',1500,4,'#','',NULL,1,0,'F','0','0','demo:demo:remove','#','admin','2022-12-02 05:07:40','',NULL,''),(1505,'测试单表导出',1500,5,'#','',NULL,1,0,'F','0','0','demo:demo:export','#','admin','2022-12-02 05:07:40','',NULL,''),(1506,'测试树表',5,1,'tree','demo/tree/index',NULL,1,0,'C','0','0','demo:tree:list','#','admin','2022-12-02 05:07:40','',NULL,'测试树表菜单'),(1507,'测试树表查询',1506,1,'#','',NULL,1,0,'F','0','0','demo:tree:query','#','admin','2022-12-02 05:07:40','',NULL,''),(1508,'测试树表新增',1506,2,'#','',NULL,1,0,'F','0','0','demo:tree:add','#','admin','2022-12-02 05:07:40','',NULL,''),(1509,'测试树表修改',1506,3,'#','',NULL,1,0,'F','0','0','demo:tree:edit','#','admin','2022-12-02 05:07:40','',NULL,''),(1510,'测试树表删除',1506,4,'#','',NULL,1,0,'F','0','0','demo:tree:remove','#','admin','2022-12-02 05:07:40','',NULL,''),(1511,'测试树表导出',1506,5,'#','',NULL,1,0,'F','0','0','demo:tree:export','#','admin','2022-12-02 05:07:40','',NULL,''),(1600,'文件查询',118,1,'#','','',1,0,'F','0','0','system:oss:query','#','admin','2022-12-02 05:06:55','',NULL,''),(1601,'文件上传',118,2,'#','','',1,0,'F','0','0','system:oss:upload','#','admin','2022-12-02 05:06:55','',NULL,''),(1602,'文件下载',118,3,'#','','',1,0,'F','0','0','system:oss:download','#','admin','2022-12-02 05:06:55','',NULL,''),(1603,'文件删除',118,4,'#','','',1,0,'F','0','0','system:oss:remove','#','admin','2022-12-02 05:06:55','',NULL,''),(1604,'配置添加',118,5,'#','','',1,0,'F','0','0','system:oss:add','#','admin','2022-12-02 05:06:55','',NULL,''),(1605,'配置编辑',118,6,'#','','',1,0,'F','0','0','system:oss:edit','#','admin','2022-12-02 05:06:55','',NULL,''),(1720999559165194240,'实时任务',1747465179131650050,1,'realtimeTask','system/realtimeTask/index',NULL,1,0,'C','0','0','system:realtimeTask:list','monitor','admin','2024-01-17 03:43:38','admin','2024-01-17 11:46:30','实时任务菜单'),(1720999559165194241,'实时任务查询',1720999559165194240,1,'#','',NULL,1,0,'F','0','0','system:realtimeTask:query','#','admin','2024-01-17 03:43:38','',NULL,''),(1720999559165194242,'实时任务新增',1720999559165194240,2,'#','',NULL,1,0,'F','0','0','system:realtimeTask:add','#','admin','2024-01-17 03:43:38','',NULL,''),(1720999559165194243,'实时任务修改',1720999559165194240,3,'#','',NULL,1,0,'F','0','0','system:realtimeTask:edit','#','admin','2024-01-17 03:43:38','',NULL,''),(1720999559165194244,'实时任务删除',1720999559165194240,4,'#','',NULL,1,0,'F','0','0','system:realtimeTask:remove','#','admin','2024-01-17 03:43:38','',NULL,''),(1720999559165194245,'实时任务导出',1720999559165194240,5,'#','',NULL,1,0,'F','0','0','system:realtimeTask:export','#','admin','2024-01-17 03:43:38','',NULL,''),(1721031570324000768,'离线任务',1747465179131650050,2,'offlineTask','system/offlineTask/index',NULL,1,0,'C','0','0','system:offlineTask:list','offline','admin','2024-01-17 03:43:09','admin','2024-01-17 11:47:01','离线任务菜单'),(1721031570324000769,'离线任务查询',1721031570324000768,1,'#','',NULL,1,0,'F','0','0','system:offlineTask:query','#','admin','2024-01-17 03:43:09','',NULL,''),(1721031570324000770,'离线任务新增',1721031570324000768,2,'#','',NULL,1,0,'F','0','0','system:offlineTask:add','#','admin','2024-01-17 03:43:09','',NULL,''),(1721031570324000771,'离线任务修改',1721031570324000768,3,'#','',NULL,1,0,'F','0','0','system:offlineTask:edit','#','admin','2024-01-17 03:43:09','',NULL,''),(1721031570324000772,'离线任务删除',1721031570324000768,4,'#','',NULL,1,0,'F','0','0','system:offlineTask:remove','#','admin','2024-01-17 03:43:09','',NULL,''),(1721031570324000773,'离线任务导出',1721031570324000768,5,'#','',NULL,1,0,'F','0','0','system:offlineTask:export','#','admin','2024-01-17 03:43:09','',NULL,''),(1721346515414618112,'本地任务',1747465179131650050,3,'localTask','system/localTask/index',NULL,1,0,'C','0','0','system:localTask:list','loca_task','admin','2024-01-17 03:42:02','admin','2024-01-17 11:47:24','本地任务菜单'),(1721346515414618113,'本地任务查询',1721346515414618112,1,'#','',NULL,1,0,'F','0','0','system:localTask:query','#','admin','2024-01-17 03:42:02','',NULL,''),(1721346515414618114,'本地任务新增',1721346515414618112,2,'#','',NULL,1,0,'F','0','0','system:localTask:add','#','admin','2024-01-17 03:42:02','',NULL,''),(1721346515414618115,'本地任务修改',1721346515414618112,3,'#','',NULL,1,0,'F','0','0','system:localTask:edit','#','admin','2024-01-17 03:42:02','',NULL,''),(1721346515414618116,'本地任务删除',1721346515414618112,4,'#','',NULL,1,0,'F','0','0','system:localTask:remove','#','admin','2024-01-17 03:42:02','',NULL,''),(1721346515414618117,'本地任务导出',1721346515414618112,5,'#','',NULL,1,0,'F','0','0','system:localTask:export','#','admin','2024-01-17 03:42:02','',NULL,''),(1721365914498445312,'任务结果',0,4,'analysisResult','system/analysisResult/index',NULL,1,0,'C','0','0','system:analysisResult:list','analysis','admin','2024-01-17 03:42:01','admin','2024-01-17 13:34:13','任务结果菜单'),(1721365914498445313,'任务结果查询',1721365914498445312,1,'#','',NULL,1,0,'F','0','0','system:analysisResult:query','#','admin','2024-01-17 03:42:01','',NULL,''),(1721365914498445314,'任务结果新增',1721365914498445312,2,'#','',NULL,1,0,'F','0','0','system:analysisResult:add','#','admin','2024-01-17 03:42:01','',NULL,''),(1721365914498445315,'任务结果修改',1721365914498445312,3,'#','',NULL,1,0,'F','0','0','system:analysisResult:edit','#','admin','2024-01-17 03:42:01','',NULL,''),(1721365914498445316,'任务结果删除',1721365914498445312,4,'#','',NULL,1,0,'F','0','0','system:analysisResult:remove','#','admin','2024-01-17 03:42:01','',NULL,''),(1721365914498445317,'任务结果导出',1721365914498445312,5,'#','',NULL,1,0,'F','0','0','system:analysisResult:export','#','admin','2024-01-17 03:42:01','',NULL,''),(1726783216152801280,'终端管理',0,2,'terminal','system/terminal/index',NULL,1,0,'C','0','0','system:terminal:list','ai_board','admin','2024-01-17 03:42:02','admin','2024-01-17 13:35:57','终端管理菜单'),(1726783216152801281,'终端管理查询',1726783216152801280,1,'#','',NULL,1,0,'F','0','0','system:terminal:query','#','admin','2024-01-17 03:42:02','',NULL,''),(1726783216152801282,'终端管理新增',1726783216152801280,2,'#','',NULL,1,0,'F','0','0','system:terminal:add','#','admin','2024-01-17 03:42:02','',NULL,''),(1726783216152801283,'终端管理修改',1726783216152801280,3,'#','',NULL,1,0,'F','0','0','system:terminal:edit','#','admin','2024-01-17 03:42:02','',NULL,''),(1726783216152801284,'终端管理删除',1726783216152801280,4,'#','',NULL,1,0,'F','0','0','system:terminal:remove','#','admin','2024-01-17 03:42:02','',NULL,''),(1726783216152801285,'终端管理导出',1726783216152801280,5,'#','',NULL,1,0,'F','0','0','system:terminal:export','#','admin','2024-01-17 03:42:02','',NULL,''),(1744204632045211648,'人脸属性',1720999156411346946,1,'faceAttr','system/faceAttr/index',NULL,1,0,'C','0','0','system:faceAttr:list','#','admin','2024-01-17 03:42:01','',NULL,'人脸属性菜单'),(1744204632045211649,'人脸属性查询',1744204632045211648,1,'#','',NULL,1,0,'F','0','0','system:faceAttr:query','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632045211650,'人脸属性新增',1744204632045211648,2,'#','',NULL,1,0,'F','0','0','system:faceAttr:add','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632045211651,'人脸属性修改',1744204632045211648,3,'#','',NULL,1,0,'F','0','0','system:faceAttr:edit','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632045211652,'人脸属性删除',1744204632045211648,4,'#','',NULL,1,0,'F','0','0','system:faceAttr:remove','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632045211653,'人脸属性导出',1744204632045211648,5,'#','',NULL,1,0,'F','0','0','system:faceAttr:export','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632795992064,'行人属性',1720999156411346946,1,'personAttr','system/personAttr/index',NULL,1,0,'C','0','0','system:personAttr:list','#','admin','2024-01-17 03:42:01','',NULL,'行人属性菜单'),(1744204632795992065,'行人属性查询',1744204632795992064,1,'#','',NULL,1,0,'F','0','0','system:personAttr:query','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632795992066,'行人属性新增',1744204632795992064,2,'#','',NULL,1,0,'F','0','0','system:personAttr:add','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632795992067,'行人属性修改',1744204632795992064,3,'#','',NULL,1,0,'F','0','0','system:personAttr:edit','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632795992068,'行人属性删除',1744204632795992064,4,'#','',NULL,1,0,'F','0','0','system:personAttr:remove','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632795992069,'行人属性导出',1744204632795992064,5,'#','',NULL,1,0,'F','0','0','system:personAttr:export','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632930209792,'车辆属性',3,1,'vehicleAttr','system/vehicleAttr/index',NULL,1,0,'C','0','0','system:vehicleAttr:list','#','admin','2024-01-17 03:42:01','',NULL,'车辆属性菜单'),(1744204632930209793,'车辆属性查询',1744204632930209792,1,'#','',NULL,1,0,'F','0','0','system:vehicleAttr:query','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632930209794,'车辆属性新增',1744204632930209792,2,'#','',NULL,1,0,'F','0','0','system:vehicleAttr:add','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632930209795,'车辆属性修改',1744204632930209792,3,'#','',NULL,1,0,'F','0','0','system:vehicleAttr:edit','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632930209796,'车辆属性删除',1744204632930209792,4,'#','',NULL,1,0,'F','0','0','system:vehicleAttr:remove','#','admin','2024-01-17 03:42:01','',NULL,''),(1744204632930209797,'车辆属性导出',1744204632930209792,5,'#','',NULL,1,0,'F','0','0','system:vehicleAttr:export','#','admin','2024-01-17 03:42:01','',NULL,''),(1747465179131650050,'任务管理',0,3,'taskManager',NULL,NULL,1,0,'M','0','0',NULL,'build','admin','2024-01-17 11:45:40','admin','2024-01-17 13:34:07',''),(1747469188412317698,'算法服务',0,5,'algorithm','system/algorithm/index',NULL,1,0,'C','0','0','system:algorithm:list','ic_algorithm','admin','2024-01-17 12:01:36','admin','2024-01-17 13:34:45','');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_notice`
--

DROP TABLE IF EXISTS `sys_notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_notice` (
  `notice_id` bigint NOT NULL COMMENT '公告ID',
  `notice_title` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='通知公告表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_notice`
--

LOCK TABLES `sys_notice` WRITE;
/*!40000 ALTER TABLE `sys_notice` DISABLE KEYS */;
INSERT INTO `sys_notice` VALUES (1,'温馨提醒：2018-07-01 新版本发布啦','2',_binary '新版本内容','0','admin','2022-12-02 05:07:01','',NULL,'管理员'),(2,'维护通知：2018-07-01 系统凌晨维护','1',_binary '维护内容','0','admin','2022-12-02 05:07:01','',NULL,'管理员');
/*!40000 ALTER TABLE `sys_notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_offline_task`
--

DROP TABLE IF EXISTS `sys_offline_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_offline_task` (
  `task_id` bigint NOT NULL COMMENT '任务ID',
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `ipc_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '摄像头名称',
  `ipc_position` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '摄像头位置',
  `offline_stream_url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '码流地址',
  `stream_status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '视频流状态（0正常, 1断开）',
  `is_time_plan` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否定时启动',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `stream_start_time` datetime DEFAULT NULL COMMENT '流开始时间',
  `stream_end_time` datetime DEFAULT NULL COMMENT '流结束时间',
  `progress` int NOT NULL DEFAULT '0' COMMENT '进度',
  `channel_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '通道的状态信息，0:通道不存在，1:通道正在创建，2:通道正在运\n⾏，3:通道正在修改，4:通道正在关闭，5:通道被释放，6:通道打\n开失败，7:通道seek失败',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '任务状态（0未开始，1正常， 2停止，3异常）',
  `terminal_id` bigint NOT NULL COMMENT '终端ID',
  `terminal_name` varchar(45) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '终端名',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='离线任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_offline_task`
--

LOCK TABLES `sys_offline_task` WRITE;
/*!40000 ALTER TABLE `sys_offline_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_offline_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_oper_log`
--

DROP TABLE IF EXISTS `sys_oper_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint NOT NULL COMMENT '日志主键',
  `title` varchar(50) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '模块标题',
  `business_type` int DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求方式',
  `operator_type` int DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '返回参数',
  `status` int DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='操作日志记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_oper_log`
--

LOCK TABLES `sys_oper_log` WRITE;
/*!40000 ALTER TABLE `sys_oper_log` DISABLE KEYS */;
INSERT INTO `sys_oper_log` VALUES (1598666796368613378,'用户头像',2,'com.ruoyi.web.controller.system.SysProfileController.avatar()','POST',1,'admin','','/system/user/profile/avatar','192.168.31.18','内网IP','','',1,'创建Bucket失败, 请核对配置信息:[The Access Key Id you provided does not exist in our records. (Service: Amazon S3; Status Code: 403; Error Code: InvalidAccessKeyId; Request ID: 172CFC2F01A634FF; S3 Extended Request ID: 60f6cebc-1376-4a77-9c86-4220a4c2f858; Proxy: null)]','2022-12-02 21:14:00'),(1598666843680362498,'用户头像',2,'com.ruoyi.web.controller.system.SysProfileController.avatar()','POST',1,'admin','','/system/user/profile/avatar','192.168.31.18','内网IP','','',1,'创建Bucket失败, 请核对配置信息:[The Access Key Id you provided does not exist in our records. (Service: Amazon S3; Status Code: 403; Error Code: InvalidAccessKeyId; Request ID: 172CFC31A3CA21C1; S3 Extended Request ID: 60f6cebc-1376-4a77-9c86-4220a4c2f858; Proxy: null)]','2022-12-02 21:14:11'),(1598668873241153537,'对象存储配置',2,'com.ruoyi.web.controller.system.SysOssConfigController.edit()','PUT',1,'admin','','/system/oss/config','192.168.31.18','内网IP','{\"searchValue\":null,\"createBy\":null,\"createTime\":null,\"updateBy\":null,\"updateTime\":null,\"params\":{},\"ossConfigId\":1,\"configKey\":\"minio\",\"accessKey\":\"dqa8B7v2zAr3bCzt\",\"secretKey\":\"nxMOorSFDf7WLvmAiNKwFqxP8QUKDx8Y\",\"bucketName\":\"ring-obj\",\"prefix\":\"\",\"endpoint\":\"192.168.31.65:9000\",\"domain\":\"\",\"isHttps\":\"N\",\"status\":\"0\",\"region\":\"\",\"ext1\":\"\",\"remark\":null}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-02 21:22:15'),(1598669233779331073,'用户头像',2,'com.ruoyi.web.controller.system.SysProfileController.avatar()','POST',1,'admin','','/system/user/profile/avatar','192.168.31.18','内网IP','','{\"code\":200,\"msg\":\"操作成功\",\"data\":{\"imgUrl\":\"http://192.168.31.65:9000/ring-obj/2022/12/02/98c6e01d251049f08d68695cbb8fdee7.png\"}}',0,'','2022-12-02 21:23:41'),(1598669260551573506,'个人信息',2,'com.ruoyi.web.controller.system.SysProfileController.updateProfile()','PUT',1,'admin','','/system/user/profile','192.168.31.18','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-02 05:06:52\",\"updateBy\":\"admin\",\"updateTime\":\"2022-12-02 21:23:48\",\"params\":{},\"userId\":1,\"deptId\":null,\"userName\":null,\"nickName\":\"Grantfee\",\"userType\":\"sys_user\",\"email\":\"49767018@qq.com\",\"phonenumber\":\"15888888888\",\"sex\":\"0\",\"avatar\":null,\"status\":\"0\",\"delFlag\":\"0\",\"loginIp\":\"192.168.31.18\",\"loginDate\":\"2022-12-02 20:56:39\",\"remark\":\"管理员\",\"dept\":{\"searchValue\":null,\"createBy\":null,\"createTime\":null,\"updateBy\":null,\"updateTime\":null,\"params\":{},\"parentName\":null,\"parentId\":101,\"children\":[],\"deptId\":103,\"deptName\":\"研发部门\",\"orderNum\":1,\"leader\":\"若依\",\"phone\":null,\"email\":null,\"status\":\"0\",\"delFlag\":null,\"ancestors\":\"0,100,101\"},\"roles\":[{\"searchValue\":null,\"createBy\":null,\"createTime\":null,\"updateBy\":null,\"updateTime\":null,\"params\":{},\"roleId\":1,\"roleName\":\"超级管理员\",\"roleKey\":\"admin\",\"roleSort\":1,\"dataScope\":\"1\",\"menuCheckStrictly\":null,\"deptCheckStrictly\":null,\"status\":\"0\",\"delFlag\":null,\"remark\":null,\"flag\":false,\"menuIds\":null,\"deptIds\":null,\"permissions\":null,\"admin\":true}],\"roleIds\":null,\"postIds\":null,\"roleId\":null,\"admin\":true}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-02 21:23:48'),(1598669720557670401,'用户头像',2,'com.ruoyi.web.controller.system.SysProfileController.avatar()','POST',1,'admin','','/system/user/profile/avatar','192.168.31.18','内网IP','','{\"code\":200,\"msg\":\"操作成功\",\"data\":{\"imgUrl\":\"http://192.168.31.65:9000/ring-obj/2022/12/02/6e7a5e2ea97c441f9508dc235ea214be.png\"}}',0,'','2022-12-02 21:25:37'),(1598670822191616001,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.31.18','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-02 05:06:53\",\"updateBy\":\"admin\",\"updateTime\":\"2022-12-02 21:30:00\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":4,\"menuName\":\"PLUS官网\",\"orderNum\":4,\"path\":\"https://gitee.com/JavaLionLi/RuoYi-Vue-Plus\",\"component\":null,\"queryParam\":\"\",\"isFrame\":\"0\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"1\",\"status\":\"1\",\"perms\":\"\",\"icon\":\"guide\",\"remark\":\"RuoYi-Vue-Plus官网地址\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-02 21:30:00'),(1598670852281552897,'菜单管理',2,'com.ruoyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.31.18','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-02 05:07:40\",\"updateBy\":\"admin\",\"updateTime\":\"2022-12-02 21:30:07\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":5,\"menuName\":\"测试菜单\",\"orderNum\":5,\"path\":\"demo\",\"component\":null,\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"1\",\"status\":\"1\",\"icon\":\"star\",\"remark\":\"\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-02 21:30:07'),(1598671099862929409,'角色管理',3,'com.ruoyi.web.controller.system.SysRoleController.remove()','DELETE',1,'admin','','/system/role/4','192.168.31.18','内网IP','{roleIds=4}','',1,'仅本人已分配,不能删除','2022-12-02 21:31:06'),(1598672355499134977,'部门管理',3,'com.ruoyi.web.controller.system.SysDeptController.remove()','DELETE',1,'admin','','/system/dept/109','192.168.31.18','内网IP','{deptId=109}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-02 21:36:06'),(1598672367394181122,'部门管理',3,'com.ruoyi.web.controller.system.SysDeptController.remove()','DELETE',1,'admin','','/system/dept/108','192.168.31.18','内网IP','{deptId=108}','{\"code\":500,\"msg\":\"部门存在用户,不允许删除\",\"data\":null}',0,'','2022-12-02 21:36:08'),(1599018027133915138,'用户管理',3,'com.ruoyi.web.controller.system.SysUserController.remove()','DELETE',1,'admin','','/system/user/3','192.168.31.18','内网IP','{userIds=3}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-03 20:29:40'),(1599018064169619457,'用户管理',3,'com.ruoyi.web.controller.system.SysUserController.remove()','DELETE',1,'admin','','/system/user/4','192.168.31.18','内网IP','{userIds=4}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-03 20:29:49'),(1599018241869697025,'部门管理',2,'com.ruoyi.web.controller.system.SysDeptController.edit()','PUT',1,'admin','','/system/dept','192.168.31.18','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-02 05:06:52\",\"updateBy\":\"admin\",\"updateTime\":\"2022-12-03 20:30:31\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"deptId\":100,\"deptName\":\"智波科技\",\"orderNum\":0,\"leader\":\"智波\",\"phone\":\"15888888888\",\"email\":\"xxx@qq.com\",\"status\":\"0\",\"delFlag\":\"0\",\"ancestors\":\"0\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-03 20:30:31'),(1599018261901692930,'部门管理',3,'com.ruoyi.web.controller.system.SysDeptController.remove()','DELETE',1,'admin','','/system/dept/102','192.168.31.18','内网IP','{deptId=102}','{\"code\":500,\"msg\":\"存在下级部门,不允许删除\",\"data\":null}',0,'','2022-12-03 20:30:36'),(1599018279668764673,'部门管理',3,'com.ruoyi.web.controller.system.SysDeptController.remove()','DELETE',1,'admin','','/system/dept/108','192.168.31.18','内网IP','{deptId=108}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-03 20:30:40'),(1599018291362484225,'部门管理',3,'com.ruoyi.web.controller.system.SysDeptController.remove()','DELETE',1,'admin','','/system/dept/102','192.168.31.18','内网IP','{deptId=102}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2022-12-03 20:30:43'),(1747459542628802562,'参数管理',2,'com.hanyi.web.controller.system.SysConfigController.edit()','PUT',1,'admin','','/system/config','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-01 21:07:00\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:23:15\",\"params\":{},\"configId\":4,\"configName\":\"账号自助-验证码开关\",\"configKey\":\"sys.account.captchaEnabled\",\"configValue\":\"false\",\"configType\":\"N\",\"remark\":\"是否开启验证码功能（true开启，false关闭）\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:23:16'),(1747460626172715009,'角色管理',2,'com.hanyi.web.controller.system.SysRoleController.edit()','PUT',1,'admin','','/system/role','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2023-11-05 02:22:23\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:27:34\",\"params\":{},\"roleId\":\"1720989909128654850\",\"roleName\":\"高级管理员\",\"roleKey\":\"manager\",\"roleSort\":3,\"dataScope\":\"4\",\"menuCheckStrictly\":true,\"deptCheckStrictly\":true,\"status\":\"0\",\"delFlag\":\"0\",\"remark\":\"高级管理员\",\"flag\":false,\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,103,1017,1018,1019,1020,\"1733003501906542594\",\"1726783216152801280\",\"1720999156411346946\",\"1720999559165194240\",\"1721031570324000768\",\"1721346515414618112\",\"1744204632795992064\",\"1744204632045211648\",\"1721365914498445312\",\"1744966015116443649\"],\"deptIds\":null,\"permissions\":null,\"admin\":false}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:27:34'),(1747465179194564609,'菜单管理',1,'com.hanyi.web.controller.system.SysMenuController.add()','POST',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-17 03:45:39\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:45:39\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1747465179131650050\",\"menuName\":\"任务管理\",\"orderNum\":2,\"path\":\"taskManager\",\"component\":null,\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"icon\":\"build\",\"remark\":null}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:45:40'),(1747465391568953346,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:43:38\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:46:30\",\"params\":{},\"parentName\":null,\"parentId\":\"1747465179131650050\",\"children\":[],\"menuId\":\"1720999559165194240\",\"menuName\":\"实时任务\",\"orderNum\":1,\"path\":\"realtimeTask\",\"component\":\"system/realtimeTask/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:realtimeTask:list\",\"icon\":\"monitor\",\"remark\":\"实时任务菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:46:30'),(1747465519000297473,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:43:09\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:47:00\",\"params\":{},\"parentName\":null,\"parentId\":\"1747465179131650050\",\"children\":[],\"menuId\":\"1721031570324000768\",\"menuName\":\"离线任务\",\"orderNum\":2,\"path\":\"offlineTask\",\"component\":\"system/offlineTask/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:offlineTask:list\",\"icon\":\"offline\",\"remark\":\"离线任务菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:47:01'),(1747465615532204033,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:02\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:47:23\",\"params\":{},\"parentName\":null,\"parentId\":\"1747465179131650050\",\"children\":[],\"menuId\":\"1721346515414618112\",\"menuName\":\"本地任务\",\"orderNum\":3,\"path\":\"localTask\",\"component\":\"system/localTask/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:localTask:list\",\"icon\":\"loca_task\",\"remark\":\"本地任务菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:47:24'),(1747465805756473345,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:48:09\",\"params\":{},\"parentName\":null,\"parentId\":null,\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":3,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"#\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:48:09'),(1747466022182559745,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:49:00\",\"params\":{},\"parentName\":null,\"parentId\":3,\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":3,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"#\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:49:01'),(1747466405818769409,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:50:32\",\"params\":{},\"parentName\":null,\"parentId\":null,\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":3,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"#\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:50:32'),(1747466536718802946,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:51:03\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":3,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"#\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:51:03'),(1747466655899951105,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-01 21:06:53\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:51:31\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":3,\"menuName\":\"系统工具\",\"orderNum\":4,\"path\":\"tool\",\"component\":null,\"queryParam\":\"\",\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"\",\"icon\":\"tool\",\"remark\":\"系统工具目录\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:51:32'),(1747466849936842753,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:52:18\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":3,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"analysis\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:52:18'),(1747467756191719425,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 03:55:54\",\"params\":{},\"parentName\":null,\"parentId\":\"1747465179131650050\",\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":3,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"analysis\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 11:55:54'),(1747469188454260737,'菜单管理',1,'com.hanyi.web.controller.system.SysMenuController.add()','POST',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-17 04:01:35\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 04:01:35\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1747469188412317698\",\"menuName\":\"算法服务\",\"orderNum\":3,\"path\":\"algorithm\",\"component\":\"/system/algorithm/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:algorithm:list\",\"icon\":\"ic_algorithm\",\"remark\":null}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 12:01:36'),(1747469311875850242,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-17 04:01:36\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 04:02:05\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1747469188412317698\",\"menuName\":\"算法服务\",\"orderNum\":3,\"path\":\"algorithm\",\"component\":\"system/algorithm/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:algorithm:list\",\"icon\":\"ic_algorithm\",\"remark\":\"\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 12:02:05'),(1747469466842800129,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 04:02:41\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":3,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"analysis\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 12:02:42'),(1747469581699620866,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-17 04:01:36\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 04:03:09\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1747469188412317698\",\"menuName\":\"算法服务\",\"orderNum\":4,\"path\":\"algorithm\",\"component\":\"system/algorithm/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:algorithm:list\",\"icon\":\"ic_algorithm\",\"remark\":\"\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 12:03:09'),(1747470250401701889,'角色管理',2,'com.hanyi.web.controller.system.SysRoleController.edit()','PUT',1,'admin','','/system/role','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2023-11-05 02:22:23\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 04:05:48\",\"params\":{},\"roleId\":\"1720989909128654850\",\"roleName\":\"高级管理员\",\"roleKey\":\"manager\",\"roleSort\":3,\"dataScope\":\"4\",\"menuCheckStrictly\":true,\"deptCheckStrictly\":true,\"status\":\"0\",\"delFlag\":\"0\",\"remark\":\"高级管理员\",\"flag\":false,\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,103,1017,1018,1019,1020,\"1726783216152801280\",\"1747465179131650050\",\"1720999559165194240\",\"1720999559165194241\",\"1720999559165194242\",\"1720999559165194243\",\"1720999559165194244\",\"1720999559165194245\",\"1721031570324000768\",\"1721031570324000769\",\"1721031570324000770\",\"1721031570324000771\",\"1721031570324000772\",\"1721031570324000773\",\"1721346515414618112\",\"1721346515414618113\",\"1721346515414618114\",\"1721346515414618115\",\"1721346515414618116\",\"1721346515414618117\",\"1721365914498445312\",\"1721365914498445313\",\"1721365914498445314\",\"1721365914498445315\",\"1721365914498445316\",\"1721365914498445317\",\"1747469188412317698\"],\"deptIds\":null,\"permissions\":null,\"admin\":false}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 12:05:49'),(1747492406355402753,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:02\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:33:51\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1726783216152801280\",\"menuName\":\"终端管理\",\"orderNum\":2,\"path\":\"terminal\",\"component\":\"system/terminal/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:terminal:list\",\"icon\":\"#\",\"remark\":\"终端管理菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:33:51'),(1747492472591851522,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-17 03:45:40\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:34:06\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1747465179131650050\",\"menuName\":\"任务管理\",\"orderNum\":3,\"path\":\"taskManager\",\"component\":null,\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"icon\":\"build\",\"remark\":\"\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:34:07'),(1747492499531866114,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:01\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:34:13\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1721365914498445312\",\"menuName\":\"任务结果\",\"orderNum\":4,\"path\":\"analysisResult\",\"component\":\"system/analysisResult/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:analysisResult:list\",\"icon\":\"analysis\",\"remark\":\"任务结果菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:34:13'),(1747492571103469570,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-01 21:06:53\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:34:30\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":3,\"menuName\":\"系统工具\",\"orderNum\":5,\"path\":\"tool\",\"component\":null,\"queryParam\":\"\",\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"\",\"icon\":\"tool\",\"remark\":\"系统工具目录\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:34:30'),(1747492592230178817,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-01 21:06:53\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:34:35\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":4,\"menuName\":\"PLUS官网\",\"orderNum\":6,\"path\":\"https://gitee.com/JavaLionLi/RuoYi-Vue-Plus\",\"component\":null,\"queryParam\":\"\",\"isFrame\":\"0\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"1\",\"status\":\"1\",\"perms\":\"\",\"icon\":\"guide\",\"remark\":\"RuoYi-Vue-Plus官网地址\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:34:35'),(1747492632856207361,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-17 04:01:36\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:34:45\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1747469188412317698\",\"menuName\":\"算法服务\",\"orderNum\":5,\"path\":\"algorithm\",\"component\":\"system/algorithm/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:algorithm:list\",\"icon\":\"ic_algorithm\",\"remark\":\"\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:34:45'),(1747492703412789249,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-01 21:06:53\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:35:01\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":3,\"menuName\":\"系统工具\",\"orderNum\":6,\"path\":\"tool\",\"component\":null,\"queryParam\":\"\",\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"\",\"icon\":\"tool\",\"remark\":\"系统工具目录\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:35:02'),(1747492738942738433,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2022-12-01 21:07:40\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:35:10\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":5,\"menuName\":\"测试菜单\",\"orderNum\":7,\"path\":\"demo\",\"component\":null,\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"M\",\"visible\":\"1\",\"status\":\"1\",\"icon\":\"star\",\"remark\":\"\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:35:10'),(1747492934787375106,'菜单管理',2,'com.hanyi.web.controller.system.SysMenuController.edit()','PUT',1,'admin','','/system/menu','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2024-01-16 19:42:02\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:35:57\",\"params\":{},\"parentName\":null,\"parentId\":0,\"children\":[],\"menuId\":\"1726783216152801280\",\"menuName\":\"终端管理\",\"orderNum\":2,\"path\":\"terminal\",\"component\":\"system/terminal/index\",\"queryParam\":null,\"isFrame\":\"1\",\"isCache\":\"0\",\"menuType\":\"C\",\"visible\":\"0\",\"status\":\"0\",\"perms\":\"system:terminal:list\",\"icon\":\"ai_board\",\"remark\":\"终端管理菜单\"}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:35:57'),(1747493035765243906,'角色管理',2,'com.hanyi.web.controller.system.SysRoleController.edit()','PUT',1,'admin','','/system/role','192.168.2.183','内网IP','{\"searchValue\":null,\"createBy\":\"admin\",\"createTime\":\"2023-11-05 02:22:23\",\"updateBy\":\"admin\",\"updateTime\":\"2024-01-17 05:36:21\",\"params\":{},\"roleId\":\"1720989909128654850\",\"roleName\":\"高级管理员\",\"roleKey\":\"manager\",\"roleSort\":3,\"dataScope\":\"4\",\"menuCheckStrictly\":true,\"deptCheckStrictly\":true,\"status\":\"0\",\"delFlag\":\"0\",\"remark\":\"高级管理员\",\"flag\":false,\"menuIds\":[1,100,1001,1002,1003,1004,1005,1006,1007,103,1017,1018,1019,1020,\"1726783216152801280\",\"1726783216152801281\",\"1726783216152801282\",\"1726783216152801283\",\"1726783216152801284\",\"1726783216152801285\",\"1747465179131650050\",\"1720999559165194240\",\"1720999559165194241\",\"1720999559165194242\",\"1720999559165194243\",\"1720999559165194244\",\"1720999559165194245\",\"1721031570324000768\",\"1721031570324000769\",\"1721031570324000770\",\"1721031570324000771\",\"1721031570324000772\",\"1721031570324000773\",\"1721346515414618112\",\"1721346515414618113\",\"1721346515414618114\",\"1721346515414618115\",\"1721346515414618116\",\"1721346515414618117\",\"1721365914498445312\",\"1721365914498445313\",\"1721365914498445314\",\"1721365914498445315\",\"1721365914498445316\",\"1721365914498445317\",\"1747469188412317698\"],\"deptIds\":null,\"permissions\":null,\"admin\":false}','{\"code\":200,\"msg\":\"操作成功\",\"data\":null}',0,'','2024-01-17 13:36:21');
/*!40000 ALTER TABLE `sys_oper_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_oss`
--

DROP TABLE IF EXISTS `sys_oss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_oss` (
  `oss_id` bigint NOT NULL COMMENT '对象存储主键',
  `file_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `original_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原名',
  `file_suffix` varchar(10) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件后缀名',
  `url` varchar(500) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'URL地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '上传人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新人',
  `service` varchar(20) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'minio' COMMENT '服务商',
  PRIMARY KEY (`oss_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='OSS对象存储表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_oss`
--

LOCK TABLES `sys_oss` WRITE;
/*!40000 ALTER TABLE `sys_oss` DISABLE KEYS */;
INSERT INTO `sys_oss` VALUES (1598669233666084866,'2022/12/02/98c6e01d251049f08d68695cbb8fdee7.png','ic_avatar.png','.png','http://192.168.31.65:9000/ring-obj/2022/12/02/98c6e01d251049f08d68695cbb8fdee7.png','2022-12-02 21:23:41','admin','2022-12-02 21:23:41','admin','minio'),(1598669720297623554,'2022/12/02/6e7a5e2ea97c441f9508dc235ea214be.png','profile.png','.png','http://192.168.31.65:9000/ring-obj/2022/12/02/6e7a5e2ea97c441f9508dc235ea214be.png','2022-12-02 21:25:37','admin','2022-12-02 21:25:37','admin','minio');
/*!40000 ALTER TABLE `sys_oss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_oss_config`
--

DROP TABLE IF EXISTS `sys_oss_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_oss_config` (
  `oss_config_id` bigint NOT NULL COMMENT '主建',
  `config_key` varchar(20) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置key',
  `access_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT 'accessKey',
  `secret_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '秘钥',
  `bucket_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '桶名称',
  `prefix` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '前缀',
  `endpoint` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '访问站点',
  `domain` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '自定义域名',
  `is_https` char(1) COLLATE utf8mb4_general_ci DEFAULT 'N' COMMENT '是否https（Y=是,N=否）',
  `region` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '域',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '状态（0=正常,1=停用）',
  `ext1` varchar(255) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '扩展字段',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`oss_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='对象存储配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_oss_config`
--

LOCK TABLES `sys_oss_config` WRITE;
/*!40000 ALTER TABLE `sys_oss_config` DISABLE KEYS */;
INSERT INTO `sys_oss_config` VALUES (1,'minio','dqa8B7v2zAr3bCzt','nxMOorSFDf7WLvmAiNKwFqxP8QUKDx8Y','ring-obj','','192.168.31.65:9000','','N','','0','','admin','2022-12-02 05:07:02','admin','2022-12-02 21:22:15',''),(2,'qiniu','XXXXXXXXXXXXXXX','XXXXXXXXXXXXXXX','ruoyi','','s3-cn-north-1.qiniucs.com','','N','','1','','admin','2022-12-02 05:07:02','admin','2022-12-02 05:07:02',NULL),(3,'aliyun','XXXXXXXXXXXXXXX','XXXXXXXXXXXXXXX','ruoyi','','oss-cn-beijing.aliyuncs.com','','N','','1','','admin','2022-12-02 05:07:02','admin','2022-12-02 05:07:02',NULL),(4,'qcloud','XXXXXXXXXXXXXXX','XXXXXXXXXXXXXXX','ruoyi-1250000000','','cos.ap-beijing.myqcloud.com','','N','ap-beijing','1','','admin','2022-12-02 05:07:02','admin','2022-12-02 05:07:02',NULL),(5,'image','ruoyi','ruoyi123','ruoyi','image','127.0.0.1:9000','','N','','1','','admin','2022-12-02 05:07:02','admin','2022-12-02 05:07:02',NULL);
/*!40000 ALTER TABLE `sys_oss_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_person_attr`
--

DROP TABLE IF EXISTS `sys_person_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_person_attr` (
  `id` bigint NOT NULL COMMENT 'ID',
  `result_id` bigint NOT NULL COMMENT '事件ID',
  `gender` varchar(16) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '性别',
  `gender_score` float NOT NULL DEFAULT '0' COMMENT '性别分值',
  `age` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '年龄',
  `age_score` float NOT NULL DEFAULT '0' COMMENT '年龄分值',
  `upper_color` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '上衣颜色',
  `upper_color_score` float NOT NULL DEFAULT '0' COMMENT '上衣颜色分值',
  `bottom_color` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '下衣颜色',
  `bottom_color_score` float NOT NULL DEFAULT '0' COMMENT '下衣颜色分值',
  `upper_type` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '上衣类型',
  `upper_type_score` float NOT NULL DEFAULT '0' COMMENT '上衣类型分值',
  `bottom_type` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '下衣类型',
  `bottom_type_score` float NOT NULL DEFAULT '0' COMMENT '下衣类型分值',
  `hair` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '头发',
  `hair_score` float NOT NULL DEFAULT '0' COMMENT '头发分值',
  `hat` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '帽子',
  `hat_score` float NOT NULL DEFAULT '0' COMMENT '帽子分值',
  `mask` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '口罩',
  `mask_score` float NOT NULL DEFAULT '0' COMMENT '口罩分值',
  `glass` varchar(64) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '未知' COMMENT '眼镜',
  `glass_score` float NOT NULL DEFAULT '0' COMMENT '眼镜分值',
  `dept_id` bigint DEFAULT NULL,
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='行人属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_person_attr`
--

LOCK TABLES `sys_person_attr` WRITE;
/*!40000 ALTER TABLE `sys_person_attr` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_person_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_post`
--

DROP TABLE IF EXISTS `sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_post` (
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  `post_code` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='岗位信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_post`
--

LOCK TABLES `sys_post` WRITE;
/*!40000 ALTER TABLE `sys_post` DISABLE KEYS */;
INSERT INTO `sys_post` VALUES (1,'ceo','董事长',1,'0','admin','2022-12-02 05:06:52','',NULL,''),(2,'se','项目经理',2,'0','admin','2022-12-02 05:06:52','',NULL,''),(3,'hr','人力资源',3,'0','admin','2022-12-02 05:06:52','',NULL,''),(4,'user','普通员工',4,'0','admin','2022-12-02 05:06:52','',NULL,'');
/*!40000 ALTER TABLE `sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_realtime_task`
--

DROP TABLE IF EXISTS `sys_realtime_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_realtime_task` (
  `task_id` bigint NOT NULL COMMENT '任务ID',
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `ipc_name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '摄像头名称',
  `ipc_position` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '摄像头位置',
  `main_stream_url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主码流流地址',
  `sub_stream_url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子流流地址',
  `main_stream_status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '视频流状态（0正常, 1断开）',
  `is_time_plan` char(1) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否定时任务',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `progress` int NOT NULL DEFAULT '0',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '任务状态（0未开始，1正常， 2停止，3异常）',
  `channel_status` tinyint(1) NOT NULL DEFAULT '0',
  `terminal_id` bigint NOT NULL,
  `terminal_name` varchar(60) COLLATE utf8mb4_general_ci DEFAULT '',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='实时任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_realtime_task`
--

LOCK TABLES `sys_realtime_task` WRITE;
/*!40000 ALTER TABLE `sys_realtime_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_realtime_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `role_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
  `status` char(1) COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'超级管理员','admin',1,'1',1,1,'0','0','admin','2022-12-02 05:06:53','',NULL,'超级管理员'),(2,'普通角色','common',2,'2',1,1,'0','0','admin','2022-12-02 05:06:53','',NULL,'普通角色'),(3,'本部门及以下','test1',3,'4',1,1,'0','0','admin','2022-12-02 05:07:40','admin',NULL,NULL),(4,'仅本人','test2',4,'5',1,1,'0','0','admin','2022-12-02 05:07:40','admin',NULL,NULL),(1720989909128654850,'高级管理员','manager',3,'4',1,1,'0','0','admin','2023-11-05 10:22:23','admin','2024-01-17 13:36:21','高级管理员'),(1730393087242727425,'普通管理员','guest',4,'4',1,1,'0','0','admin','2023-12-01 09:07:15','admin','2023-12-01 09:08:40','普通管理员');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_dept`
--

DROP TABLE IF EXISTS `sys_role_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色和部门关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_dept`
--

LOCK TABLES `sys_role_dept` WRITE;
/*!40000 ALTER TABLE `sys_role_dept` DISABLE KEYS */;
INSERT INTO `sys_role_dept` VALUES (2,100),(2,101),(2,105);
/*!40000 ALTER TABLE `sys_role_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色和菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (2,1),(2,2),(2,3),(2,4),(2,100),(2,101),(2,102),(2,103),(2,104),(2,105),(2,106),(2,107),(2,108),(2,109),(2,110),(2,111),(2,112),(2,113),(2,114),(2,115),(2,116),(2,117),(2,120),(2,500),(2,501),(2,1000),(2,1001),(2,1002),(2,1003),(2,1004),(2,1005),(2,1006),(2,1007),(2,1008),(2,1009),(2,1010),(2,1011),(2,1012),(2,1013),(2,1014),(2,1015),(2,1016),(2,1017),(2,1018),(2,1019),(2,1020),(2,1021),(2,1022),(2,1023),(2,1024),(2,1025),(2,1026),(2,1027),(2,1028),(2,1029),(2,1030),(2,1031),(2,1032),(2,1033),(2,1034),(2,1035),(2,1036),(2,1037),(2,1038),(2,1039),(2,1040),(2,1041),(2,1042),(2,1043),(2,1044),(2,1045),(2,1046),(2,1047),(2,1048),(2,1050),(2,1055),(2,1056),(2,1057),(2,1058),(2,1059),(2,1060),(2,1720999156411346946),(2,1720999559165194240),(2,1720999559165194241),(2,1720999559165194242),(2,1720999559165194243),(2,1720999559165194244),(2,1720999559165194245),(2,1721031570324000768),(2,1721031570324000769),(2,1721031570324000770),(2,1721031570324000771),(2,1721031570324000772),(2,1721031570324000773),(2,1721346515414618112),(2,1721346515414618113),(2,1721346515414618114),(2,1721346515414618115),(2,1721346515414618116),(2,1721346515414618117),(2,1721365914498445312),(2,1721365914498445313),(2,1721365914498445314),(2,1721365914498445315),(2,1721365914498445316),(2,1721365914498445317),(2,1726783216152801280),(2,1726783216152801281),(2,1726783216152801282),(2,1726783216152801283),(2,1726783216152801284),(2,1726783216152801285),(3,1),(3,5),(3,100),(3,101),(3,102),(3,103),(3,104),(3,105),(3,106),(3,107),(3,108),(3,500),(3,501),(3,1001),(3,1002),(3,1003),(3,1004),(3,1005),(3,1006),(3,1007),(3,1008),(3,1009),(3,1010),(3,1011),(3,1012),(3,1013),(3,1014),(3,1015),(3,1016),(3,1017),(3,1018),(3,1019),(3,1020),(3,1021),(3,1022),(3,1023),(3,1024),(3,1025),(3,1026),(3,1027),(3,1028),(3,1029),(3,1030),(3,1031),(3,1032),(3,1033),(3,1034),(3,1035),(3,1036),(3,1037),(3,1038),(3,1039),(3,1040),(3,1041),(3,1042),(3,1043),(3,1044),(3,1045),(3,1500),(3,1501),(3,1502),(3,1503),(3,1504),(3,1505),(3,1506),(3,1507),(3,1508),(3,1509),(3,1510),(3,1511),(4,5),(4,1500),(4,1501),(4,1502),(4,1503),(4,1504),(4,1505),(4,1506),(4,1507),(4,1508),(4,1509),(4,1510),(4,1511),(1720989909128654850,1),(1720989909128654850,100),(1720989909128654850,103),(1720989909128654850,1001),(1720989909128654850,1002),(1720989909128654850,1003),(1720989909128654850,1004),(1720989909128654850,1005),(1720989909128654850,1006),(1720989909128654850,1007),(1720989909128654850,1017),(1720989909128654850,1018),(1720989909128654850,1019),(1720989909128654850,1020),(1720989909128654850,1720999559165194240),(1720989909128654850,1720999559165194241),(1720989909128654850,1720999559165194242),(1720989909128654850,1720999559165194243),(1720989909128654850,1720999559165194244),(1720989909128654850,1720999559165194245),(1720989909128654850,1721031570324000768),(1720989909128654850,1721031570324000769),(1720989909128654850,1721031570324000770),(1720989909128654850,1721031570324000771),(1720989909128654850,1721031570324000772),(1720989909128654850,1721031570324000773),(1720989909128654850,1721346515414618112),(1720989909128654850,1721346515414618113),(1720989909128654850,1721346515414618114),(1720989909128654850,1721346515414618115),(1720989909128654850,1721346515414618116),(1720989909128654850,1721346515414618117),(1720989909128654850,1721365914498445312),(1720989909128654850,1721365914498445313),(1720989909128654850,1721365914498445314),(1720989909128654850,1721365914498445315),(1720989909128654850,1721365914498445316),(1720989909128654850,1721365914498445317),(1720989909128654850,1726783216152801280),(1720989909128654850,1726783216152801281),(1720989909128654850,1726783216152801282),(1720989909128654850,1726783216152801283),(1720989909128654850,1726783216152801284),(1720989909128654850,1726783216152801285),(1720989909128654850,1747465179131650050),(1720989909128654850,1747469188412317698),(1730393087242727425,1720999156411346946),(1730393087242727425,1720999559165194240),(1730393087242727425,1720999559165194241),(1730393087242727425,1720999559165194245),(1730393087242727425,1721031570324000768),(1730393087242727425,1721031570324000769),(1730393087242727425,1721031570324000773),(1730393087242727425,1721346515414618112),(1730393087242727425,1721346515414618113),(1730393087242727425,1721346515414618117),(1730393087242727425,1721365914498445312),(1730393087242727425,1721365914498445313),(1730393087242727425,1721365914498445317),(1730393087242727425,1726783216152801280),(1730393087242727425,1726783216152801281),(1730393087242727425,1726783216152801285);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_terminal`
--

DROP TABLE IF EXISTS `sys_terminal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_terminal` (
  `terminal_id` bigint NOT NULL COMMENT '终端ID',
  `terminal_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '终端名称',
  `service_ip` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'IP地址',
  `service_port` int NOT NULL COMMENT '端口',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`terminal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='终端管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_terminal`
--

LOCK TABLES `sys_terminal` WRITE;
/*!40000 ALTER TABLE `sys_terminal` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_terminal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(10) COLLATE utf8mb4_general_ci DEFAULT 'sys_user' COMMENT '用户类型（sys_user系统用户）',
  `email` varchar(50) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '手机号码',
  `sex` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '密码',
  `status` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,103,'admin','Grantfee','sys_user','49767018@qq.com','15888888888','0','','$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2','0','0','192.168.2.183','2024-01-17 13:35:45','admin','2022-12-02 05:06:52','admin','2024-01-17 13:35:45','管理员'),(1730402602562379778,100,'supper_admin','超级管理员','sys_user','','','0','','$2a$10$hoqpEp7gfC937VMlpOzjYOTpFI8iUEP6r9IdiiDCXkxR9FcxoQNDe','0','0','127.0.0.1','2023-12-03 12:10:15','admin','2023-12-01 09:45:04','supper_admin','2023-12-03 12:10:15',''),(1730440700629405697,101,'manager','高级管理员','sys_user','','','0','','$2a$10$uslgmmF6N2g47cH27Xcu0.xSMvNSYdJFPurVsLspwqd17EiDxqnt6','0','0','192.168.2.183','2024-01-17 13:36:28','supper_admin','2023-12-01 12:16:27','manager','2024-01-17 13:36:28',''),(1730785942499024898,1727580449928417281,'xixiang_manager','xixiang_manager','sys_user','','','0','','$2a$10$NYmagjhLfvoQtY7vhipMA.mE7r/GqrHeEgfUwwXtksTlL1uG.iHaG','0','0','127.0.0.1','2023-12-02 11:08:43','supper_admin','2023-12-02 11:08:19','xixiang_manager','2023-12-02 11:08:43',''),(1730786325996822530,1727666297910890498,'xixiang_guest','xixiang_guest','sys_user','','','0','','$2a$10$G3UvRMJnDyJGJlBFmyl98ecACleYyRpuEM0cQ7yTOEtTytxI/Z.GG','0','0','127.0.0.1','2023-12-02 11:10:28','xixiang_manager','2023-12-02 11:09:51','xixiang_guest','2023-12-02 11:10:28',''),(1730811760746967042,1727580449928417281,'manager2','manager2','sys_user','','','0','','$2a$10$XtfAi4MQDY0SbHhCKDMe7OU5o2oixRCwHtH1Ztz1bnssWaTobAmtC','0','0','127.0.0.1','2023-12-03 13:04:01','supper_admin','2023-12-02 12:50:55','manager2','2023-12-03 13:04:01','');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_post`
--

DROP TABLE IF EXISTS `sys_user_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_post` (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户与岗位关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_post`
--

LOCK TABLES `sys_user_post` WRITE;
/*!40000 ALTER TABLE `sys_user_post` DISABLE KEYS */;
INSERT INTO `sys_user_post` VALUES (1,1),(1726805161711828993,4),(1726806676627324929,2),(1727665632060936194,4),(1727667073366396930,3),(1730401055187202050,1),(1730402602562379778,1),(1730440700629405697,2),(1730785942499024898,2),(1730786325996822530,4),(1730811760746967042,2);
/*!40000 ALTER TABLE `sys_user_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_role` (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户和角色关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1),(1726805161711828993,1720989909128654850),(1726806676627324929,2),(1727665632060936194,1720989909128654850),(1727667073366396930,1720989909128654850),(1730401055187202050,2),(1730402602562379778,2),(1730440700629405697,1720989909128654850),(1730785942499024898,1720989909128654850),(1730786325996822530,1730393087242727425),(1730811760746967042,1720989909128654850);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_vehicle_attr`
--

DROP TABLE IF EXISTS `sys_vehicle_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_vehicle_attr` (
  `id` bigint NOT NULL COMMENT 'ID',
  `result_id` bigint NOT NULL COMMENT '事件ID',
  `vehicle_type` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '车型',
  `vehicle_type_score` float DEFAULT NULL COMMENT '车型分值',
  `color` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '颜色',
  `color_score` float DEFAULT NULL COMMENT '颜色分值',
  `license` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '车牌',
  `license_score` float DEFAULT NULL COMMENT '车牌分值',
  `brand` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '品牌',
  `brand_score` float DEFAULT NULL COMMENT '品牌分值',
  `dept_id` bigint DEFAULT NULL,
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='车辆属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_vehicle_attr`
--

LOCK TABLES `sys_vehicle_attr` WRITE;
/*!40000 ALTER TABLE `sys_vehicle_attr` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_vehicle_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_demo`
--

DROP TABLE IF EXISTS `test_demo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `test_demo` (
  `id` bigint NOT NULL COMMENT '主键',
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  `order_num` int DEFAULT '0' COMMENT '排序号',
  `test_key` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'key键',
  `value` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '值',
  `version` int DEFAULT '0' COMMENT '版本',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `del_flag` int DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='测试单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_demo`
--

LOCK TABLES `test_demo` WRITE;
/*!40000 ALTER TABLE `test_demo` DISABLE KEYS */;
INSERT INTO `test_demo` VALUES (1,102,4,1,'测试数据权限','测试',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(2,102,3,2,'子节点1','111',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(3,102,3,3,'子节点2','222',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(4,108,4,4,'测试数据','demo',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(5,108,3,13,'子节点11','1111',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(6,108,3,12,'子节点22','2222',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(7,108,3,11,'子节点33','3333',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(8,108,3,10,'子节点44','4444',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(9,108,3,9,'子节点55','5555',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(10,108,3,8,'子节点66','6666',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(11,108,3,7,'子节点77','7777',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(12,108,3,6,'子节点88','8888',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(13,108,3,5,'子节点99','9999',0,'2022-12-02 05:07:42','admin',NULL,NULL,0);
/*!40000 ALTER TABLE `test_demo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_tree`
--

DROP TABLE IF EXISTS `test_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `test_tree` (
  `id` bigint NOT NULL COMMENT '主键',
  `parent_id` bigint DEFAULT '0' COMMENT '父id',
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  `tree_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '值',
  `version` int DEFAULT '0' COMMENT '版本',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `del_flag` int DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='测试树表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_tree`
--

LOCK TABLES `test_tree` WRITE;
/*!40000 ALTER TABLE `test_tree` DISABLE KEYS */;
INSERT INTO `test_tree` VALUES (1,0,102,4,'测试数据权限',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(2,1,102,3,'子节点1',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(3,2,102,3,'子节点2',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(4,0,108,4,'测试树1',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(5,4,108,3,'子节点11',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(6,4,108,3,'子节点22',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(7,4,108,3,'子节点33',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(8,5,108,3,'子节点44',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(9,6,108,3,'子节点55',0,'2022-12-02 05:07:42','admin',NULL,NULL,0),(10,7,108,3,'子节点66',0,'2022-12-02 05:07:43','admin',NULL,NULL,0),(11,7,108,3,'子节点77',0,'2022-12-02 05:07:43','admin',NULL,NULL,0),(12,10,108,3,'子节点88',0,'2022-12-02 05:07:43','admin',NULL,NULL,0),(13,10,108,3,'子节点99',0,'2022-12-02 05:07:43','admin',NULL,NULL,0);
/*!40000 ALTER TABLE `test_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_group`
--

DROP TABLE IF EXISTS `xxl_job_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_name` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行器AppName',
  `title` varchar(12) COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行器名称',
  `address_type` tinyint NOT NULL DEFAULT '0' COMMENT '执行器地址类型：0=自动注册、1=手动录入',
  `address_list` text COLLATE utf8mb4_general_ci COMMENT '执行器地址列表，多地址逗号分隔',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_group`
--

LOCK TABLES `xxl_job_group` WRITE;
/*!40000 ALTER TABLE `xxl_job_group` DISABLE KEYS */;
INSERT INTO `xxl_job_group` VALUES (1,'xxl-job-executor','示例执行器',0,NULL,'2018-11-03 22:21:31');
/*!40000 ALTER TABLE `xxl_job_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_info`
--

DROP TABLE IF EXISTS `xxl_job_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_group` int NOT NULL COMMENT '执行器主键ID',
  `job_desc` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `author` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作者',
  `alarm_email` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '报警邮件',
  `schedule_type` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'NONE' COMMENT '调度类型',
  `schedule_conf` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '调度配置，值含义取决于调度类型',
  `misfire_strategy` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DO_NOTHING' COMMENT '调度过期策略',
  `executor_route_strategy` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器路由策略',
  `executor_handler` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务参数',
  `executor_block_strategy` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '阻塞处理策略',
  `executor_timeout` int NOT NULL DEFAULT '0' COMMENT '任务执行超时时间，单位秒',
  `executor_fail_retry_count` int NOT NULL DEFAULT '0' COMMENT '失败重试次数',
  `glue_type` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext COLLATE utf8mb4_general_ci COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'GLUE备注',
  `glue_updatetime` datetime DEFAULT NULL COMMENT 'GLUE更新时间',
  `child_jobid` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
  `trigger_status` tinyint NOT NULL DEFAULT '0' COMMENT '调度状态：0-停止，1-运行',
  `trigger_last_time` bigint NOT NULL DEFAULT '0' COMMENT '上次调度时间',
  `trigger_next_time` bigint NOT NULL DEFAULT '0' COMMENT '下次调度时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_info`
--

LOCK TABLES `xxl_job_info` WRITE;
/*!40000 ALTER TABLE `xxl_job_info` DISABLE KEYS */;
INSERT INTO `xxl_job_info` VALUES (1,1,'测试任务1','2018-11-03 22:21:31','2018-11-03 22:21:31','XXL','','CRON','0 0 0 * * ? *','DO_NOTHING','FIRST','demoJobHandler','','SERIAL_EXECUTION',0,0,'BEAN','','GLUE代码初始化','2018-11-03 22:21:31','',0,0,0);
/*!40000 ALTER TABLE `xxl_job_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_lock`
--

DROP TABLE IF EXISTS `xxl_job_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_lock` (
  `lock_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '锁名称',
  PRIMARY KEY (`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_lock`
--

LOCK TABLES `xxl_job_lock` WRITE;
/*!40000 ALTER TABLE `xxl_job_lock` DISABLE KEYS */;
INSERT INTO `xxl_job_lock` VALUES ('schedule_lock');
/*!40000 ALTER TABLE `xxl_job_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_log`
--

DROP TABLE IF EXISTS `xxl_job_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `job_group` int NOT NULL COMMENT '执行器主键ID',
  `job_id` int NOT NULL COMMENT '任务，主键ID',
  `executor_address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
  `executor_handler` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务参数',
  `executor_sharding_param` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行器任务分片参数，格式如 1/2',
  `executor_fail_retry_count` int NOT NULL DEFAULT '0' COMMENT '失败重试次数',
  `trigger_time` datetime DEFAULT NULL COMMENT '调度-时间',
  `trigger_code` int NOT NULL COMMENT '调度-结果',
  `trigger_msg` text COLLATE utf8mb4_general_ci COMMENT '调度-日志',
  `handle_time` datetime DEFAULT NULL COMMENT '执行-时间',
  `handle_code` int NOT NULL COMMENT '执行-状态',
  `handle_msg` text COLLATE utf8mb4_general_ci COMMENT '执行-日志',
  `alarm_status` tinyint NOT NULL DEFAULT '0' COMMENT '告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败',
  PRIMARY KEY (`id`),
  KEY `I_trigger_time` (`trigger_time`),
  KEY `I_handle_code` (`handle_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_log`
--

LOCK TABLES `xxl_job_log` WRITE;
/*!40000 ALTER TABLE `xxl_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xxl_job_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_log_report`
--

DROP TABLE IF EXISTS `xxl_job_log_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_log_report` (
  `id` int NOT NULL AUTO_INCREMENT,
  `trigger_day` datetime DEFAULT NULL COMMENT '调度-时间',
  `running_count` int NOT NULL DEFAULT '0' COMMENT '运行中-日志数量',
  `suc_count` int NOT NULL DEFAULT '0' COMMENT '执行成功-日志数量',
  `fail_count` int NOT NULL DEFAULT '0' COMMENT '执行失败-日志数量',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_trigger_day` (`trigger_day`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_log_report`
--

LOCK TABLES `xxl_job_log_report` WRITE;
/*!40000 ALTER TABLE `xxl_job_log_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `xxl_job_log_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_logglue`
--

DROP TABLE IF EXISTS `xxl_job_logglue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_logglue` (
  `id` int NOT NULL AUTO_INCREMENT,
  `job_id` int NOT NULL COMMENT '任务，主键ID',
  `glue_type` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext COLLATE utf8mb4_general_ci COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'GLUE备注',
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_logglue`
--

LOCK TABLES `xxl_job_logglue` WRITE;
/*!40000 ALTER TABLE `xxl_job_logglue` DISABLE KEYS */;
/*!40000 ALTER TABLE `xxl_job_logglue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_registry`
--

DROP TABLE IF EXISTS `xxl_job_registry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_registry` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registry_group` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `registry_key` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `registry_value` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_g_k_v` (`registry_group`,`registry_key`,`registry_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_registry`
--

LOCK TABLES `xxl_job_registry` WRITE;
/*!40000 ALTER TABLE `xxl_job_registry` DISABLE KEYS */;
/*!40000 ALTER TABLE `xxl_job_registry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xxl_job_user`
--

DROP TABLE IF EXISTS `xxl_job_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `xxl_job_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `password` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `role` tinyint NOT NULL COMMENT '角色：0-普通用户、1-管理员',
  `permission` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限：执行器ID列表，多个逗号分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xxl_job_user`
--

LOCK TABLES `xxl_job_user` WRITE;
/*!40000 ALTER TABLE `xxl_job_user` DISABLE KEYS */;
INSERT INTO `xxl_job_user` VALUES (1,'admin','e10adc3949ba59abbe56e057f20f883e',1,NULL);
/*!40000 ALTER TABLE `xxl_job_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-17 13:37:04
